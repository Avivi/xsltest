<?xml version="1.0" encoding="UTF-8"?>
<!-- 
	Transformata generująca warstwę prezentacyjną informacji o receptach w pakiecie na potrzeby powiadomień i wydruków, 
	przygotowana przez Centrum Systemów Informacyjnych Ochrony Zdrowia.
	
	Transformata nie jest zgodna ze standardem HL7 CDA, wykorzystuje elementy HL7 CDA do wyświetlenia wybranych informacji z dokumentów medycznych.
	
	Wersja pl_informacja_o_receptach_1.3.1:1.0 dla CDA_PL_IG_1.3.1.
	
	Historia wersji:
		- pl_informacja_o_receptach_1.0:1.0, 1904 linie kodu, autor Marcin Pusz, Pentacomp Systemy Informatyczne S.A., wersja inicjalna
		- pl_informacja_o_receptach_1.3.1:1.0, 2752 linie kodu, autor Marcin Pusz, Pentacomp Systemy Informatyczne S.A., dostosowanie do IHE PRE, kod kreskowy SVG (IE9+) jako domyślna alternatywa dla zakodowanych elementów graficznych (IE8-), usunięcie zastosowania tablic globalnych ze względu na trudności z dostępem w różnych środowiskach
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:hl7="urn:hl7-org:v3" xmlns:extPL="http://www.csioz.gov.pl/xsd/extPL/r2" version="1.0">

	<xsl:output method="html" version="4.01" encoding="UTF-8" indent="yes" doctype-public="-//W3C//DTD HTML 4.01//EN" media-type="text/html" doctype-system="about:legacy-compat"/>

	<xsl:variable name="LOWERCASE_LETTERS">aąbcćdeęfghijklłmnńoópqrsśtuvwxyzżź</xsl:variable>
	<xsl:variable name="UPPERCASE_LETTERS">AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŻŹ</xsl:variable>

	<!-- 44-cyfrowy klucz pakietu recept wystarczający do realizacji recept -->
	<xsl:param name="kluczPakietu"/>

	<!-- 4-cyfrowy kod dostępowy do pakietu recept, wymagający podania id usługobiorcy przy realizacji recept -->
	<xsl:param name="kodPakietu"/>

	<!-- parametr opcjonalny, ustawiając dowolną wartość inną niż domyślna (np. disabled) można wyłączyć generowanie kodów kreskowych SVG i wykorzystać obsługiwane przez IE8- img -->
	<xsl:param name="SVG">enabled</xsl:param>

	<!-- stała dot. wysokości kodu kreskowego w pikselach (w XSLT variable to stała), aktualnie nie ma potrzeby wystawiania tego jako parametr -->
	<xsl:variable name="barcodeHeight" select="30"/>

	<!-- ilość recept w pakiecie, jeśli nie podano, przyjmuje się ilość wynikającą z wypełnionych parametrów
		 parametr wraz z parametrami doc*Nr umożliwia wyświetlenie podzbioru dużego pakietu recept na jednej informacji
		 np. Recepta 4 z 11 ogółem, Recepta 5 z 11 ogółem, Recepta 6 z 11 ogółem -->
	<xsl:param name="receptWPakiecie"/>

	<!-- dane pierwszego dokumentu recepty do wyświetlenia, dokument źródłowy przyjęto jako pierwszy -->
	<xsl:param name="kluczDoc1"/>
	<xsl:variable name="doc1" select="/"/>
	<xsl:param name="doc1Nr" select="1"/>

	<!-- dane drugiego dokumentu recepty do wyświetlenia -->
	<xsl:param name="kluczDoc2"/>
	<!-- opcjonalna nazwa pliku z dokumentem przydatna przy uruchamianiu transformaty z linii poleceń lub skryptów -->
	<xsl:param name="doc2FN"/>
	<!-- alternatywnie dla nazwy pliku z dokumentem możliwe jest podanie zawartości dokumentu, przydatne przy uruchamianiu transformaty w kodzie systemu -->
	<xsl:param name="doc2" select="document($doc2FN)"/>
	<xsl:param name="doc2Nr" select="2"/>

	<!-- dane trzeciego dokumentu recepty do wyświetlenia -->
	<xsl:param name="kluczDoc3"/>
	<xsl:param name="doc3FN"/>
	<xsl:param name="doc3" select="document($doc3FN)"/>
	<xsl:param name="doc3Nr" select="3"/>

	<!-- dane czwartego dokumentu recepty do wyświetlenia -->
	<xsl:param name="kluczDoc4"/>
	<xsl:param name="doc4FN"/>
	<xsl:param name="doc4" select="document($doc4FN)"/>
	<xsl:param name="doc4Nr" select="4"/>

	<!-- dane piątego dokumentu recepty do wyświetlenia -->
	<xsl:param name="kluczDoc5"/>
	<xsl:param name="doc5FN"/>
	<xsl:param name="doc5" select="document($doc5FN)"/>
	<xsl:param name="doc5Nr" select="5"/>

	<!-- ilość recept w pakiecie wyliczana, jeśli nie podano z parametru -->
	<xsl:variable name="iloscRecept">
		<xsl:call-template name="iloscRecept"/>
	</xsl:variable>

	<!-- opcjonalny tekst dotyczący terminu kontroli -->
	<xsl:param name="termin"/>

	<xsl:template match="/">
		<xsl:apply-templates select="hl7:ClinicalDocument"/>
	</xsl:template>

	<!-- Wyznaczenie ilości recept w pakiecie -->
	<xsl:template name="iloscRecept">
		<xsl:choose>
			<xsl:when test="$receptWPakiecie">
				<xsl:value-of select="$receptWPakiecie"/>
			</xsl:when>
			<xsl:when test="not($doc2/hl7:ClinicalDocument)">
				<xsl:value-of select="1"/>
			</xsl:when>
			<xsl:when test="not($doc3/hl7:ClinicalDocument)">
				<xsl:value-of select="2"/>
			</xsl:when>
			<xsl:when test="not($doc4/hl7:ClinicalDocument)">
				<xsl:value-of select="3"/>
			</xsl:when>
			<xsl:when test="not($doc5/hl7:ClinicalDocument)">
				<xsl:value-of select="4"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="5"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Wykorzystanie parametrów oraz dokumentów recept zgodnych z IG -->
	<xsl:template match="hl7:ClinicalDocument">
		<html>
			<head>
				<xsl:call-template name="styles"/>
			</head>
			<body>
				<div class="document">
					<xsl:call-template name="title"/>
					<xsl:call-template name="klucz">
						<xsl:with-param name="klucz" select="$kluczPakietu"/>
					</xsl:call-template>
					<xsl:call-template name="disclaimer"/>
					<div class="doc_header table">
						<xsl:call-template name="kod"/>
						<xsl:call-template name="recordTarget"/>
						<xsl:call-template name="effectiveTime"/>
						<xsl:call-template name="author"/>
						<xsl:call-template name="terminKontroli"/>
					</div>

					<xsl:call-template name="docBody">
						<xsl:with-param name="doc" select="$doc1/hl7:ClinicalDocument"/>
						<xsl:with-param name="docNr" select="$doc1Nr"/>
						<xsl:with-param name="kluczDoc" select="$kluczDoc1"/>
					</xsl:call-template>
					<xsl:call-template name="docBody">
						<xsl:with-param name="doc" select="$doc2/hl7:ClinicalDocument"/>
						<xsl:with-param name="docNr" select="$doc2Nr"/>
						<xsl:with-param name="kluczDoc" select="$kluczDoc2"/>
					</xsl:call-template>
					<xsl:call-template name="docBody">
						<xsl:with-param name="doc" select="$doc3/hl7:ClinicalDocument"/>
						<xsl:with-param name="docNr" select="$doc3Nr"/>
						<xsl:with-param name="kluczDoc" select="$kluczDoc3"/>
					</xsl:call-template>
					<xsl:call-template name="docBody">
						<xsl:with-param name="doc" select="$doc4/hl7:ClinicalDocument"/>
						<xsl:with-param name="docNr" select="$doc4Nr"/>
						<xsl:with-param name="kluczDoc" select="$kluczDoc4"/>
					</xsl:call-template>
					<xsl:call-template name="docBody">
						<xsl:with-param name="doc" select="$doc5/hl7:ClinicalDocument"/>
						<xsl:with-param name="docNr" select="$doc5Nr"/>
						<xsl:with-param name="kluczDoc" select="$kluczDoc5"/>
					</xsl:call-template>
					<div class="doc_header2">
						<div class="oswiadczenie">
							Oświadczam, że nie realizowałem/am wcześniej w/w recept.<br/>
							Jestem świadomy odpowiedzialności karnej za złożenie fałszywego oświadczenia.
							<div class="podpis">
								.........................................................................................................
							</div>
							<div class="dataIMiejscowosc">
								(data i podpis)
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

	<!--
		Dane wyświetlane w ramach informacji o receptach:
		- klucz i kod dostępowy pakietu recept na podstawie zewnętrznych parametrów
		- dane wspólne o receptach w pakiecie na podstawie danych jednej z recept:
			- data wystawienia pakietu recept
			- imię i nazwisko pacjenta
			- imię i nazwisko osoby wystawiającej pakiet recept
			- numer prawa wykonywania zawodu osoby wystawiającej
			- numer telefonu do bezpośredniego kontaktu z osobą wystawiającą
			
		- dane poszczególnych dokumentów recept:
			- klucz dokumentu recepty na podstawie zewnętrznego parametru
			- id dokumentu recepty
			- narrative text sekcji zalecenia leku (Prescriptions), w tym:
				- nazwa leku
				- postać
				- dawka
				- ilość
				- sposób stosowania (do narrative block sekcji zalecenia leku włączono od 1.2.1 IG)
			- datę "realizacji od" konkretnej daty, jeżeli dotyczy
		
		- wskazany termin kontroli lekarskiej, jeżeli dotyczy, na podstawie zewnętrznego parametru o tekstowym typie
	-->

	<xsl:template name="docBody">
		<xsl:param name="doc"/>
		<xsl:param name="docNr"/>
		<xsl:param name="kluczDoc"/>

		<xsl:if test="$doc">
			<div style="page-break-after:avoid">&#160;</div>
			<div class="doc_body">
				<div class="noScreen">
					<div id="footer">

					</div>
				</div>
				<xsl:call-template name="docTitle">
					<xsl:with-param name="docNr" select="$docNr"/>
				</xsl:call-template>

				<xsl:call-template name="docId">
					<xsl:with-param name="docId" select="$doc/hl7:id"/>
				</xsl:call-template>

				<xsl:call-template name="klucz">
					<xsl:with-param name="klucz" select="$kluczDoc"/>
				</xsl:call-template>

				<div class="body_element table">
					<xsl:call-template name="prescriptions">
						<xsl:with-param name="doc" select="$doc"/>
					</xsl:call-template>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<!-- tytuł informacji -->
	<xsl:template name="title">
		<div class="doc_title">
			<span>
				<xsl:text>Informacja o receptach elektronicznych</xsl:text>
			</span>
		</div>
	</xsl:template>

	<!-- nazwa dokumentu recepty -->
	<xsl:template name="docTitle">
		<xsl:param name="docNr"/>
		<div class="section_title">
			<xsl:text>Recepta </xsl:text>
			<xsl:value-of select="$docNr"/>
			<xsl:text> z </xsl:text>
			<xsl:value-of select="$iloscRecept"/>
			<xsl:text> ogółem</xsl:text>
		</div>
	</xsl:template>

	<!-- identyfikator recepty -->
	<xsl:template name="docId">
		<xsl:param name="docId"/>
		<div class="header_element">
			<xsl:choose>
				<xsl:when test="string-length($docId/@extension) &gt;= 1 and string-length($docId/@root) + string-length($docId/@extension) &gt; 46">
					<div class="doc_id">
						<div class="doc_id_label header_label">
							<xsl:text>ID</xsl:text>
						</div>
						<div class="doc_id_value header_inline_value header_value">
							<xsl:value-of select="$docId/@root"/>
						</div>
					</div>
					<div style="clear:both"/>
					<div class="doc_id">
						<div class="doc_id_value header_inline_value header_value">
							<xsl:value-of select="$docId/@extension"/>
						</div>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="doc_id">
						<div class="doc_id_label header_label">
							<xsl:text>ID</xsl:text>
						</div>
						<div class="doc_id_value header_inline_value header_value">
							<xsl:value-of select="$docId/@root"/>
							<xsl:if test="string-length($docId/@extension) &gt;= 1">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$docId/@extension"/>
							</xsl:if>
						</div>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<!-- effectiveTime -->
	<xsl:template name="effectiveTime">
		<div class="header_element rowedm">
			<xsl:call-template name="dateTimeInDiv">
				<xsl:with-param name="date" select="hl7:effectiveTime"/>
				<xsl:with-param name="label">Wystawiono</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<!-- klucz pakietu lub dokumentu -->
	<xsl:template name="klucz">
		<xsl:param name="klucz"/>

		<div class="klucz">
			<xsl:choose>
				<xsl:when test="$klucz and string-length($klucz) = 44">
					<xsl:variable name="barcodeWidth" select="277"/>

					<xsl:variable name="p1" select="number(substring($klucz, 1, 2))"/>
					<xsl:variable name="p2" select="number(substring($klucz, 3, 2))"/>
					<xsl:variable name="p3" select="number(substring($klucz, 5, 2))"/>
					<xsl:variable name="p4" select="number(substring($klucz, 7, 2))"/>
					<xsl:variable name="p5" select="number(substring($klucz, 9, 2))"/>
					<xsl:variable name="p6" select="number(substring($klucz, 11, 2))"/>
					<xsl:variable name="p7" select="number(substring($klucz, 13, 2))"/>
					<xsl:variable name="p8" select="number(substring($klucz, 15, 2))"/>
					<xsl:variable name="p9" select="number(substring($klucz, 17, 2))"/>
					<xsl:variable name="p10" select="number(substring($klucz, 19, 2))"/>
					<xsl:variable name="p11" select="number(substring($klucz, 21, 2))"/>
					<xsl:variable name="p12" select="number(substring($klucz, 23, 2))"/>
					<xsl:variable name="p13" select="number(substring($klucz, 25, 2))"/>
					<xsl:variable name="p14" select="number(substring($klucz, 27, 2))"/>
					<xsl:variable name="p15" select="number(substring($klucz, 29, 2))"/>
					<xsl:variable name="p16" select="number(substring($klucz, 31, 2))"/>
					<xsl:variable name="p17" select="number(substring($klucz, 33, 2))"/>
					<xsl:variable name="p18" select="number(substring($klucz, 35, 2))"/>
					<xsl:variable name="p19" select="number(substring($klucz, 37, 2))"/>
					<xsl:variable name="p20" select="number(substring($klucz, 39, 2))"/>
					<xsl:variable name="p21" select="number(substring($klucz, 41, 2))"/>
					<xsl:variable name="p22" select="number(substring($klucz, 43, 2))"/>

					<xsl:variable name="checksum" select="(105+$p1*1+$p2*2+$p3*3+$p4*4+$p5*5+$p6*6+$p7*7+$p8*8+$p9*9+$p10*10+$p11*11+$p12*12+$p13*13+$p14*14+$p15*15+$p16*16+$p17*17+$p18*18+$p19*19+$p20*20+$p21*21+$p22*22) mod 103"/>

					<div class="barcode">
						<xsl:choose>
							<xsl:when test="$SVG = 'enabled'">
								<!-- kod SVG niekompatybilny z IE8-, przy generowaniu wyniku należy podać parametr SVG=disabled by wykorzystać img zamiast SVG. Efekt może być mniej czytelny dla czytników kodów. -->
								<svg id="kluczSkierowania" xmlns="http://www.w3.org/2000/svg" width="{$barcodeWidth}" height="{$barcodeHeight}">
									<g transform="translate(0)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="'startC'"/>
										</xsl:call-template>
									</g>
									<g transform="translate(11)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p1"/>
										</xsl:call-template>
									</g>
									<g transform="translate(22)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p2"/>
										</xsl:call-template>
									</g>
									<g transform="translate(33)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p3"/>
										</xsl:call-template>
									</g>
									<g transform="translate(44)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p4"/>
										</xsl:call-template>
									</g>
									<g transform="translate(55)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p5"/>
										</xsl:call-template>
									</g>
									<g transform="translate(66)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p6"/>
										</xsl:call-template>
									</g>
									<g transform="translate(77)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p7"/>
										</xsl:call-template>
									</g>
									<g transform="translate(88)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p8"/>
										</xsl:call-template>
									</g>
									<g transform="translate(99)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p9"/>
										</xsl:call-template>
									</g>
									<g transform="translate(110)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p10"/>
										</xsl:call-template>
									</g>
									<g transform="translate(121)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p11"/>
										</xsl:call-template>
									</g>
									<g transform="translate(132)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p12"/>
										</xsl:call-template>
									</g>
									<g transform="translate(143)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p13"/>
										</xsl:call-template>
									</g>
									<g transform="translate(154)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p14"/>
										</xsl:call-template>
									</g>
									<g transform="translate(165)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p15"/>
										</xsl:call-template>
									</g>
									<g transform="translate(176)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p16"/>
										</xsl:call-template>
									</g>
									<g transform="translate(187)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p17"/>
										</xsl:call-template>
									</g>
									<g transform="translate(198)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p18"/>
										</xsl:call-template>
									</g>
									<g transform="translate(209)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p19"/>
										</xsl:call-template>
									</g>
									<g transform="translate(220)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p20"/>
										</xsl:call-template>
									</g>
									<g transform="translate(231)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p21"/>
										</xsl:call-template>
									</g>
									<g transform="translate(242)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$p22"/>
										</xsl:call-template>
									</g>
									<g transform="translate(253)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="$checksum"/>
										</xsl:call-template>
									</g>
									<g transform="translate(264)">
										<xsl:call-template name="drawBarSVG">
											<xsl:with-param name="code" select="'stopAndTerminate'"/>
										</xsl:call-template>
									</g>
								</svg>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="'startC'"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p1"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p2"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p3"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p4"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p5"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p6"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p7"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p8"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p9"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p10"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p11"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p12"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p13"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p14"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p15"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p16"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p17"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p18"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p19"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p20"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p21"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$p22"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="$checksum"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="'stop'"/>
								</xsl:call-template>
								<xsl:call-template name="drawBarIMG">
									<xsl:with-param name="code" select="'term'"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div class="klucz_value">
						<xsl:value-of select="$klucz"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Brak poprawnego klucza dokumentu</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<!-- kod dostępu do pakietu recept -->
	<xsl:template name="kod">
		<div class="header_element rowedm">
			<div class="row_label cell">
				<xsl:text>Kod dostępu</xsl:text>
			</div>
			<div class="row_value cell">
				<xsl:choose>
					<xsl:when test="$kodPakietu and string-length($kodPakietu) &gt;= 1">
						<!-- ograniczenie tymczasowe wynikające z obsługi kodu w P1, planuje się wprowadzenie informacji o błędnym kodzie przy długości != 4 -->
						<xsl:variable name="kodPakietu4cyfry" select="substring($kodPakietu, 1, 4)"/>
						<xsl:value-of select="$kodPakietu4cyfry"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>brak kodu dostępu</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>

	<!-- zastrzeżenie dot. treści informacji o receptach elektronicznych -->
	<xsl:template name="disclaimer">
		<div class="header_element">
			<div class="disclaimer">
				<div>
					<xsl:text>Informacja umożliwia realizację recept elektronicznych,</xsl:text>
				</div>
				<div>
					<xsl:text>nie zastępując jednak ich treści.</xsl:text>
				</div>
				<div class="strona_ikp">
					Wejdź na <b>pacjent.gov.pl</b> i poznaj szczegóły.
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- sekcja główna dokumentu recepty -->
	<xsl:template name="prescriptions">
		<xsl:param name="doc"/>

		<!-- sekcja Prescriptions o różnym OID w zależności od rodzaju recepty, ze stałym kodem LOINC 57828-6 wykorzystanym do odnalezienia jej -->
		<xsl:variable name="prescriptions" select="$doc/hl7:component/hl7:structuredBody/hl7:component/hl7:section[hl7:code/@code='57828-6' and hl7:code/@codeSystem='2.16.840.1.113883.6.1']"/>
		<xsl:variable name="sa" select="$prescriptions/hl7:entry/hl7:substanceAdministration"/>

		<div class="header_element rowedm">
			<div class="row_label cell">
				<xsl:text>Przepisano</xsl:text>
			</div>
			<div class="row_value cell">
				<xsl:call-template name="sectionText">
					<xsl:with-param name="text" select="$prescriptions/hl7:text"/>
				</xsl:call-template>
			</div>
		</div>

		<xsl:variable name="dateFrom" select="$sa/hl7:entryRelationship/hl7:supply/hl7:effectiveTime"/>

		<xsl:if test="$dateFrom/@value and string-length($dateFrom/@value) &gt;= 1">
			<xsl:variable name="later">
				<xsl:call-template name="isFirstDateLaterThanSecond">
					<xsl:with-param name="firstDate" select="$dateFrom/@value"/>
					<xsl:with-param name="secondDate" select="$doc/hl7:effectiveTime/@value"/>
				</xsl:call-template>
			</xsl:variable>

			<xsl:if test="$later = 'true'">
				<div class="header_element rowedm">
					<xsl:call-template name="dateTimeInDiv">
						<xsl:with-param name="date" select="$dateFrom"/>
						<xsl:with-param name="label">Realizacja od</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template name="terminKontroli">
		<xsl:if test="$termin and string-length($termin) &gt;= 1">
			<div class="header_element rowedm">
				<div class="row_label cell">
					<xsl:text>Termin kontroli</xsl:text>
				</div>
				<div class="row_value cell">
					<xsl:value-of select="$termin"/>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<!-- dane pacjenta -->
	<xsl:template name="recordTarget">
		<xsl:variable name="patientRole" select="hl7:recordTarget/hl7:patientRole"/>

		<div class="header_element rowedm">
			<div class="row_label cell">
				<xsl:text>Pacjent</xsl:text>
			</div>
			<div class="row_value cell">
				<xsl:choose>
					<xsl:when test="hl7:recordTarget/@nullFlavor">
						<xsl:call-template name="translateNullFlavor">
							<xsl:with-param name="nullableElement" select="hl7:recordTarget"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$patientRole/@nullFlavor">
						<xsl:call-template name="translateNullFlavor">
							<xsl:with-param name="nullableElement" select="$patientRole"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<!-- imiona i nazwiska pacjenta -->
						<xsl:call-template name="person">
							<xsl:with-param name="person" select="$patientRole/hl7:patient"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>

	<!-- wystawca dokumentu, do wersji PIK 1.2 był to legalAuthenticator, od PIK 1.2.1 w związku z dostosowaniem do IHE PRE jest to author. PIK dopuszcza jednego autora recepty -->
	<xsl:template name="author">
		<xsl:call-template name="assignedEntity">
			<xsl:with-param name="entity" select="hl7:author/hl7:assignedAuthor"/>
			<xsl:with-param name="blockLabel">Wystawca</xsl:with-param>
			<xsl:with-param name="knownIdentifiersOnly" select="true()"/>
		</xsl:call-template>
	</xsl:template>


	<!-- ++++++++++++++++++++++++++++++++++++++ DRUGA LINIA +++++++++++++++++++++++++++++++++++++++++++-->

	<!-- osoba przypisana AssignedEntity templateId 2.16.840.1.113883.3.4424.13.10.2.49 -->
	<xsl:template name="assignedEntity">
		<xsl:param name="entity"/>
		<!-- kontekst domyślny -->
		<xsl:param name="context">assignedEntity</xsl:param>
		<xsl:param name="blockLabel">Blok danych</xsl:param>
		<xsl:param name="knownIdentifiersOnly" select="true()"/>

		<!-- maksymalna liczność assignedEntity to 1 -->
		<!-- id 1:*, code 0:1, addr 0:*, telecom 0:*, assignedPerson 0:1/1:1, representedOrganization 0:1 -->

		<div class="header_element rowedm">
			<div class="row_label cell">
				<xsl:value-of select="$blockLabel"/>
			</div>
			<div class="row_value cell">
				<xsl:choose>
					<xsl:when test="not($entity)">
						<xsl:text>(nie podano informacji)</xsl:text>
					</xsl:when>
					<xsl:when test="$entity/@nullFlavor">
						<xsl:call-template name="translateNullFlavor">
							<xsl:with-param name="nullableElement" select="$entity"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$context = 'assignedEntity'">
						<xsl:call-template name="person">
							<xsl:with-param name="person" select="$entity/hl7:assignedPerson"></xsl:with-param>
						</xsl:call-template>

						<xsl:call-template name="identifiersInDiv">
							<xsl:with-param name="ids" select="$entity/hl7:id"/>
							<xsl:with-param name="knownOnly" select="$knownIdentifiersOnly"/>
						</xsl:call-template>

						<xsl:choose>
							<xsl:when test="$entity/hl7:telecom">
								<!-- dane kontaktowe przypisanego bytu -->
								<xsl:call-template name="addressTelecomInDivs">
									<xsl:with-param name="telecom" select="$entity/hl7:telecom"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<!-- jeśli nie podano danych przypisanego bytu, wyświetla się dane jego organizacji, o ile istnieją -->
								<xsl:call-template name="addressTelecomInDivs">
									<xsl:with-param name="telecom" select="$entity/hl7:representedOrganization/hl7:telecom"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>(nie podano informacji)</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>

	<!-- osoba templateId 2.16.840.1.113883.3.4424.13.10.2.1 -->
	<xsl:template name="person">
		<xsl:param name="person"/>

		<xsl:if test="$person">
			<xsl:choose>
				<xsl:when test="$person/@nullFlavor">
					<xsl:call-template name="translateNullFlavor">
						<xsl:with-param name="nullableElement" select="$person"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- imiona i nazwiska przypisanej osoby, brak innych istotnych danych w tym elemencie -->
					<xsl:call-template name="personName">
						<xsl:with-param name="name" select="$person/hl7:name"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- imiona i nazwiska osoby z prefiksem i suffiksem, templateId 2.16.840.1.113883.3.4424.13.10.7.2 -->
	<xsl:template name="personName">
		<xsl:param name="name"/>

		<xsl:choose>
			<xsl:when test="not($name)">
				<xsl:call-template name="translateNullFlavor">
					<xsl:with-param name="nullableElement" select="$name"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!-- może istnieć wiele "nazw" osób, przy czym jedno imię i jedno nazwisko jest wymagane -->
				<xsl:for-each select="$name">
					<div class="row_value_element">
						<xsl:choose>
							<xsl:when test="./@nullFlavor">
								<xsl:call-template name="translateNullFlavor">
									<xsl:with-param name="nullableElement" select="."/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="string-length(./hl7:prefix) &gt;= 1">
									<xsl:value-of select="./hl7:prefix"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:for-each select="./hl7:given">
									<xsl:choose>
										<xsl:when test="./@nullFlavor">
											<xsl:text>(imienia nie podano) </xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
											<xsl:text> </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:for-each select="./hl7:family">
									<xsl:choose>
										<xsl:when test="./@nullFlavor">
											<xsl:text>(nazwiska nie podano)</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:if test="position()!=last()">
										<xsl:text> </xsl:text>
									</xsl:if>
								</xsl:for-each>
								<xsl:if test="string-length(./hl7:suffix) &gt;= 1">
									<xsl:text> </xsl:text>
									<xsl:value-of select="./hl7:suffix"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- pełna, sformatowana lista identyfikatorów typu II -->
	<xsl:template name="identifiersInDiv">
		<xsl:param name="ids"/>
		<xsl:param name="knownOnly" select="false()"/>

		<xsl:variable name="displayableIds" select="$ids[not(@displayable='false')]"/>
		<xsl:variable name="count" select="count($displayableIds)"/>

		<xsl:if test="$count &gt; 0">
			<xsl:call-template name="listIdentifiersOID">
				<xsl:with-param name="ids" select="$displayableIds"/>
				<xsl:with-param name="knownOnly" select="$knownOnly"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!--  lista identyfikatorów OID -->
	<xsl:template name="listIdentifiersOID">
		<xsl:param name="ids"/>
		<xsl:param name="knownOnly" select="true()"/>

		<xsl:for-each select="$ids[not(@nullFlavor)]">
			<xsl:variable name="row">
				<xsl:call-template name="identifierOID">
					<xsl:with-param name="id" select="."/>
					<xsl:with-param name="knownOnly" select="$knownOnly"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="string-length($row) &gt;= 1">
				<div class="row_value_element">
					<xsl:value-of select="$row"/>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!--  identyfikator OID -->
	<xsl:template name="identifierOID">
		<xsl:param name="id"/>
		<xsl:param name="knownOnly"/>

		<xsl:choose>
			<xsl:when test="not($id) or $id/@nullFlavor">
				<span class="null_flavor_id">
					<xsl:text>ID </xsl:text>
				</span>
				<xsl:call-template name="translateNullFlavor">
					<xsl:with-param name="nullableElement" select="$id"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="rootName">
					<xsl:call-template name="translateOID">
						<xsl:with-param name="oid" select="$id/@root"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:choose>
					<xsl:when test="$knownOnly">
						<!-- identyfikator nie jest wyświetlany gdy nie jest znany, a knownOnly = true -->
						<xsl:if test="string-length($rootName) &gt;= 1">
							<xsl:if test="$id/@root != '2.16.840.1.113883.3.4424.1.1.616'">
								<span>
									<xsl:value-of select="$rootName"/>
									<xsl:if test="string-length($id/@extension) &gt;= 1">
										<xsl:text> </xsl:text>
										<xsl:value-of select="$id/@extension"/>
									</xsl:if>
								</span>
							</xsl:if>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<span>
							<xsl:choose>
								<xsl:when test="string-length($rootName) &gt;= 1">
									<xsl:value-of select="$rootName"/>
								</xsl:when>
								<xsl:otherwise>
									<span class="not_known_id_prefix">
										<xsl:text>ID </xsl:text>
									</span>
									<xsl:value-of select="$id/@root"/>
									<xsl:if test="string-length($id/@assigningAuthorityName) &gt;= 1">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="$id/@assigningAuthorityName"/>
										<xsl:text>)</xsl:text>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="string-length($id/@extension) &gt;= 1">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$id/@extension"/>
							</xsl:if>
						</span>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- dane adresowe i kontaktowe -->
	<xsl:template name="addressTelecomInDivs">
		<xsl:param name="addr" select="false()"/>
		<xsl:param name="telecom" select="false()"/>

		<xsl:if test="$addr and count($addr) &gt; 0">
			<xsl:call-template name="addresses">
				<xsl:with-param name="addresses" select="$addr"/>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="$telecom and count($telecom) &gt; 0">
			<xsl:call-template name="telecoms">
				<xsl:with-param name="telecoms" select="$telecom"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- adresy -->
	<xsl:template name="addresses">
		<xsl:param name="addresses"/>

		<xsl:for-each select="$addresses">
			<div class="row_value_element address_element">
				<span class="address_label">
					<xsl:call-template name="translateAddressUseCode">
						<xsl:with-param name="useCode" select="./@use"/>
					</xsl:call-template>
				</span>
				<div class="address_value">
					<xsl:call-template name="address">
						<xsl:with-param name="addr" select="."/>
					</xsl:call-template>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>

	<!-- adres templateId 2.16.840.1.113883.3.4424.13.10.7.1 -->
	<xsl:template name="address">
		<xsl:param name="addr"/>

		<xsl:choose>
			<!-- wyświetlenie informacji o braku wyłącznie gdy podano nullFlavor na poziomie całego adresu -->
			<xsl:when test="$addr/@nullFlavor">
				<xsl:call-template name="translateNullFlavor">
					<xsl:with-param name="nullableElement" select="$addr"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!-- obsługiwane są wyłącznie podstawowe pola zdefiniowane w PL IG (bez unitType,
					 w którym za granicą wyróżnia się typ lokalu, np. appartment)
					 oraz pole streetAddressLine wspierające zapis adresów zagranicznych,
					 nie są wyświetlanie adresy wprowadzane w postaci nieanalitycznej, tzw. plain-text -->
				<xsl:if test="$addr/hl7:streetAddressLine or $addr/hl7:streetName or $addr/hl7:city or $addr/hl7:country">

					<!-- linie dla adresu zagranicznego, przy czym dopuszczalne jest stosowanie także innych elementów -->
					<xsl:for-each select="$addr/hl7:streetAddressLine">
						<div>
							<xsl:value-of select="."/>
						</div>
					</xsl:for-each>

					<!-- układ adresu polskiego, stosowanego także dla adresów zagranicznych z podanym city -->
					<xsl:if test="string-length($addr/hl7:city) &gt;= 1">
						<xsl:choose>
							<xsl:when test="string-length($addr/hl7:streetName) &gt;= 1">
								<!-- ulica, numer domu, numer mieszkania -->
								<div>
									<xsl:value-of select="$addr/hl7:streetName"/>
									<xsl:if test="string-length($addr/hl7:houseNumber) &gt;= 1">
										<xsl:text> </xsl:text>
										<xsl:value-of select="$addr/hl7:houseNumber"/>
										<xsl:if test="string-length($addr/hl7:unitID) &gt;= 1">
											<xsl:text> lok. </xsl:text>
											<xsl:value-of select="$addr/hl7:unitID"/>
										</xsl:if>
									</xsl:if>
								</div>
								<xsl:choose>
									<xsl:when test="not($addr/hl7:postalCode/@postCity) or $addr/hl7:postalCode/@postCity = $addr/hl7:city">
										<!-- adres z ulicą i miejscowością posiadającą pocztę
											ul. Stroma 120
											41-400 Równe -->
										<div>
											<xsl:if test="string-length($addr/hl7:postalCode) &gt;= 1">
												<xsl:value-of select="$addr/hl7:postalCode"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:value-of select="$addr/hl7:city"/>
										</div>
									</xsl:when>
									<xsl:otherwise>
										<!-- adres z ulicą, miejscowością i inną pocztą
											ul. Stroma 120
											Wygoniska
											Poczta: 41-400 Równe -->
										<div>
											<xsl:value-of select="$addr/hl7:city"/>
										</div>
										<div>
											<xsl:text>Poczta: </xsl:text>
											<xsl:if test="string-length($addr/hl7:postalCode) &gt;= 1">
												<xsl:value-of select="$addr/hl7:postalCode"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:value-of select="$addr/hl7:postalCode/@postCity"/>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<!-- ulica nie istnieje w adresie -->
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="not($addr/hl7:postalCode/@postCity) or $addr/hl7:postalCode/@postCity = $addr/hl7:city">
										<!-- adres bez ulicy, z miejscowością posiadającą pocztę
											41-400 Równe 120 -->
										<div>
											<xsl:if test="string-length($addr/hl7:postalCode) &gt;= 1">
												<xsl:value-of select="$addr/hl7:postalCode"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:value-of select="$addr/hl7:city"/>
											<xsl:if test="string-length($addr/hl7:houseNumber) &gt;= 1">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$addr/hl7:houseNumber"/>
												<xsl:if test="string-length($addr/hl7:unitID) &gt;= 1">
													<xsl:text> lok. </xsl:text>
													<xsl:value-of select="$addr/hl7:unitID"/>
												</xsl:if>
											</xsl:if>
										</div>
									</xsl:when>
									<xsl:otherwise>
										<!-- adres bez ulicy, z miejscowością i inną pocztą
											Wygoniska 120
											41-400 Równe -->
										<div>
											<xsl:value-of select="$addr/hl7:city"/>
											<xsl:if test="string-length($addr/hl7:houseNumber) &gt;= 1">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$addr/hl7:houseNumber"/>
												<xsl:if test="string-length($addr/hl7:unitID) &gt;= 1">
													<xsl:text> lok. </xsl:text>
													<xsl:value-of select="$addr/hl7:unitID"/>
												</xsl:if>
											</xsl:if>
										</div>
										<div>
											<xsl:text>Poczta: </xsl:text>
											<xsl:if test="string-length($addr/hl7:postalCode) &gt;= 1">
												<xsl:value-of select="$addr/hl7:postalCode"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:value-of select="$addr/hl7:postalCode/@postCity"/>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>

					<!-- kod teryt hl7:censusTract nie jest wyświetlany, powinien być obsługiwany elektronicznie -->

					<!-- kraj, region/stan nie jest wyświetlany -->
					<xsl:if test="string-length($addr/hl7:country) &gt;= 1">
						<div>
							<xsl:value-of select="$addr/hl7:country"/>
						</div>
					</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- dane kontaktowe -->
	<xsl:template name="telecoms">
		<xsl:param name="telecoms"/>

		<xsl:for-each select="$telecoms">
			<div class="row_value_element">
				<xsl:call-template name="telecom">
					<xsl:with-param name="tele" select="."/>
				</xsl:call-template>
			</div>
		</xsl:for-each>
	</xsl:template>

	<!-- linia danych kontaktowych -->
	<xsl:template name="telecom">
		<xsl:param name="tele"/>

		<xsl:choose>
			<!-- wyświetlenie informacji o braku wyłącznie gdy podano nullFlavor -->
			<xsl:when test="$tele/@nullFlavor">
				<xsl:call-template name="translateNullFlavor">
					<xsl:with-param name="nullableElement" select="$tele"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!-- format <telecom use="PUB" value="tel: 22 2345 123"/> -->
				<xsl:variable name="address" select="substring-after($tele/@value, ':')"/>
				<xsl:variable name="protocol" select="substring-before($tele/@value, ':')"/>

				<xsl:choose>
					<xsl:when test="$address and $protocol">
						<xsl:call-template name="translateTelecomProtocolCode">
							<xsl:with-param name="protocolCode" select="$protocol"/>
						</xsl:call-template>

						<xsl:value-of select="$address"/>

						<xsl:if test="$tele/@use">
							<xsl:text> (</xsl:text>
							<xsl:call-template name="translateTelecomUseCode">
								<xsl:with-param name="useCode" select="$tele/@use"/>
							</xsl:call-template>
							<xsl:text>)</xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- dane w niepoprawnym formacie, są jednak wyświetlane -->
						<xsl:value-of select="$tele"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- źródło - HL7 V3 Data Types 2.19.2 Telecommunication Use Code -->
	<xsl:template name="translateTelecomUseCode">
		<xsl:param name="useCode"/>
		<xsl:choose>
			<xsl:when test="$useCode='H' or $useCode='HP'">
				<xsl:text>domowy</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='HV'">
				<xsl:text>podczas urlopu</xsl:text>
			</xsl:when>

			<xsl:when test="$useCode='WP'">
				<xsl:text>służbowy</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='DIR'">
				<xsl:text>służbowy bezpośredni</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='PUB'">
				<xsl:text>rejestracja</xsl:text>
			</xsl:when>

			<xsl:when test="$useCode='TMP'">
				<xsl:text>tymczasowy</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='EC'">
				<xsl:text>w nagłych przypadkach</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='MC'">
				<xsl:text>komórkowy</xsl:text>
			</xsl:when>

			<xsl:otherwise>
				<xsl:text>inny: </xsl:text>
				<xsl:value-of select="$useCode"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- źródło - HL7 V3 Data Types 2.18.1 Protocol Code -->
	<xsl:template name="translateTelecomProtocolCode">
		<xsl:param name="protocolCode"/>
		<xsl:choose>
			<xsl:when test="$protocolCode='fax'">
				<xsl:text>faks: </xsl:text>
			</xsl:when>
			<xsl:when test="$protocolCode='http'">
				<xsl:text>Internet: </xsl:text>
			</xsl:when>
			<xsl:when test="$protocolCode='mailto'">
				<xsl:text>e-mail: </xsl:text>
			</xsl:when>
			<xsl:when test="$protocolCode='tel'">
				<xsl:text>tel: </xsl:text>
			</xsl:when>
			<!-- pozostałe przypadki są nieistotne, będą jednak wyświetlane -->
			<xsl:otherwise>
				<xsl:text>inny: </xsl:text>
				<xsl:value-of select="$protocolCode"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- źródło - HL7 V3 Data Types 2.21.1 Postal Address Use Code -->
	<xsl:template name="translateAddressUseCode">
		<xsl:param name="useCode"/>
		<xsl:choose>
			<!-- podstawowy adres instytucji, biura lub pracy -->
			<xsl:when test="not($useCode) or $useCode='WP' or $useCode='DIR' or $useCode='PUB' or $useCode='PHYS'">
				<xsl:text>Adres</xsl:text>
			</xsl:when>

			<xsl:when test="$useCode='H' or $useCode='HP'">
				<xsl:text>Adres zamieszkania</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='HV'">
				<xsl:text>Adres w trakcie urlopu</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='TMP'">
				<xsl:text>Adres tymczasowy</xsl:text>
			</xsl:when>
			<xsl:when test="$useCode='PST'">
				<xsl:text>Adres korespondencyjny</xsl:text>
			</xsl:when>

			<xsl:otherwise>
				<xsl:text>Inny adres (</xsl:text>
				<xsl:value-of select="$useCode"/>
				<xsl:text>)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- ++++++++++++++ obsługa kodów kreskowych code 128 zestaw C w SVG (ograniczenie IE9+) ++++++++++++++++ -->
	<xsl:template name="drawBarSVG">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'startC'"><!-- 211232 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="1"/>
				<rect x="6" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 0"><!-- 212222 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="2"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 1"><!-- 222122 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="2"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 2"><!-- 222221 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="2"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 3"><!-- 121223 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 4"><!-- 121322 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 5"><!-- 131222 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 6"><!-- 122213 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 7"><!-- 122312 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 8"><!-- 132212 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 9"><!-- 221213 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 10"><!-- 221312 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 11"><!-- 231212 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="1"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 12"><!-- 112232 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="6" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 13"><!-- 122132 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="6" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 14"><!-- 122231 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 15"><!-- 113222 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="3"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 16"><!-- 123122 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="3"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 17"><!-- 123221 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="3"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 18"><!-- 223211 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="3"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 19"><!-- 221132 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="3"/>
			</xsl:when>

			<xsl:when test="$code = 20"><!-- 221231 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 21"><!-- 213212 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="3"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 22"><!-- 223112 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="3"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 23"><!-- 312131 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="2"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 24"><!-- 311222 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 25"><!-- 321122 -->
				<rect x="0" height="30" width="3"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 26"><!-- 321221 -->
				<rect x="0" height="30" width="3"/>
				<rect x="5" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 27"><!-- 312212 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 28"><!-- 322112 -->
				<rect x="0" height="30" width="3"/>
				<rect x="5" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 29"><!-- 322211 -->
				<rect x="0" height="30" width="3"/>
				<rect x="5" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 30"><!-- 212123 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="2"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 31"><!-- 212321 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="2"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 32"><!-- 232121 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="2"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 33"><!-- 111323 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 34"><!-- 131123 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 35"><!-- 131321 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 36"><!-- 112313 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 37"><!-- 132113 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="2"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 38"><!-- 132311 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 39"><!-- 211313 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="1"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 40"><!-- 231113 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 41"><!-- 231311 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 42"><!-- 112133 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="5" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 43"><!-- 112331 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 44"><!-- 132131 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="2"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 45"><!-- 113123 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="3"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 46"><!-- 113321 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="3"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 47"><!-- 133121 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="3"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 48"><!-- 313121 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="3"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 49"><!-- 211331 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="1"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>

			<xsl:when test="$code = 50"><!-- 231131 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 51"><!-- 213113 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="3"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 52"><!-- 213311 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="3"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 53"><!-- 213131 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="3"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 54"><!-- 311123 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 55"><!-- 311321 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 56"><!-- 331121 -->
				<rect x="0" height="30" width="3"/>
				<rect x="6" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 57"><!-- 312113 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="2"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 58"><!-- 312311 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 59"><!-- 332111 -->
				<rect x="0" height="30" width="3"/>
				<rect x="6" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 60"><!-- 314111 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="4"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 61"><!-- 221411 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 62"><!-- 431111 -->
				<rect x="0" height="30" width="4"/>
				<rect x="7" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 63"><!-- 111224 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="5" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 64"><!-- 111422 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 65"><!-- 121124 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="5" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 66"><!-- 121421 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 67"><!-- 141122 -->
				<rect x="0" height="30" width="1"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 68"><!-- 141221 -->
				<rect x="0" height="30" width="1"/>
				<rect x="5" height="30" width="1"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 69"><!-- 112214 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="6" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 70"><!-- 112412 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 71"><!-- 122114 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="6" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 72"><!-- 122411 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 73"><!-- 142112 -->
				<rect x="0" height="30" width="1"/>
				<rect x="5" height="30" width="2"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 74"><!-- 142211 -->
				<rect x="0" height="30" width="1"/>
				<rect x="5" height="30" width="2"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 75"><!-- 241211 -->
				<rect x="0" height="30" width="2"/>
				<rect x="6" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 76"><!-- 221114 -->
				<rect x="0" height="30" width="2"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 77"><!-- 413111 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="3"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 78"><!-- 241112 -->
				<rect x="0" height="30" width="2"/>
				<rect x="6" height="30" width="1"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 79"><!-- 134111 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="4"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>

			<xsl:when test="$code = 80"><!-- 111242 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="5" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 81"><!-- 121142 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="5" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 82"><!-- 121241 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="1"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 83"><!-- 114212 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="4"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 84"><!-- 124112 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="4"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 85"><!-- 124211 -->
				<rect x="0" height="30" width="1"/>
				<rect x="3" height="30" width="4"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 86"><!-- 411212 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="1"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 87"><!-- 421112 -->
				<rect x="0" height="30" width="4"/>
				<rect x="6" height="30" width="1"/>
				<rect x="8" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 88"><!-- 421211 -->
				<rect x="0" height="30" width="4"/>
				<rect x="6" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 89"><!-- 212141 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="2"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>

			<xsl:when test="$code = 90"><!-- 214121 -->
				<rect x="0" height="30" width="2"/>
				<rect x="3" height="30" width="4"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 91"><!-- 412121 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="2"/>
				<rect x="8" height="30" width="2"/>
			</xsl:when>
			<xsl:when test="$code = 92"><!-- 111143 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="4" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 93"><!-- 111341 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="1"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 94"><!-- 131141 -->
				<rect x="0" height="30" width="1"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 95"><!-- 114113 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="4"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 96"><!-- 114311 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="4"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 97"><!-- 411113 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 98"><!-- 411311 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="1"/>
				<rect x="9" height="30" width="1"/>
			</xsl:when>
			<xsl:when test="$code = 99"><!-- 113141 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="3"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>

			<!-- trzy kody do zastosowania wyłącznie na potrzeby sumy kontrolnej -->
			<xsl:when test="$code = 100"><!-- 114131 -->
				<rect x="0" height="30" width="1"/>
				<rect x="2" height="30" width="4"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>
			<xsl:when test="$code = 101"><!-- 311141 -->
				<rect x="0" height="30" width="3"/>
				<rect x="4" height="30" width="1"/>
				<rect x="6" height="30" width="4"/>
			</xsl:when>
			<xsl:when test="$code = 102"><!-- 411131 -->
				<rect x="0" height="30" width="4"/>
				<rect x="5" height="30" width="1"/>
				<rect x="7" height="30" width="3"/>
			</xsl:when>

			<xsl:when test="$code = 'stopAndTerminate'"><!-- 2331112 -->
				<rect x="0" height="30" width="2"/>
				<rect x="5" height="30" width="3"/>
				<rect x="9" height="30" width="1"/>
				<rect x="11" height="30" width="2"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- ++++++++++++++ wariant dla IE8- obsługi kodów kreskowych code 128 zestaw C przy zastosowaniu bloków graficznych ++++++++++++++++ -->
	<xsl:template name="drawBarIMG">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'startC'"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAvSURBVDhPY4CA////Q0gIAwJQuBAWitCoilEVoypGVUA4EBIhNKpiVAV9VPz/DwBnmvoibX9agwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 'stop'"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA1SURBVDhP7cqxDQAgDMRA9l86KOGgp38XkfzxOtSFz/JuM9+GpwBPAZ4CPAV4CvAU4B9F1QbDhPoiW9BlugAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 'term'"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAAzCAIAAAAy8k57AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAOSURBVChTYxgFtAYMDAABZQABiqcCgwAAAABJRU5ErkJggg==" width="2" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 0"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAtSURBVDhP7cqhEQAwDMPA7r+0CuILMguV0AO9CVhUe3gE1R4eQbWHR1B9OeAD1PT6ImuabuUAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 1"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAApSURBVDhPY4CA/2AAZ8MZcPaoilEVoypGVYyqQOMgM+DsURX0VPH/PwBnmvoisR60VwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 2"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAvSURBVDhP7cohDgAwDIDA/f/TXTKCm6sFdYJD8/oadGjQoUGHBh0adGjQocHmmLn6MfoiEpWL0AAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 3"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACnSURBVDhPTcexEcBAEINA99/0mfkAQbR8dK9C06bQtCk0bQpNm0LTptC0KTRtCk2bQtOm0LQpNG0KTZtC06bQtCk0bQpNm0LTptC0KTRtCk2bQtOm0LQpNG0KTZtC06bQtCk0bQpNm0LTptC0KTRtCk2bQtOm0LQpNG0KTZtC06bQtCk0bQpNm0LTptC0KTRtCk2bQtOm0LQpNG0KTZtC06bQtCn03f23DysO9xFdygAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 4"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAxSURBVDhP7YqxDQAwDIP6/9OunUhpL8gEE0Ico2LEpH6do3g+1mJSOTg4QioHx84hXUm1Kw7GxRTxAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 5"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA2SURBVDhP7YqxDQAgDMP4/+kQ1BaRjQPsybK8jIbrp75eZqJycHC0hJeZqBwcHC3hZSbq7yFtEwgrDu6JfY8AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 6"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC1SURBVDhPVcixEYAAEIRA+2/6RUk4oh0eur9PsaC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5taC5tbi7FyR4Kw6yvVNQAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 7"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA1SURBVDhP7YpBCgAgEIT6/6enNQjp2nk8ibiGHLDrw9O1HlashxXrYcV6WLEeVqyHFfs5kg3tvCsOJj/7eAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 8"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAqSURBVDhPYwCC/zAA4sC4yIxRFaMqQADEGVUxqmJUBQiAOKMqBoGK//8BgGIrDntzeXwAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 9"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA1SURBVDhP7YqxDQAgAIP8/+lK6OjoWiYSOCXyCth3VMC+owL2HRWw76iAfUcF7DsqYP86kguR0isOkRI0QAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 10"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADhSURBVDhPTcjBEYAAEMJA+2/6BONk2FfgwX2M2LMcROxZDiL2LAcRe5aDiD3LQcSe5SBiz3IQsWc5iNizHETsWQ4i9iwHEXuWg4g9y0HEnuUgYs9yELFnOYjYsxxE7FkOIvYsBxF7loOIPctBxJ7lIGLPchCxZzmI2LMcROxZDiL2LAcRe5aDiD3LQcSe5SBiz3IQsWc5iNizHETsWQ4i9iwHEXuWg4g9y0HEnuUgYs9yELFnOYjYsxxE7FkOIvYsBxF7loOIPctBxJ7lIGLPchCxZzmI2LMcROxZDiL+vnsBWyUrDg3YTP8AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 11"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADNSURBVDhPTcfBEYAAEINA+2/6TAZlwmt56P70ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9KIxSS8ak/SiMUkvGpP0ojFJLxqT9Ie7FyR4Kw4yehscAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 12"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACsSURBVDhPRcixAYAADMMw/n+6gIfISxs9X3f3n579X+YUlTlFZU5RmVNU5hSVOUVlTlGZU1TmFJU5RWVOUZlTVOYUlTlFZU5RmVNU5hSVOUVlTlGZU1TmFJU5RWVOUZlTVOYUlTlFZU5RmVNU5hSVOUVlTlGZU1TmFJU5RWVOUZlTVOYUlTlFZU5RmVNU5hSVOUVlTlGZU1TmFJU5RWVOUZlTVOYUlTlF9Y+7FzDt+iInAwR3AAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 13"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAySURBVDhP7cohDgAwDMPA/f/TWdeByrTYQafIp5beU/ujNv/IgrKgLCgLyoKyoDZFcgHDhPoiqUmvwAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 14"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACqSURBVDhPVcixDcMAEMSw7L/0RzauoFUR+tW9Pfq6Zq+u2atr9uqavbpmr67Zq2v26pq9umavrtmra/bqmr26Zq+u2atr9uqavbpmr67Zq2v26pq9umavrtmra/bqmr26Zq+u2atr9uqavbpmr67Zq2v26pq9umavrtmra/bqmr26Zq+u2atr9uqavbpmr67Zq2v26pq9umavrtmra/bqmr26Zq+u2avr7v4fffoiQpfoZQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 15"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA3SURBVDhP7YqxDQAwDIP6/9Nu6BDWHmCWOIgzJOE8Zu/rXjVoW4C2BWhbgLYFaFuAtgVof4vkAp5H+iKF1IRWAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 16"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACdSURBVDhPRcixEcAwAISw7L+0Y1O8qHR8t1NPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081+1SzTzX7VLNPNftUs081v3vOD/ox+iKFu4oIAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 17"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC2SURBVDhPTcixEYAAEIRA+2/6ReeCJQKeuJ/PfsyJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8yJK8y7ewGM1/oiT0gpuAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 18"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACkSURBVDhPTcixDcMAEMSw7L/0B7AMntWJv7qndz47RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOwQlR2iskNUdojKDlHZISo7RGWHqOzd/QFnmvoiskMAyAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 19"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAtSURBVDhPY4CA/2AAYUBEgAAuiMJBCI2qGFUxqmJUBTIHITSqYlQFfVT8/w8AMO36Io4VCMgAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 20"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACcSURBVDhPTcnBCYAAEMRA+2/6BAOz5jWQp+7rj5oNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAmg2o2YCaDajZgJoNqNmAursXjNf6Ij9gEssAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 21"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA0SURBVDhP7cqxDQAgEMNA9l86yDzCNX1cXaSsKcnV8cwHUn2Q6oNUH6T6INUHqT5IfT+SDUJd+iJlNdLKAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 22"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAuSURBVDhPY4CA/2AA5YC5EBIhiMIZVTGqYlTFqIpRFSAAYaMIonBGVdBWxf//AJ5H+iIn18oCAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 23"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAtSURBVDhPY4CD////ozGAAJk9qmJUBQoDCEZVjKqAglEVoyqQGUBAJxX//wMAqrzJRXCBcAQAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 24"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC+SURBVDhPXchBCoAAEMNA///plRKQwRxK0ufj7trk56Pw1Ufhq4/CVx+Frz4KX30UvvoofPVR+Oqj8NVH4auPwlcfha8+Cl99FL76KHz1Ufjqo/DVR+Grj8JXH4WvPgpffRS++ih89VH46qPw1Ufhq4/CVx+Frz4KX30UvvoofPVR+Oqj8NVH4auPwlcfha8+Cl99FL76KHz1Ufjqo/DVR+Grj8JXH4WvPgpffRS++ih89VH46qPw1UfhO7t7AQuw+iJ9IIe8AAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 25"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACdSURBVDhPXcgxDgAgEMMw/v/pQ0DkgUytl+b2xiflU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+JeVTUj4l5VNSPiXlU1I+PX9mA9T0+iIuo1TtAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 26"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC7SURBVDhPTcnBCYAAEMRA+2/6ZA0M5jWQR/f1Rw7LgByWATksA3JYBuSwDMhhGZDDMiCHZUAOy4AclgE5LANyWAbksAzIYRmQwzIgh2VADsuAHJYBOSwDclgG5LAMyGEZkMMyIIdlQA7LgByWATksA3JYBuSwDMhhGZDDMiCHZUAOy4AclgE5LANyWAbksAzIYRmQwzIgh2VADsuAHJYBOSwDclgG5LAMyGEZkMMyIIdlQA7LgByWAfPdC2ea+iKrwN/VAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 27"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAChSURBVDhPPcnBCQAgAMNA91+6IobLpwk92Ea+kxAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsJBQjhICAcJ4SAhHCSEg4RwkBAOEsLxZrt5CvoiVddVUQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 28"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC2SURBVDhPXcjBCcBAAITA9N/0BbNwDPGlPpfzcf13wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp54wp7U57wLsPoiIww3uwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 29"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACoSURBVDhPXcixCcAAEMSw7L/0h0shTFwZPbqv35NF+2TRPlm0Txbtk0X7ZNE+WbRPFu2TRftk0T5ZtE8W7ZNF+2TRPlm0Txbtk0X7ZNE+WbRPFu2TRftk0T5ZtE8W7ZNF+2TRPlm0Txbtk0X7ZNE+WbRPFu2TRftk0T5ZtE8W7ZNF+2TRPlm0Txbtk0X7ZNE+WbRPFu2TRftk0T5ZtE8W7ZNF+2TR/t291PT6IiGD028AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 30"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAuSURBVDhP7YohDgAgAIT8/6fPwEhGKyTYOLDtFSA7hOwQskPIDiE7hOwQ8ufYLkJd+iK/9jbIAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 31"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAxSURBVDhP7YqxDQAgAIP8/+k6EHzAFSZIOLDtCbxEOkykw0Q6TKTDRDpMpMNEfo7tAmea+iLhoFOOAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 32"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAtSURBVDhPY4CA/zAA56IxRlWgMkZVoDJGVaAyRlWgMkZVoDJGVaAyKFHx/z8AjNf6IsKVWJkAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 33"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA2SURBVDhP7YoxEgAwCML6/09TBSY2dzLohcsbAOiKnUL15MNOLVoQS04tWhBLTi1aEEtOhwL47bwrDn/PgokAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 34"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAzSURBVDhP7YohDgAgEID8/6dPGTPYjZAYYx3mopPe2NEBOqkDdFIH6KQO0EkdoJO+HjMbgGIrDsiP800AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 35"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACjSURBVDhPXcfBEYAwAIRA+2866uzlAx/g+TiXtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPSdkLaTkjbCWk7IW0npO2EtJ2QthPS/57zAqWfKw4lR3rOAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 36"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAxSURBVDhP7cohAgAwDMLA/f/TbC0o7GwwnMh5kzS38MoUFIFXpqAIvDIFReCVPwvpApHSKw6odRI+AAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 37"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAvSURBVDhPYwCC/zAA4oC56IJQ5qgKIIAKQQCIM6piVMWoChAAcUZVjKqgs4r//wG3DysONTksLgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 38"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAABCSURBVDhP7YrBDcAwEMKy/9KXBiuPeoNI+IEwYn3M5chfyT4C3ZOU7CPQPUnJPgLdk5TsI9A9Sck+At2TlHziMbMBSbUrDnki2W8AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 39"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADPSURBVDhPXcjBCQBBEALByz/pOaUeC9ZDGj/uzjJdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVwxXcoV06VcMV3KFdOlXDFdyhXTpVzx+u4HyH8rDlZPpH4AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 40"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA4SURBVDhP7YqxAQAgDML8/2lEUhdXVzKVJgt04X4+h1kt4lswTXwLpolvwTTxLZgmvgXTxH8V0gZbJSsOUa1MPgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 41"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA2SURBVDhP7YqxDQAwDIP6/9OpBeqUsauZkODIPLbTewSd3iPo9B5Bp/cIOr1H0Ok9gk7/OmYu7bwrDruDQ/YAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 42"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA6SURBVDhP7YqxAQAgDML8/2ksmKGzM1laIGeQ5LOeYf4HIWWNGpDVEFLWqAFZDSFljRqQ1RBS/hnSBdT0+iLiHA0HAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 43"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEnSURBVDhPPclBEgBADAHB/f+nsxilL0Ge3J1PAqjSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKs2b8jWqNG/K16jSvClfo0rzpnyNKnf3AYzX+iJcTrvgAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 44"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA7SURBVDhP7YqxAQAgDIP8/+lYUA9wD1MLWUMePL7nGO5tBWUXVlB2YQVlF1ZQdmEFZRdWUHZhBeXvItmyFPoi5EQnfQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 45"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEOSURBVDhPPYlBEoAAEIL6/6e3IAcuoj4fd0dI/snPiiPkvrDiCLkvrDhC7gsrjpD7wooj5L6w4gi5L6w4Qu4LK46Q+8KKI+S+sOIIuS+sOELuCyuOkPvCiiPkvrDiCLkvrDhC7gsrjpD7wooj5L6w4gi5L6w4Qu4LK46Q+8KKI+S+sOIIuS+sOELuCyuOkPvCiiPkvrDiCLkvrDhC7gsrjpD7wooj5L6w4gi5L6w4Qu4LK46Q+8KKI+S+sOIIuS+sOELuCyuOkPvCiiPkvrDiCLkvrDhC7gsrjpD7wooj5L6w4gi5L6w4Qu4LK46Q+8KKI+S+sOIIuS+sOELuCyuOkPvCiiPkvrDiCLnv3d0LC7D6IkKWeSkAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 46"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADYSURBVDhPTcnBDcQAEMLA67/pvdiJkOcDiN/j7gg9/bWZkC82E/LFZkK+2EzIF5sJ+WIzIV9sJuSLzYR8sZmQLzYT8sVmQr7YTMgXmwn5YjMhX2wm5IvNhHyxmZAvNhPyxWZCvthMyBebCfliMyFfbCbki82EfLGZkC82E/LFZkK+2EzIF5sJ+WIzIV9sJuSLzYR8sZmQLzYT8sVmQr7YTMgXmwn5YjMhX2wm5IvNhHyxmZAvNhPyxWZCvthMyBebCfliMyFfbCbki82EfLGZkC82E/LFN+/+MO36IsedAuUAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 47"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEASURBVDhPPclBEoAADINA///pKkxkTzR9PvfjUL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRL3wC0fUC79wRH13L+jB+iJ69fv4AAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 48"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAArSURBVDhPY4CD////Q1lIbGTBURWjKqBgVMWoCmQGCIyqGFUBAfRW8f8/AOFpyUWfTfHuAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 49"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEOSURBVDhPRcnBDQAwDMLA7r90avCDe0QWebo7rxyxtvoNR6ytfsMRa6vfcMTa6jccsbb6DUesrX7DEWur33DE2uo3HLG2+g1HrK1+wxFrq99wxNrqNxyxtvoNR6ytfsMRa6vfcMTa6jccsbb6DUesrX7DEWur33DE2uo3HLG2+g1HrK1+wxFrq99wxNrqNxyxtvoNR6ytfsMRa6vfcMTa6jccsbb6DUesrX7DEWur33DE2uo3HLG2+g1HrK1+wxFrq99wxNrqNxyxtvoNR6ytfsMRa6vfcMTa6jccsbb6DUesrX7DEWur33DE2uo3HLG2+g1HrK1+wxFrq99wxNrqNxyxtvoNR6ytfsMRd/cBw4T6IiYFQ/cAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 50"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEGSURBVDhPRcnBDcBAEMLA9N/0xmCdmJcFn+6xHbHuG7Yj1n3DdsS6b9iOWPcN2xHrvmE7Yt03bEes+4btiHXfsB2x7hu2I9Z9w3bEum/Yjlj3DdsR675hO2LdN2xHrPuG7Yh137Adse4btiPWfcN2xLpv2I5Y9w3bEeu+YTti3TdsR6z7hu2Idd+wHbHuG7Yj1n3DdsS6b9iOWPcN2xHrvmE7Yt03bEes+4btiHXfsB2x7hu2I9Z9w3bEum/Yjlj3DdsR675hO2LdN2xHrPuG7Yh137Adse4btiPWfcN2xLpv2I5Y9w3bEeu+YTti3TdsR6z7hu2Idd+wHbHuG7Yj1n3DdsTd/VYq+iJENo2YAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 51"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAzSURBVDhP7YrBCQAxAINu/6VzBesE/eojBPGDbff5zwKyQpAVgqwQZIUgKwRZIciXYvsBeQr6IgY/jHEAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 52"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA1SURBVDhP7YrBDQAwEIK6/9I2ihvcV15EeSCpFgecccWKktfgjCtWlLwGZ1yxouQ1OOOlkD4LsPoisb71OgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 53"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAsSURBVDhPY4CA////Q1m42KMqQGBUxagKVNaoClTWqApU1qgKVBYNVPz/DwB0D8lFJBaRNAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 54"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAD/SURBVDhPPYlBCgAwDIP2/093GEEPpZoXd9f1ERWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltBRWU0lZQQSltvbv7eQr6IqDk8IwAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 55"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADuSURBVDhPTcnBCQBBEALByz/pObSbxXoI6vfcHYkdRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSN3YUpW/sKErf2FGUvrGjKH1jR1H6xo6i9I0dRekbO4rSNxzvfp5H+iLDNuZhAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 56"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAD5SURBVDhPPcnBEYAAEINA+2/6HIKyLyZ5cj+70cBe2I0G9sJuNLAXdqOBvbAbDeyF3WhgL+xGA3thNxrYC7vRwF7YjQb2wm40sBd2o4G9sBsN7IXdaGAv7EYDe2E3GtgLu9HAXtiNBvbCbjSwF3ajgb2wGw3shd1oYC/sRgN7YTca2Au70cBe2I0G9sJuNLAXdqOBvbAbDeyF3WhgL+xGA3thNxrYC7vRwF7YjQb2wm40sBd2o4G9sBsN7IXdaGAv7EYDe2E3GtgLu9HAXtiNBvbCbjSwF3ajgb2wGw3shd1oYC/sRgN7YTca2Au70cBe2I0G9sL+xrsXMO36Isaa3d4AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 57"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEOSURBVDhPTYnBDcAAEIK6/9LXIgkpD4P6xN395UtxhIqyFxyhouwFR6goe8ERKspecISKshccoaLsBUeoKHvBESrKXnCEirIXHKGi7AVHqCh7wREqyl5whIqyFxyhouwFR6goe8ERKspecISKshccoaLsBUeoKHvBESrKXnCEirIXHKGi7AVHqCh7wREqyl5whIqyFxyhouwFR6goe8ERKspecISKshccoaLsBUeoKHvBESrKXnCEirIXHKGi7AVHqCh7wREqyl5whIqyFxyhouwFR6goe8ERKspecISKshccoaLsBUeoKHvBESrKXnCEirIXHKGi7AVHqCh7wREqyl5whIqyFxyhouy9u3sBr7f6Igm9BoEAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 58"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADhSURBVDhPTcnJCQBBEMPAzT/pXuQBoXr5+HR3hqcjLHvREZa96AjLXnSEZS86wrIXHWHZi46w7EVHWPaiIyx70RGWvegIy150hGUvOsKyFx1h2YuOsOxFR1j2oiMse9ERlr3oCMtedIRlLzrCshcdYdmLjrDsRUdY9qIjLHvREZa96AjLXnSEZS86wrIXHWHZi46w7EVHWPaiIyx70RGWvegIy150hGUvOsKyFx1h2YuOsOxFR1j2oiMse9ERlr3oCMtedIRlLzrCshcdYdmLjrDsRUdY9qIjLHvREZa9IN/9Ql36ItvE898AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 59"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADHSURBVDhPPclBCgBBEAKx/f+ne1Gc5FAgftxj6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmDp9w9TpG6ZO3zB1+oap0zdMnb5h6vQNU6dvmOndD2ea+iIunjDaAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 60"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAApSURBVDhPY4CD////Q1lgAOGiCI6qGFUBB6MqRlWMqoCBURUDqOL/fwAYJclFMxVUcAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 61"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA3SURBVDhP7YqxDQAwAIL6/9PWQNIHusqkgSOBN8SLX1G8+BXFi19RvPgVxYtfUbz4FcWL/yqSCyR4Kw5tjw/JAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 62"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADVSURBVDhPTclBCsAAEMLA/v/TW2Kodg5B8Pm7j7udvHC3kxfudvLC3U5euNvJC3c7eeFuJy/c7eSFu528cLeTF+528sLdTl6428kLdzt54W4nL9zt5IW7nbxwt5MX7nbywt1OXrjbyQt3O3nhbicv3O3khbudvHC3kxfudvLC3U5euNvJC3c7eeFuJy/c7eSFu528cLeTF+528sLdTl6428kLdzt54W4nL9zt5IW7nbxwt5MX7nbywt1OXrjbyQt3O3nhbicv3O3khbudvHC3kxduevcCC7D6Iq+f8nQAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 63"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA4SURBVDhP7Yq5DQAwEMKy/9IkiKdPj4vTGfk8AOjq6S/sXekrVgR7V/qKFcHelb5iRbB3pf8VwAVbJSsOtbNJZwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 64"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA4SURBVDhP7YqxDQAwCMP6/9NpLShZOSAeELF8HpLqDthv+ptdYFMYbAqDTWGwKQw2hcGmMNhtIV2AYisOAo/1MAAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 65"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA2SURBVDhP7YoxEgAgDIP8/6ejtfSc3ckEOdZZ7hrK5+k9byi3gHALCLeAcAsIt4BwCwj/K5INJHgrDiZsdngAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 66"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA5SURBVDhP7YrBEQAgDMLcf2k0xZOvA5BPuSbroOENg5jPPV4eBtEiIFoERIuAaBEQLQKiRUB8FdIG3EwrDuVieEkAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 67"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA7SURBVDhP7YrBDQAgEIPcf+kqIeZ8OkB5FdJ1yINKvX2WqNQ+QKX2ASq1D1CpfYBK7QNUah+gUn8fyQbcTCsO+kWOsgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 68"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA4SURBVDhP7YrBDQAgEMLcf2kUuTQ+HYB+gNB10APTx+0TwPRRo0YCmD5q1EgA00eNGglg+vgypA1u8isOlZM9jQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 69"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA6SURBVDhP7YqxDQAgAML8/2mkwgXOdClJOUYSeiM7IzSgPUwD2sM0oD1MA9rDNKA9TAPawzSgz4d0Ach/Kw7eAWhOAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 70"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA3SURBVDhP7YqxDQAwDML6/9M0NMgHdIYFI3wmklwPCDOHq8ZCjRoBwszhqrFQo0aAMHO4Pg3pAlslKw5OPIVrAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 71"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA1SURBVDhP7cqxDQAgDMRA9l86QDhlAOp3ZVleh2qutU95qCyHynKoLIfKcqgsh8pyqOznqNpbJSsOIoUfPgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 72"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA2SURBVDhP7YoxDgAgEIP8/6ereDHszu1ECessd9Dj2Vwt1EILtdBCLbRQCy3UQgu10EI/RbIBtw8rDkudv70AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 73"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA0SURBVDhP7YoxDgAgDIT8/6drMabn6g4bhNXUA37K9OsD7hFwj4B7BNwj4B4B9wj431G1ARMIKw44CUNtAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 74"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA0SURBVDhP7YoxDgAgEIP8/6er5GJwdS9TaViHPOD3maEPeAvBWwjeQvAWgrcQvIXgf0WyAdxMKw6YQ3CeAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 75"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAzSURBVDhP7YoxCgAwDIT6/0+nYEC6d/UmDz27eeZdwFcI+AoBXyHgKwR8hYCvEPBfxcwFtw8rDkqTE3AAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 76"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAxSURBVDhP7YrBCQAwEIO6/9IpGHCB+8ZXgr4S6PAW/ArBrxD8CsGvEPwKwa8Q/KlIPsh/Kw76i5dZAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 77"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAApSURBVDhPY0AG////h7JgbGQREBhVMaoCAkZVjKoYVQEDoyoGUMX//wBO0slFJYlhCgAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 78"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAzSURBVDhP7YoxCgAwDIT6/0+nYEC6d/W2U89unu0V4iuE+AohvkKIrxDiK4T4CiH+q5i57bwrDnNfueUAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 79"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEFSURBVDhPNcnBAcBAEILA9N/05lCZH/o9J0LNjXlBqLkxLwg1N+YFoebGvCDU3JgXhJob84JQc2NeEGpuzAtCzY15Qai5MS8INTfmBaHmxrwg1NyYF4SaG/OCUHNjXhBqbswLQs2NeUGouTEvCDU35gWh5sa8INTcmBeEmhvzglBzY14Qam7MC0LNjXlBqLkxLwg1N+YFoebGvCDU3JgXhJob84JQc2NeEGpuzAtCzY15Qai5MS8INTfmBaHmxrwg1NyYF4SaG/OCUHNjXhBqbswLQs2NeUGouTEvCDU35gWh5sa8INTcmBeEmhvzglBzY14Qam7MC0LNjXlBqLkxLwg17+4HH336IvmzQQgAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 80"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACsSURBVDhPRcjBDcMADMPA7L+004JQjg/L5PPj7ro9H0rDh9LwoTR8KA0fSsOH0vChNHwoDR9Kw4fS8KE0fCgNH0rDh9LwoTR8KA0fSsOH0vChNHwoDR9Kw4fS8KE0fCgNH0rDh9LwoTR8KA0fSsOH0vChNHwoDR9Kw4fS8KE0fCgNH0rDh9LwoTR8KA0fSsOH0vChNHwoDR9Kw4fS8KE0fCgNH0rDh9Lw8fe7F8OE+iKL7HYOAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 81"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAySURBVDhP7YrBDQAgEIPcf+mqmGMBv+XVEtYh8Mb9g74F6FuAvgXoW4C+BehbgP6zSDaM1/oiztgPtwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 82"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACcSURBVDhPTcnBCQAxAITA67/pTQgynC/F77LHX1AaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBUBoEpUFQGgSlQVAaBKVBsO0AshT6IiI8UfcAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 83"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAzSURBVDhP7YrBEQAgDIPcf+lYz5MM4Bc+UsMakpznMef9QSwsCgNiYVEYEAuLwoB8FskGC7D6Im/GHaoAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 84"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACqSURBVDhPRcjBEYAwAIRA+286Gm+Y5QU8H+fnWix9FkufxdJnsfRZLH0WS5/F0mex9FksfRZLn8XSZ7H0WSx9FkufxdJnsfRZLH0WS5/F0mex9FksfRZLn8XSZ7H0WSx9FkufxdJnsfRZLH0WS5/F0mex9FksfRZLn8XSZ7H0WSx9FkufxdJnsfRZLH0WS5/F0mex9FksfRZLn8XSZ7H0WSx9FkufxfLec14w7foiq87mBwAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 85"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACcSURBVDhPPcjJEQAgEMMw+m96OcbIr0TrNK+7fiT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfJPdFcl8k90VyXyT3RXJfZGY2+jH6ItE1OC4AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 86"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACQSURBVDhPXYlBCsAwAIP6/0+njIEWPSSC52XbvxFJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCQBkQREEhBJQCThu+0Cr7f6InijmDYAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 87"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACpSURBVDhPTchBCoAAEMNA///pFQxMzSG0ef7cR+N/h8SNIXFjSNwYEjeGxI0hcWNI3BgSN4bEjSFxY0jcGBI3hsSNIXFjSNwYEjeGxI0hcWNI3BgSN4bEjSFxY0jcGBI3hsSNIXFjSNwYEjeGxI0hcWNI3BgSN4bEjSFxY0jcGBI3hsSNIXFjSNwYEjeGxI0hcWNI3BgSN4bEjSFxY0jcGBI3hsSNIfHdvXkK+iKhvETjAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 88"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAC1SURBVDhPXcnBCcAAEITA9N/0BSQuEl/CPO3oN4gVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVNogVvrl7AUJd+iLf6HiFAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 89"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAtSURBVDhPY4CA////ozEgAMEdVQECoypGVaCwRlWgskZVoLJGVaCyaKHi/38APWLJRfVZTRQAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>

			<xsl:when test="$code = 90"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAwSURBVDhP7coxCgAgEMRA///pCCpKattslTtm7AGn1u75/glFwpFwJBwJR8LxI2ACqrzJRYC4FxAAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 91"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAAsSURBVDhP7cohAgAwDIPA/f/TzERUpKYaFOLeDNgmKRR1kkJRJykUddJJwAcYJclF+SmbiQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 92"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADKSURBVDhPPcnBDYAAEAJB+2/6FCQzj00Iz+fulG/+NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0jQ2lb2wofWND6RsbSt/YUPrGhtI3NpS+saH0vbt7AZ5H+iIlkkAIAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 93"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEUSURBVDhPNcnBEcBADMLA9N+0c0KwDzPg77k7r5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5AumaTXyBdO0GvmCaVqNfME0rUa+YJpWI18wTauRL5im1cgXTNNq5Aumubsf6MH6IurWr58AAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 94"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADnSURBVDhPRcnJEYAAEAJB8096dZAp+sXxfE5/ZlJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15sUmtebFJrXmxSa15s0t29e2f6IuVHFBMAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 95"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADrSURBVDhPRcnBDcAADMLA7r90WrBq7pEIeD53l/cjfherROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0TsGqtE7BqrROwaq0Tsenf3AkJd+iKfRTCHAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 96"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAEUSURBVDhPNcnBAcAwDMLA7r+0GyG4Dxh/z90R807ZOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9k7E5At7J2Lyhb0TMfnC3omYfGHvREy+sHciJl/YOxGTL+ydiMkX9Lsf1PT6Ismta3gAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 97"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAADJSURBVDhPTclBCoAAEMNA///pFRsSnEOg9Pm7u3+xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWJX7BG7Yo/YFXvErtgjdsUesSv2iF2xR+yKPWLXz90L5mT6It1UrTQAAAAASUVORK5CYII=" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 98"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAACXSURBVDhPTcjBCcAAEMOw7r/0lWJQqkcIfv7uro0yapRRo4waZdQoo0YZNcqoUUaNMmqUUaOMGmXUKKNGGTXKqFFGjTJqlFGjjBpl1CijRhk1yqhRRo0yapRRo4waZdQoo0YZNcqoUUaNMmqUUaOMGmXUKKNGGTXKqFFGjTJqlFGjjBpl1CijRhk1yqhRRo0yapRR8/27F3kK+iL1YAK1AAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 99"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAIAAADO52WxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAArSURBVDhPYwCC////gygwQGYDAZQ7qgIKRlWMqhhVAQOjKkZVDAIV//8DAAa1yUXFCdpnAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<!-- trzy kody do zastosowania wyłącznie na potrzeby sumy kontrolnej -->
			<xsl:when test="$code = 100"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAYAAABBhfLmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA9SURBVEhL7coxCgAgDATBU1Lk/69NISiCxWKl/U2VHNuix6xRykjRve2/n/uJY3JMjskxOSbH5Jgc00csLTttClsiEU5TAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 101"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAYAAABBhfLmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA7SURBVEhL7cpBCgAhDEPRxPvfOY4i8pfOPg9KS/mWlG+2JLJ9N63fOPeTxtSYGlNjakyNqTE1ph+xNAFsAApiqVFIuQAAAABJRU5ErkJggg==" width="11" height="{$barcodeHeight}"/></xsl:when>
			<xsl:when test="$code = 102"><img border="0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAzCAYAAABBhfLmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAAAA6SURBVEhL7cpBCgAgDAPB1P//OSpoyVHvO1DawpYkr2m2VVW9r/2Pcz8hTsSJOBEn4kSciBNx+oilCVQACmJAOOSrAAAAAElFTkSuQmCC" width="11" height="{$barcodeHeight}"/></xsl:when>
		</xsl:choose>
	</xsl:template>


	<!-- ++++++++++++++ obsługa dat ++++++++++++++++ -->

	<xsl:template name="translateMonthVariant">
		<xsl:param name="monthNo"/>

		<xsl:choose>
			<xsl:when test="$monthNo = 1">
				<xsl:text>stycznia</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 2">
				<xsl:text>lutego</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 3">
				<xsl:text>marca</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 4">
				<xsl:text>kwietnia</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 5">
				<xsl:text>maja</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 6">
				<xsl:text>czerwca</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 7">
				<xsl:text>lipca</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 8">
				<xsl:text>sierpnia</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 9">
				<xsl:text>września</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 10">
				<xsl:text>października</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 11">
				<xsl:text>listopada</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 12">
				<xsl:text>grudnia</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>(miesiąc nieznany)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="translateMonth">
		<xsl:param name="monthNo"/>

		<xsl:choose>
			<xsl:when test="$monthNo = 1">
				<xsl:text>styczeń</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 2">
				<xsl:text>luty</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 3">
				<xsl:text>marzec</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 4">
				<xsl:text>kwiecień</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 5">
				<xsl:text>maj</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 6">
				<xsl:text>czerwiec</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 7">
				<xsl:text>lipiec</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 8">
				<xsl:text>sierpień</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 9">
				<xsl:text>wrzesień</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 10">
				<xsl:text>październik</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 11">
				<xsl:text>listopad</xsl:text>
			</xsl:when>
			<xsl:when test="$monthNo = 12">
				<xsl:text>grudzień</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>(miesiąc nieznany)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- data i czas jako @value lub hl7:high/@value i hl7:low/@value -->
	<!-- obsługa ograniczona do danych przeznaczonych do wizualizacji, brak wyświetlania szeregu atrybutów TS i IVL TS -->
	<xsl:template name="dateTimeInDiv">
		<xsl:param name="date"/>
		<xsl:param name="label"/>

		<xsl:if test="$date">
			<xsl:if test="$label">
				<div class="row_label cell">
					<xsl:value-of select="$label"/>
				</div>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$date/@nullFlavor">
					<xsl:call-template name="translateNullFlavor">
						<xsl:with-param name="nullableElement" select="$date"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$date/@value">
					<div class="row_value cell">
						<xsl:call-template name="formatDateTime">
							<xsl:with-param name="date" select="$date/@value"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:when test="$date/hl7:low/@value and $date/hl7:high/@value">
					<div class="row_value cell">
						<xsl:call-template name="formatDateTime">
							<xsl:with-param name="date" select="$date/hl7:low/@value"/>
						</xsl:call-template>
						<xsl:text> - </xsl:text>
						<xsl:call-template name="formatDateTime">
							<xsl:with-param name="date" select="$date/hl7:high/@value"/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> (nie podano)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- formatowanie daty i czasu -->
	<xsl:template name="formatDateTime">
		<xsl:param name="date"/>
		<xsl:param name="shift" select="false()"/>
		<xsl:param name="side" select="false()"/>

		<xsl:variable name="year" select="number(substring($date, 1, 4))"/>
		<xsl:variable name="monthIndex" select="number(substring($date, 5, 2))"/>
		<xsl:variable name="day" select="number(substring($date, 7, 2))"/>
		<xsl:variable name="hour" select="substring($date, 9, 2)"/>
		<xsl:variable name="minute" select="substring($date, 11, 2)"/>
		<xsl:variable name="second" select="substring($date, 13, 2)"/>

		<xsl:choose>
			<xsl:when test="$day">
				<xsl:value-of select="$day"/>
				<xsl:text> </xsl:text>
				<xsl:call-template name="translateMonthVariant">
					<xsl:with-param name="monthNo" select="$monthIndex"/>
				</xsl:call-template>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$year"/>
				<xsl:text> r.</xsl:text>

				<xsl:if test="$hour">
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="$minute">
							<xsl:text> godz. </xsl:text>
							<xsl:choose>
								<xsl:when test="$hour = 00">
									<xsl:value-of select="$hour"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="number($hour)"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>:</xsl:text>
							<xsl:value-of select="$minute"/>
							<xsl:if test="$second">
								<xsl:text>:</xsl:text>
								<xsl:value-of select="$second"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text> godz. ok. </xsl:text>
							<xsl:value-of select="$hour"/>
							<xsl:text>.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$monthIndex">
					<xsl:call-template name="translateMonth">
						<xsl:with-param name="monthNo" select="$monthIndex"/>
					</xsl:call-template>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="$year"/>
				<xsl:text> r.</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- porównanie dat przez porównanie liczby typu 20150314 -->
	<xsl:template name="isFirstDateLaterThanSecond">
		<xsl:param name="firstDate"/>
		<xsl:param name="secondDate"/>

		<xsl:variable name="first" select="number(substring($firstDate, 1, 8))"/>
		<xsl:variable name="second" select="number(substring($secondDate, 1, 8))"/>

		<xsl:value-of select="$first &gt; $second"/>
	</xsl:template>

	<!-- znane węzły OID -->
	<xsl:template name="translateOID">
		<xsl:param name="oid"/>

		<xsl:choose>
			<!--<xsl:when test="$oid='2.16.840.1.113883.3.4424.1.1.616'">-->
			<!--<xsl:text>PESEL</xsl:text>-->
			<!--</xsl:when>-->
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.1.6.1'">
				<xsl:text>PWZ farmaceuty</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.1.6.2'">
				<xsl:text>PWZ lekarza</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.1.6.3'">
				<xsl:text>PWZ pielęgniarki</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.1.6.4'">
				<xsl:text>PWZ diagnosty lab.</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.2.1'">
				<xsl:text>REGON</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.2.2'">
				<xsl:text>REGON</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.1'">
				<xsl:text>NIP</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.3.1'">
				<xsl:text>I cz. kodu resort.</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.3.2'">
				<xsl:text>I-V cz. kodu resort.</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.3.3'">
				<xsl:text>I-VII cz. kodu resort.</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.2.6'">
				<xsl:text>Apteka</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.3.1'">
				<xsl:text>Oddział NFZ</xsl:text>
			</xsl:when>
			<xsl:when test="$oid='2.16.840.1.113883.3.4424.11.1.49'">
				<xsl:text>Kod kraju</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with($oid, '2.16.840.1.113883.3.4424.2.4.')">
				<xsl:text>Praktyka lekarska</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with($oid, '2.16.840.1.113883.3.4424.2.5.')">
				<xsl:text>Praktyka pielęgniarska</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- nullFlavor-->
	<xsl:template name="translateNullFlavor">
		<xsl:param name="nullableElement"/>
		<!-- HL7 CDA dopuszcza podanie nullFlavor dla każdego kwantu dancyh i każdego elementu
			 obsłużono wyłącznie najważniejsze przypadki i najpopularniejsze kody:
			 NI = No Information. This is the most general and default null flavor.
			 NA = Not Applicable. Known to have no proper value (e.g., last menstrual period for a male).
			 UNK = Unknown. A proper value is applicable, but is not known.
			 ASKU = asked, but not known. Information was sought, but not found (e.g., the patient was asked but did not know).
			 NAV = temporarily unavailable. The information is not available, but is expected to be available later.
			 NASK = Not Asked. The patient was not asked. -->
		<xsl:if test="not($nullableElement) or $nullableElement/@nullFlavor">
			<span class="null_flavor">
				<xsl:choose>
					<xsl:when test="not($nullableElement)">
						<xsl:text>(nie podano)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='NI'">
						<xsl:text>(brak informacji)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='NA'">
						<xsl:text>(nie dotyczy)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='UNK'">
						<xsl:text>(nieznane)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='ASKU'">
						<xsl:text>(nie uzyskano informacji)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='NAV'">
						<xsl:text>(czasowo niedostępne)</xsl:text>
					</xsl:when>
					<xsl:when test="$nullableElement/@nullFlavor='NASK'">
						<xsl:text>(nie pytano)</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>(nie podano)</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</xsl:if>
	</xsl:template>

	<xsl:template name="styles">
		<style type="text/css">
			<xsl:text>

@media print {
	body {
		margin: 0px;
		display: block;
		background-color: white;
	}

	.document {
		width: 80mm;
		overflow: hidden;
		font-family: "Noto sans", sans-serif;
		font-size: 9pt;
		background-color: white;
		background-repeat: no-repeat;
		background-image: none;
	}

	.oswiadczenie
	{
		margin:10px;
		text-align: center;
		font-family: "Noto sans", sans-serif;
		font-size: 7pt;
		padding: 5px;
		border: 0.1em black solid;
	}

	.strona_ikp
    {
        text-align: center;
        font-size: 7pt;
    }

	.podpis
	{
		padding: 20px 0 0 0px;
	}

	.dataIMiejscowosc
	{
		font-size: 5pt;
		font-family: "Noto sans", sans-serif;
	}
}

@media screen {
	body {
		margin: 0px;
		display: block;
		background-color: #f1f1f1;
	}

    .noPrint{}

    .noScreen{display:none;}

	.document {
		margin: 15px auto;
		width: 300px;
		overflow: hidden;
		font-family: "Noto sans", sans-serif;
		font-size: 9pt;
		-webkit-box-shadow: 0px 0px 2px 2px #dcdcdc;
		box-shadow: 0px 0px 2px 2px #dcdcdc;
		background-color: white;
	}

	.oswiadczenie
	{
		margin:10px;
		text-align: center;
		font-family: "Noto sans", sans-serif;
		font-size: 7pt;
		padding: 5px;
		border: 0.1em black solid;
	}

	.strona_ikp
    {
        text-align: center;
        font-size: 7pt;
    }

	.podpis
	{
		padding: 20px 0 0 0px;
	}

	.dataIMiejscowosc
	{
		font-size: 5pt;
		font-family: "Noto sans", sans-serif;
	}

	.doc_header {
		/* background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAvQAAAK5CAYAAADQGrqkAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuM4zml1AAAJQVSURBVHhe7f3LkiTbun8HnR4vQYvHoI/R1wvQ0XPQo8FD0KKB0cOMppAZJgEGJoGkP5iQdPZada9V17zF/Zau7+ces9Iz8osIv8zptxjj2Dh776rKiPDp091HeHp4/Nvz83OGiDh2AarizR9ExDFL0CNirwKMFW8+IyL2IUGPiJ0IcEt42wAiYioJekTsRIBbwtsGEBFTSdAjYjQBoBre9oOI2FSCHhGPiQEAY8LblhHxNiXoEW9MAJgu3jaPiNOXoEe8IQFg2njbPSJOX4IecaICAAS8fQQiTkeCHnFEAgCkxNvvIOLwJegRBywAQN94+yZEHJYEPeJABAAYC94+DBH7k6BH7EAAgFvC2w8iYjoJesQOBAC4Jbz9ICKmk6BHTCQAALzg7ScRMY4EPWIkAQCgGt4+FBGbS9AjVhAAALrB2wcj4mUJesQzAgDAMPD20Yj4IkGPeEYAABgG3j4aEV8k6BFLAgDAsPH23Yi3LkGPNycAAEwXb7+POHUJepy8AABwu3jHBcSpSdDj5AQAADjFO14gTkWCHkctAABAG7xjC+LYJOhxVAIAAKTEO/YgDl2CHgcvAABA13jHI8ShStDjYAQAABg63vELsW8JeuxVAACAseMd3xC7lKDHXgUAABg73vENsUsJeuxcAACAKeId8xC7kKDHpAIAANwy3rERMbYEPUYXAAAA3uIdMxFjSNBjFAEAAKA63rEUsakEPUYRAAAAquMdSxGbStBjbQEAACA+3jEXsYoEPV4VAAAAusU7HiOek6DHiwIAAEB/eMdmxFMJevwjAAAADBvv+I1I0OMfAQAAYPh4x3C8bQn6GxcAAADGjXd8x9uSoL8RAQAA4HbwWgCnK0E/YQEAAAC8RsBpSdBPSAAAAIBzeO2A05Cgn5AAAAAAl/D6AccvQT8RAQAAAKritQSOV4J+pAIAAADEwmsNHI8E/YgEAAAASI3XIDhsCfoRCAAAANA1XpPgMCXoRyAAAABA13hNgsOUoB+gAAAAAEPDaxYchgT9QAQAAAAYC17LYH8S9D0LAAAAMEa8rsF+JOh7EAAAAGCKeN2D6SXoOxYAAABgqnjtg+kl6DsSAAAA4BbwOgjTStB3IAAAAMAt4nURxpegjywAAAAA+HjthO0l6CMKAAAAANfxOgqbS9C3FAAAAADq43UVNpOgbygAAAAAxMPrLawmQV9DAAAAAEiL12B4WYK+ggAAAADQPV6X4VsJekcAAAAAGA5er+GLBL0jAAAAAAwHr9fwRYL+RAAAAAAYLl6/3boEvQkAAAAA48Jrulv1ZoMeAAAAAMaP13m35s0FPQAAAABMD6/7bkWCHgAAAABGj9d9t+LNBD0AAAAA3AZeC07ZSQc9AAAAANwuXh9O0ckGPQAAAACA8FpxSk4q6AEAAAAALuE15NidRNADAAAAANTBa8qxStADAAAAwM3hNeVYHWXQAwAAAADExGvOsTiqoAcAAAAASI3XoUN28EEPAAAAANA1XpcOVYIeAAAAAOAEr0uH6qCDHgAAAACgT7xGHZqDC3oAAEiF9rGxBQC4Hbx2HYKDCXoAAIiN9q/77PC8zd0f1uYi2+/nJWfmU7bb35t3R8v//S7/+9c/Yx6W9pib42Pv7HkOx+cEAJg+Xsv26SCCHgAALlGcDS/2mYeje1Mxvc4OiuuDhfnhwWJbAf7TQvx7tt3/Y37NtrsvuZvdZ/PT0Y8lP5jvj36wf3v6Z+V/G/z053G3Oz2HnuubPe/P4jUcHs2Zvb6lvc7N8fXqddsyHJcHAGDslHu2T3sLegAAeEuxj1T87o5nv0OwF2fTd/uH45nzXxbQPyykvx2DWrFuob19Z/6drbf/XvJ/qOG/28//lf+n//ee5ef6237e3gRsFf2f89emNxcK/d3+t3mfx77egCj2tXxF8Iez/MQ+AIyP087tWoIeAKBXtE9UwCtqVxa6i+Lsdh7uCuAf2S6P9uMZ9u2HPNg3238dYzq2TYK+isXj5m847I2H3oDs9v8Uy2dvTvZ56D9Z4C/MlY3F1gyBDwAwbLzW7dLOgx4A4LbRfvBg//94ycxhme0Oinedwf56cpZd0R47rK+ZKujPqef515/Y3+aX8ujSHV06NLfxKc7ea8yIewAYOl77dmEnQQ8AcNvYvlD/pw+oHlYWqvoQ6s88Xtd5PIdw7zrePbsOenNzqj33xsZkY69jo8t3vtmYPdr4FdfiE/YAMHS8Hk5p8qAHALgttO87ZLrzy+Gg6991RxjdNUYBr8tmTs/AW8AOyg6D/k3Iv3aV+++mwl5jpkt1dE3+r+N1+LoGX5fmEPkAMDy8Lk5l0qAHAJg+tr/TJTT5h1h1Dbxu8Riuff8nPwuf3ylmsAF/akdBfxLvpxYxf+rxzH3+hkgfutUddnQ3n9/H6+917b0uz9Hxh2MQAAwHr5NjmizoAQCmi/Zxug5eEa97u+sSGt11JgS8glPXhOva8A4vXYnikIM+qLP2/8pdb/RG6Z2pcdf46wO2FveHla0jrr0HgGHgtXJMkwQ9AMD0OEZ8finNy5l43Xtdl9EU18KPLd49Owh6i/Jr+iEvFfOeReCvLPBXOnu/PX6wNr89pm6NyWU5ANAvXjPHMmrQAwBMD0W87gcfbif5O7+URpd8jOMSmrr2G/R+xAe9kC9cnrrWn1vcb3R/ft0aU7fF1C0x9cFaztoDQH94Dd3WVkEPADBNbB+XX06jM/GK+J/Zdq8PtJavhZ/C2XjPxEFvYX5OP+LLvg15WY741/6rMA/79/YcIe71gVpdb89ZewDoF6+vm9g46AEApoXt2/J7w2/ys/G7w71F/Ndse/wip2lHfNmhBr0f8/J80JdV3P+Vrdbv7Gd0vb2+vba4Uw5fYgUAfeJ1dl0bBT0AwDTQPi1cUrPML6l5ub2kPmh5KxFftp+g9yO+bJuYP7W41n65+WCvyd607XSt/fx4OY7e1HGcA4DuOe3tOlYOegCAaaF9m76pdVZcUpNHvAJPZ+NvLeLLJgp6i/JL+hEf9GNeNgv6f88Wf/zLfv59ttqWL8dZ2+zgjD0A9IPX4desFPQAANPiYNG2yHYWcBvdZnI094jvwgRBb1F+ST/ig1dCXpZCvYovMV/Wwn6ta+31Yed/LOwfbI7oUhyOgQDQPactfs2LQQ8AMA20PzteWnOYHS+r+XwM+Vs+G+85/KBvE/PSD/p/Hf3LLMJ+s/1ub/qOZ+zza+wBALrHa/RT3aAHAJgG2qftLciO18fvipCfzj3jUxg56C3KPf14P/VKzEuL8Tr6MS9D0B9dWdivLOzXuhRHdznSN9GubU4R9gDQD16zBwl6AJgg2pdZyOtbXPcPFvH68qdwtxpC/rIEfRH0QZ2x151xirDf7+c2t/QNtBwrAaBbvGYPvgl6AIDx8nJpje5ast19tzgNd6uxkMQKDiXo38a8TBP0pZB/E/QvFtfYf7F5pevrN/lcAwDoitNmL/sq6AEAxov2Y/tjyH+zmORsfDMjBr2Fuacf8KdeiXkL8br6MS8t2K/E/PyPOmOvL6nSfez1zbP6cioAgG4p97v8t+OfAwCMEtuVWVTpjPxTttMZ+Z3uUqIgtXjEBvYd9G9DPtgm5uXVkA+WQv51zJej/p29luIynJ29iSy+dRYAoFsIegAYPcUHXhfHu9Z8sojkrHx7IwS9hfk5/Ygv21XMy2PAX4h5P+iDCvsP2Wr7T7bZ3+dfTKW3mAAAXULQA8BIORQfeD08WshbTOXXyRPycSToqwe9VNTrW2c/FR+atTeYfGgWALqEoAeAkaGQ31g0zSzkf1h46ptdCfm4tgx6i/JL+hEfjB/zbUI+6If8W/PbXG70jcMP2f55ZUnPZTgA0A0EPQCMAsVR8e2uvyw4P1oc/uslFL2wxIamC3o/4oN+zMumQe/HvLT4vhL0XrBXtbjN5bdsp3vXH7b57AUASAlBDwADRiH0bFG0tji6y/58u6vFnReMuV5kYg1bBL23Pkw/4Mv6IS/7OjvvhXoVZ7l/2X//2x5bn+v4le0Pq4xvmgWAlBD0ADBQ9MVQG4v5cD95fTFU6ax8Vb3wxAtOI+j9mJcW7FeC3gv1qhZBX6jr6+fr9/b6/7E3pLP8bL29PT3ObwCAeBD0ADAwdAuuXR7y4e41V8/KX9ILT7zgcIL+T8xLi/E6+jEvjxHfQdAX6my9bnGpa+t/Z/vD0ua3rq3nMhwAiAdBDwADQveU31jI31vIf7EYfPcmDpPoxenN2iDovTE96gd80A952TTkpR/y8hjwZ0I+RczPloXFJTgfbdn0hVSzY9QDAMSBoAeAQfDyLa+6e42+ibPB5TVt9WL15qwR9N4YlvQjPngl5IMW4lX1I76sRfuZmPcCvY5vYv4Y8qfmZ+vXX22eP9mbV25vCQBxIOgBoFfyu9c8r/Kz8pvdF4s63frPC8BCLxw70QvaSdpF0PsxL5vGvPQjPngMeSfm2wZ91ZgPzpeK+i82znd8YBYAokDQA0BP6PKa7at7yq82/zK9AHyrF5Cd6cXtZKwY9N64nOitt8IuY96C/VSL7r5i/sV39tyfstX2p72ZnXMJDgC0gqAHgB7QB1+3FjIP+Vn5dX5WXlHnxd95vYjsRC9wJ+PEg96iexhBL/+2x/hgy6vr6ol6AGgOQQ8AHXOwcNEHX+8sHPUhwepn5c/pxWSnesE7WtsHvbeOXvRjXjYNej/kpQV7wqB/E/PSDfe3PpWcLd/bchS3ttQdnriuHgDqQtADQGfkH3zd64Ov3ywK9W2aCjkv+trrhWYnevE7KisEvbfcR7118dq3IS+bnpmXfsxLC/YrQe+FehWbxnw55F/7zh73Y7bZ3WeHw+a4xQAAVIOgB4BO0JnH/f7RYlGX2Px1DDkv+NLpBWhyvSAetFeC3lvGo96YF74N+LJNz8xLP+SDx4iPHPOySdD7IV9Wl+B8zNa733nUP3OmHgAqQtADQGJ0vbzOzN9bKH628AvXy0sv/rrRC9JO9CJ5UJ4Jem9ZTvTGufB1wJ/a9dl5L9DrWDfkg37En2pRv/5kY64vodIdcIh6ALgOQQ8ACTlkh+d1lt+ScmuR8ufMfNCLv270grQTy5E8SAn6a6YNelmcqV9tdAecpb0l5raWAHAZgh4AErHPDodltt39siB8b3GiD7/6QfeiF4Pp9eK0M8vRPAibBb03ri9669qCvKxFeB39iA+ehHwp5tsGffqYf3G2em9j8yPb6Uw9UQ8AFyDoASABlh95zH+zcDs9K99ELxLT6kVrZ5ZDunOdoPdeY0lv/F77dp2+inlpIV5VP+JlKeCdkG8b87KroH88WtwB51t++Q13vwGAcxD0ABAVnUnc72cWhV8t3MrXy8fSC8Zu9GK2E18Fd2pPgt57PSd6Y/Xi23WYJublMeLPxHzboO8q5mUI+seF/e/Fe1ueb8XlN1xTDwAOBD0AREL3l9f18nfZZvfZwi1FzJ/qBWR6vajtzDcBHtvqQe+NzWu9dWZhHrQIr6sf8tKCfYBB78X6NUPIl31aWtRvvmXbne5VzxdQAcBrCHoAiMAh2z8vs+3+p0Xgx2xlQeiFXHy9iEyvF7ed+SbAYzvdoPcivY59Bv2fqF//c4x6rqkHgBcIegBoxXO2z/aHebbZfbeQ15dF/esl2Ep6YZdGLyy70YveTnwT5W0sBb33XCW9MXjt2/Xzal5YiNfRD3l5DPhTLbpjBX2TmJdesF/Ti/nCv+3vi8tvtrsFUQ8AfyDoAaAx+f3lD7pe/h+Ltb9fx9oFvdBLpxea3ehFcGe6sV7FakHvLe9rvXVRmgcW4nX0Qz54DPguYt6Cu4peqFfx3Nn5sn+ifr/IDlx+AwAGQQ8ADdCXRe2y3f7Jwq/48KsXYa8sx9wVvRBMpxej3eiFcme6MS+PQW9j4/6c6S3Li94YF/5Zx978uKAf8cFjwDsRHzXkgxbc1/RCvYpVYj5YRP33bLub52+sAeC2IegBoCa6JeXWYv7B4u5ztlor5v/1JsIuWo67CnpxmEYvULvTi+fOrBj03ut+0RvTwlfr1JsTF/RDXlq0Jwr6Icd88Gn5wcbnh22LXH4DcOsQ9ABQA4v552PMbxXzf72Jr8qeRt4FvUBMoxep3epFdCcmDPpX69ObC1esFPNjD3on2K/7tz3nRxtXvlEW4NYh6AGgIicxv2kR82VPg6+CXjSm0YvW9Hoh3Z1+0Huv863eGJ6sP28OXNCPeWnBnijmZZdB3+Ts/Iv6oOwHG1uLer5RFuBmIegBoBKK+e3+zmL+k0VapJi/5mkMXtALyXR6MZvecmCn00K+UdB743Synrx1fMWrIX8m5qUX6lXsPOalG+uXffijov6jjfdvi/q1ba18+RTArUHQA8BVipj/bWH30SKro5g/9TQOz+hFZXq9wE1vObjj+Tboved+rTcmhX/WjbdOL/g25IMW6xWC3gv1qtaNeS/Uq9jmzHw55gvf2WN+snX3YNvr7rjlAsCtQNADwEXyu9ns9AHYHmPesxyLV/RCM75e6Hbr6zBv6uug957nrd54lNaBt/6uOJiYlxbel/RivYpxg76I+vn6a7bZPxL1ADcGQQ8AZynuM69bU36xyKp5J5vUloPxil5sxtcL3W59G+dNJOg7C/pSoNfVD/q/7U2Col7fJvuUvxkHgNuAoAcAF324br8PXxo1oDPzlyxHZAW9CE2jF8Hd+DbYr1kn6L1lffHPWHvr6ox+yAct2C8EvRfoVW0S8tIL9ap6oV5FP+Zf/vfj8n0R9ftFviUDwPQh6AHA4TnbHxYWdl8tskYS86eWg/KKXozG919Hvb/rxiLSr/kvW+9/5//pPcZrwzK99tX4euvmgn7Iy1LEn4R826DvJeblMdDreinmg/qQ7HLzI//8CwBMH4IeAE5QzC8t6P6xwKrwDbBjsByYFfTjtZl5HG/fZ5vtx5Z+6MiP2Xb3Of/Pc3/vuX7le/NvW/56l2n5IS8t2K8EvRfqVe0l6EuBXtXzZ+bfBr1U1G/4kCzATUDQA8Af9G2T++eVxdg3C6x3x3gqosoLsNFrwV1HL9gv+69ss/tib5AsquxN0mi0OeD+eUX3h0cLyS/58rvj7vg64E/VPDxqsT3mmPdCvYov4e7H+yvnR3Xnm9XnbL0j6gGmDkEPADl5zOeX2Xy3cPr7GFKX9cJstFqA19EP+FP/lW13/9jYro6jfAvoC8hWttzf8uV3x9rRm1+FFu2Jgt6NeWnhfUkv1KsY437zlQxBb96bs9UXe2OpD8nuj+sIAKYGQQ8AhkXYYWkH/e8WV9ViXnphNlotwOvoB/ypBL071o7e/Cq0aCfoj5ai/ZInQa8z9Yv1j2y7X9ra4UOyAFOEoAe4eRTzawuwnxaqOvB7UVVdL9ZGp8V4Hf2Yl0XQK3Bvh4RBb7EdK+blGGJe1gr6k5gPPupDsutf2W6/Oa4nAJgSBD3ATaP42mSb3S+Lrw/HcPKiqr5etI1ei/SqEvTVg96bP4WajyUtuGMEfZOQD3qxXsUuz86XQ77s4+KTzc3746U3nKkHmBIEPcDNojPz2zzml5uPFkx/mV5UxdeLutF5EvCeBP3loPfmxosW7Bdi3gv1qo4l5mWsmJcP83f2er5k692TrSOupweYEgQ9wE1SxPx2d2fRFWL+GE65XmDF14u80UrQH6kW9N58KCzPw6MW3bGC/k3MSwvuKnqxfs085uVJqF/z5cx8vKCXD4v3tsxfs52up38+HNcZAIwdgh7g5lBwWczvHy06P1swncb8Ob34iqsXfmOWoJ9O0HuxXsW4l9pciXsn4D0V9cv172x/0PX0XHoDMAUIeoAb4znbZ/v9LFtvvlgs6Y42FkiN9GIsjV4QjkGC3g96bx0XOvPMgnusMS+9WL+mH+5xYj74tPzMrSwBJgRBD3BD6JZ1+hbYzVa3pwxfHNVWL8zS6kXiECXo3wa9tz5fdOaXRXeMmJdNYl56oX5NL9SrWivoLc6DXrifU2fp5+vv2W6vuclZeoCxQ9AD3Az64qi1xfxPC6sPFkd/vYmlXC+qGukFWzxPQ3GIEvR1gt6ZQydz04v0qjaJeS/Uqxj/Q7BnYl4qzk9ivaq6681qc3e89AYAxgxBD3ATHK+bDx+CPRfz0gurRnrRFtfTWByaBP1Ag96Cu4perFdxLEF/P39ny6lvkX3k0huAkUPQA9wAOlhv9zMLrC8WVX+/CaWzepHVWC/i4nsakH1K0L8Oem99FTrzxZmPXqhXtW7Qe6FexfYxfyHeT7Uobxf0epyXS2/4FlmA8ULQA0weC6zDOltvv1so1Yj5U73oaqwXdWksB2XXEvQNgt6Ze16gV/VVyActuq/pxXoVvVCv4vmYPxP4ivGTOG/mO3sT8tn2D4+2n+AsPcBYIegBJk0R89vtLwuq924sNbYcYa30Ai+d5cBM7S0H/caCfnkMem89FDrzwZlrXqhXsfOYl8dAr2utM/PSjfOG6iz96ke25d70AKOFoAeYMIfnnQXlvUVVcd28Fz1eQLXSi7TGehEY39MQj6YF7Yagd8fcXd/OfPLmbFWbxLz0Yv2SecjLk0ivon9m/krcW4THCPq7P76zx/1ob0Dvs8Nhd1yPADAmCHqAyXLIdvu5HaS/Whj97QbPqV5QNdYLtlZ6URjXN0HeVoLeHefC0/VrOvPIm6dVbRL0XrBfs2nQv8R8/aD3Ar2uL0FfRP3T8qutt7mtPy69ARgbBD3ARNFdbTZ/LrXxz86f6gVVY71ga6UXhXF1o7yNBL07zoWn69d05pE3T6tK0F/2ddDbny1sX7H+me34BlmA0UHQA0ySQ35Xm+Xmk4VNtZj39AKrsV7ANdYLxPi6kV7LGw36gwX91oL+7Lo6XZ+mM2e8OVnVJjEvvWC/ZNNLbeTVcD/VojtVzBfqA7Jfs3X+DbJcSw8wJgh6gMmhb4NdZ6vNZ4ui5jF/SS++autFXSu9cEyjH++ef+Vnqm8t6Pd1g96ZH968q+qbmJcW3uf0Qr2KbWJe+kF/JvItuGMFvRvzs8L7+Ydsvv5xPEsPAGOBoAeYFLrcYZOttz8tiqpdN99GL8RaeRp6rfRCMo1+zEuC/u14na4nszQHvHlW1bohH/RivYpepFe1Scx7cV5XN+blMej/nKXfzjhLDzAiCHqAyVDEfHFXmw9u8KS2HGat9KKvladRmU6C/lLQn6wXZ91786qqTYLeC/WqeqFe1bfxfibmpQV3Z0EfztKvfth61Fl6oh5gDBD0ABPBcj7b5dfNf7EwSnOpzTW9QGvkafi1thyV6SXoCfpLFvF+IeBPteDuMujvZu/sdX4qvmzqmdtYAowBgh5gIui6+fxSm3X6S23q6oVbY0+jsJXl2IwvQR/G+HTcTWfdenOnqk1iXnqhfs2m186/hHz3Me9GfPBVzBfez9/bsn7NdnvNXe54AzB0CHqACaD7RutSm8X6oxs7Q9QLulZ60djYt3HeRII+jOXp+Jon68+bI1VtEvNeqFex/V1tagS9xXbSmHdC/rXvstX24XiWnqgHGDIEPcCoec4/uLbdzbLV+ovFTT+X2sT0NPRa68VkY99G+yUJ+r9sHJxxPFlH3jyoapcxL71Qr2KTs/J9nJkv+9t8Wn2zOTznA7IAA4egBxg1iqd1trJ4Wqzeu8EzNk9jr7VeUDbWD/dzEvQEfdAP+jORb7Hdd9Ar5uX9/GO23Nxlh8P2uH4BYIgQ9ACjRWfndxaMutRGd6UY/9l5z9P4a60XmI30I74sQe8EvbNOvPVe1TohH/RCvYqdfYmUxbYX5010Q16W4t0zBL0uu5kvv2e73TJftwAwTAh6gJGiX4Hv98v8rjbzDu45PxS9IGzkaWi2kqB/4ULQe+vB9NbzNV+FfNCC+5peqFex62+E9eK8rm7IB0vxXjaEfNnHxedstXmwfc7+uI4BYGgQ9AAjRb8CX29/Wdy8exU1XvxMXS8SG1mOzwgS9OeD3luPVS3P9z9acFfRi/UqeqFexSLmKwa9hfYQLrM59W723sbue7bdr4/rGACGBkEPMEJ0dr645/wnixkLFS9wTC+Gpm45GltbjtEGEvSloD8ZW2/dVdGb5164e3qhXsW4Z+cvBL4Fd9ugdyO+bB7ovl7MB+8Xn2x93h/XMQAMDYIeYGQ8PxfBtN7+sMD52w8cRy+Opu5pRLa2FOtVJOiPQX8yjt66qqo3t7149/RivYpeqF/zJdxP4937M9NiO+nZeYvyS3oR/9p3Nhb/2Lrd5vsgABgWBD3AyAgfhF2uP1rMWKh4gePoxdHUPQ3J1p4E+zUJeoJ+OkFvj69vj93o22O5hSXA0CDoAUbFc7Y7LLLV9h+Lm+pn58/pBdMteBqYrT0J+Rf/yta3HvTOeHnrpIreHPbC3dML9Wvml9pIC/A6no32S1p0e4FeRzfkgxbkl/QC/tS7ua6l/5afpQeAYUHQA4wIXTu/2d5bFH18iRUvchrqRdQt6EVnYwn6i0HvjX9VvTlbjnZPL9Sr2P66+YpBb7EdI+alG/JSMX5BL95939lr/ZRtd8t8XwQAw4GgBxgJz/Z/B32J1Oa7hcq7N+HyRy96GugF1dQ9jc9W3njQry3otfzlMfHGvKpv5qg39x29WL9mmph3/sxiO1bQuyEftBi/pB/vvnezD8VlN4ddvq4BYBgQ9ACj4PglUts7CyOdnf/7Tbhc1YuiBnqxdQuWw7SyBP2r8fDGtYpv5qE3vx29WK9i3LvaSO/PTIttL87r6kZ8MI9wXy/Yr6vb5P7Idvs1H44FGBAEPcAIKL5EapEt118sVBrEvPTCqIFecN2C5TCtLEH/ajy8ca3im3nozW9HL9arSNBf9n7xMdtsZ1x2AzAgCHqAwfNsQbg5fonUBzdcaukFUkO9+LoFy5F6UYI+HwdvDKv6Zt55c/qMXqxfs7OYlxbcXqDX0Y34shbg5/RivZrvbN3+tvXMh2MBhgJBDzBw9HXr+ZdIrT+70dJKL5ga6sXYLfgm4ssS9Pk4eONWRW+eufPY0Yv1KjYJ+iLaawa9xbYX6HV0A76sxfc5/VCv7uPia7bZzfP1DQD9Q9ADDBrFUTg7r1vG+fESXS+kGuqF2i1I0BdB741NVd/MJ2+uOnqhfk0v1Kvqx/yZkJcW20mD3oL7kl6g1/FX/p/v87P0h2d9OBYA+oagBxgwukZ1p2vnN18tcBpeOx9DL64a6EXblCXouw16L9SrGP8yG3nmzy24kwa9xfYlvUCvq4JePq2+Z9v9Lc1tgOFC0AMMmNd3tvEjpjO9wGqhF3BTk6BvHvTenHHnZUkv1qvY5Ydgk8a8tNC+pBfodQwxLx8Wn7PV9snWOJfdAPQNQQ8wWBRGy2y5/mqx8i4PDy9ietcLrxZ6cTdWCfr6Qe/NCXfenXga6VVtH/On8X4m5qUFtxfndXVDXlpkn9OL87qWY17ezT9mi82dze/9cd0DQF8Q9AADJZyd151tTiPEC5rB6AVZC73oG4uLlQW9hS1BX11vDrjz7MTTbaSK8WP+ghbcXpw30Y15qcg+oxfoVT0N+cJ39nfvbf38sPW9Oa57AOgLgh5gkLw9O39JL3AGoxdoDfTib+jmZ+gJ+sp6692dUyd620QVmwb9S8yfBv2FwLfg9uK8iV3GvPRi/teTfG9j+E+22S3y9Q4A/UHQAwyQ4uz8vUXO27PzdfTiZxB64dZALwqHJJfctAx6b+6c6M37a+YhLy3Am/g25k//94kW3Emvm5cW2p5eoNf1TdDnMV94P/+cLTePXHYD0DMEPcDgsCDar7LV+psFy99ukFTVC6DB6AVcTb0oHJIE/ZSDvhztPQe9RfY5vUCv66Wgv5t9sHX3KzscdPtKztID9AVBDzA49tlu92QHyY8WH+2CvqwXQ4PQC7kWeqHYl3+uobfAvR3qB723HnO9+XKiN9ev6UV6Vc/H/Jmgt+Du6+y89AK9jm9i/s/lNoW/n95nT4tv2W6/ztc9APQDQQ8wMJ6ft/mHYatcO99GL45614u6Fnrx2KUEfYug9+ZHSW9OV7HNmXnpB335fx+10J56zAfvF19sfc/y780AgH4g6AEGhmJouf7mxkgqvWAajF7sNdSLybQS9P64FHrryJ0DJ3pzuKpepFe1csxLi+3kQW+R7enFeROrBv3v2Udb13c2z7mOHqAvCHqAAfH8/Jxtdo8WO+0+DBtDL6QGpReDDfViM45/ZSuC/qzeunDXdUlvrlY17gdhL2ix3VfMSy/O6/om5M/EfOF7G9/v2W6v21dy2Q1AHxD0AAPicNhm681PC5d418431YupQenFYEO92IwjQe+PS6G3Ltx1XdKbq1Ul6KtbL+jf2XJ/zW9fyWU3AP1A0AMMiO1+ZhH02Y2RvvXiqne9IGyhF53tvPGgt+X3x8Uff3cdn+jNzap6oV5FP+YvxL0FtxfndXQjvqyFtqcX5018FfROwJ+q21eu8ttX6m43ANA1BD3AQNC951fbHxY2aT8MG0MvtAahF4kN9SK0vgS9Py7+mLvrtKQ3F6sa9zaVF7TYTh70FtmeXpjX9VXI5/oBf6quo5+vf9u63x7nAQB0CUEPMBC2+3m2WH+x+Oj/cpumehE2CL14bKgXp+cl6L1x8cbVXW8nenPumnnIy5NIr+LLmfl6Qe8Feh3diC9roe3pBXodm8Z87uyDjfMP249x+0qAPiDoAQaArjtdbX9b7PT/YdgYejE2GL2YbKEXrC8S9N64vBlHbz2d6M2zKjYN+r5iXroRLy2yz+kFeh3bxPzP/D/f27h94zp6gJ4g6AEGgH5Nvdh8s7gZ/uU2VfSCbDBaZL7VicyKesH6IkHvjcubcfTW04nePKtiu7PzTrRf0qLbC/S6ujEvLbTP6UV6HZsGvWI+BL0+GLvePlnQc/tKgK4h6AF65tn+b7dfZPP8w7Djvdzmkl6gDUovMhv4Nl5vO+i1/Kdj4o2bu05O9ObVNb1Qr2rdM/NBL9Dr6Ia8tMi+pBfpdWwb9PJu/jlbbh74YCxADxD0AD2js1nr7Z0FiH5lfQwKxchRL1TG6kug/e276se5q2K0rX9b0H+/waBf25z+ni+/Py5l/XXyR2+elCzeBDf30YL8vO9KFn/2YP/9vH6kV9UNeWmBfU4vzpv4KuQrxHw55IO/Z5/4YCxATxD0AD2is/OKn/n66zEYjkHvhP0U4n6+ep8tbVnXmx9v3U7Rn9lu/3RzZyy1vFruzfaXMyYN9ObL0dVZf1ZyGcHF+qfN7X9sjn9wQ72KbshLi+xLenFe17Zn5oP6YOzT8oetez4YC9A1BD1Ajxye99l2N7ODYOnsfBUtjscX+n/nd/HZ7h6zg72JORw2N+D2eD3xrcWNvVW15dby++MyHPcxtIDdbB+z2fKLxbnuye5H+zndkA9aZJ/Ti/M6vg75djEvi+vov9k2vrL1T9ADdAlBD9AjOjuvM3zu2flLWiCPMeh1dn63n9vBXnfB0AH/FrxlvPGYnprP2908m62+WqDXC3o34staaJ/Ti/Q6Nol56cV84XtbJn0wVtu4xgYAuoKgB+gNfRh2ns1Xn/xor6MF8/ADPwS9bmvHwR6mg+bz1uZ11KC3yL6kF+h1jR/04YOxusyMW1cCdAlBD9ATutxms3uw0K15uU0VLaCHF/cEPUyT6EFvgX1NL9Dr+DrmTYvxKnoRXzb/YOzqLjscuNMNQJcQ9AA9sd+vstX6h0W37qBRivFUWlT3G/oEPUyTpkHvxry0wL6kF+h1fBPzLa+dLxs+GMudbgC6haAH6IE8AHazbL764sd3Si2u+4l7gh6mSdSgt8C+phfpVX0b8/JtvJf1wv2cv2b6jeM/tp1vjqMDAF1A0AP0gO4Astk+WABEuH6+rhbXBD1APAj6FxX09/Mvtp3r1pUA0BUEPUAP6NfRq80vi9wE18/X1WK7m7gn6GGaNAl6N+alBfY5vUCv65uQr3C5jRful7ybfco22+VxdACgCwh6gB7Q9fPL9TcL6o6un2+iRXjc0CfoYZrUCXo34oMW2ef04ryOr0P+qMX3Nb1gv+bv2cdstZ1xpxuADiHoATpG3w7b2/XzbbQobxf3BD1MkyhBb4F9Ti/Q6/o26P2AL+vFehV/WdAv1vfc6QagQwh6gI7RwX+9vc9my49+OI9BC/T6cU/QwzSpGvRuyActss/pBXodm8S89GK9irrTzWz1K9sd9MFYtnWALiDoATrlOTsc9tly88PCeMCX2zTVwv186BP0ME1aBb0F9iW9QK/j25g3Fd1X9EK9qvmdbpbfs+1updEpBgkAkkLQA3TKc7bfr7OFHfgfF3+/DeKxa+FO0MOtQdCfaEH/sPiHoAfoEIIeoEOes0O22T3agb+H21V2qQX8qQQ9TJUqQd8k5qUX6XVsEvPSDfUK/sj/870t32fb1y1tn8e2DtAFBD1Ah+j+8ytdbjOE21V2KUEPE+Za0LsxLy2wz+nFeV1fx7yFesIPwkrFfAh63elmvZ3nJzEAID0EPUBn6Pr5TbawqH1YvDP/LvnXWd1AHqFPtpxLCx6CHqbGpaB3Qz5ooe3pxXkdX4d8tzEfgl53ulltnmxsCHqALiDoATpCB7b8oL/8bKFejvnLQS+9QB6bBD1MlVEEvYX2Jb1Ir6MX9EsL+sOBoAfoAoIeoCOen3fZevuQPS4/WqSfBn3QD/qgF8pjUUGvDwMT9DA1vKB3A76shbanF+h1rRvz0ov0qpZjvtCC/klB/5jf1QsA0kPQA3TE4XlrB7if+fXzfsyf04/7sl5AD88i6BU+BD1MidOgdwM+aJF9Ti/O6/g65IN+wJ/qhXpV3wT9o/78Q/7lUnu+XAqgEwh6gA7QAX+/X9kB7h8L29Pr5+vqR31ZP6j7lqCHaVI56C2wz+kFel2bxrz0Qr2KXswXQf8+m68U9NvjKAFASgh6gA7Ir5/fLewA98WC24vy02iv4+ljvdaP6z4k6GGaDCHo38a8aYFdRS/Uq0rQAwwDgh6gA3S7ys1uZgf88IHY0/A+jfQ6nj7Wa/247kOCHqZJOejvCPpS0N8R9AAdQdADdIA+ELvZPmRPFz8QK0+D3Ps3VT19rEI/truQoIdpUinoLbA9vThvYu8hL48xH4J+tvqd7fYbjVAxUACQDIIeIDnP2eF5k603d9nj8oNFtRffniHCvb9r4uuwL+sHeGwV9P8Q9DA5+g76VzHfx3XzshTzL0H/i6AH6AiCHiAxOtjv9utsuf5pUVv3DjfBcoB7f1/V8uP4+jEeQ4IepsnVoLfQ9vTivK5NY156sV7FazFP0AN0D0EPkJgi6FcWs98tapsGfdnTCPf+TR1PH++tfqDXlaCHaRKC/skLegvtc3qBXtdXQW9xXUUv0qv6JualE/S6F/0TQQ/QGQQ9QGLyg/1ukc2WusNN21tWXvI0xL1/U8fTx3vRD/ZrEvQwTfoK+lcxP5BLbf5I0AN0CkEPkJgi6GcW9J8shr1wjuVpeHv/po6nj/eiH+zXJOhhmhD0jgQ9QKcQ9ACJKW5Z+Zg9LT5mD3aA/+ObgI6pF+Lev6uq93iFfrx7EvQwTULQu9fQW2xf8iXQ3zUyD/lzWnBX0Yv3qp6LfK6hB+gWgh4gMYfDNlttflvQfngd9Ke+iejYhgj3/q6Or4O+rB/yQYIepkkR9EsL2G8W8e+bOavm70T+qqPF+lXt3xH0AN1B0AMkZr9fZ4v1Twve99m9hfupbtyXfRPUsfSi3Pt3VfUer5CghykTztA/Lb9aHOvMefnMe3tfX1pz4pN/1r2K3hn3eBL0AF1C0AMkZrdfZvPVNwvbd27QB92YL/smoGPqhbj376roPRZBD9MlddBLN+Zz/Vivoh/isSToAbqEoAdIzHY/twPbF4vylkEv38RzTE8j3Ps3VT19LIIepkvvQd8w6v0QjyVBD9AlBD1AUp6zze7JDvSfLNovB31V3dAv+yauY1qOdO/vz/u4eEfQwyTpIuiDsaNe+kHeVoIeoEsIeoCEPD8fstX23mL2oxvnbXWDvqwT1nEsh321wCfoYap0GfTybNRbSDfVj/I2EvQAXULQAyTkcNhly81vC9oPbpDH1A36sk5kx/Vy3BP0MFW6DnrpBv2gztK/z+br39n+sD2OEgCkhKAHSMjhsLl4h5uYuhFf1onsuBL0cJsQ9G/VbSvn6zuCHqAjCHqAhOwPumXlDwva9EF/TjfuyzrxHdci8gl6mCrDCHr5NtTr6IV5Uwl6gG4h6AESUtyy8h8L5zgfiG2rG/Rl3SCPY3GXm68EPUyOPoJeukE/kLP0RdDfE/QAHUHQAyREB/nZgIL+VDfqgydB3lbO0MNUKQf9Xa9BLxXTRxXWNfXivL76ptgP2WL9kH+OCADSQ9ADJGSzm9tB/ovF8zCD/pxu4AedWK8iQQ9T5TTo7yysvQBP4cWot7huqh/qVVXQf8yWm0cL+v1xlAAgJQQ9QDKes8027j3ou9YN+rJOuJ+ToIep4gV90IvwmPpBH/RjvYp+qFfzlwX9bwv6le3/Ds+H4ygBQEoIeoBEPNv/rQn6PxL0MFUI+teGoF9vZwQ9QEcQ9ACJeM70pVKP2eOIg/5UN+qDTsS/9l3+AWGCHqZGn0Ev/Zg/mgd2M71Yr2IR9J+yzc62dfs/AEgPQQ+QiOfnvQX9gwW9viV2GkHv6cZ9WYIeJs6loO8/6i2wOz5Trzvc3M+/WNAvCXqAjiDoARJxsKBfbu6zx8W0g/5UN+pzCXqYJkMIeukHvXwb6nX0ov2SCvqHhW3ru5VGpxgkAEgKQQ+QhOf8/suLzZ0d2D644XsrEvQwda4FfVdR78e8VGQfVXDX1Iv2S+qWlU/LH9luv9boFIMEAEkh6AGSEIL+N0H/R4IepglB/1oF/Xz1+/ilUmzrAF1A0AMkgaA/laCHqVIl6KUX4Sm8GPUK7oZ68e6pe9DzpVIA3ULQAyThGPTrX9m9Bf2dBW3Qi91bkKCHqTKqoG8R9V68exb3oJ/ZuHDLSoCuIOgBkqCg32Tz9U+L2ddBf00vhqcgQQ9TpWrQB70IT6Ef9qZFd1O9gD/1bv452+6Wx9EBgC4g6AGS8JztDutGQS+9IB67BD1MlbpBL70Aj60b87l+rFfRC/iyugf9/fzr8QOxANAVBD1AEtoFvfSieMwS9DBVxhf08m2sV9UL+eCvpw/Z4/Jb/htKAOgOgh4gCe2D/lQvksckQQ9TZahBL/2Yl29DvapeyAd1/fxs9fN4hxsA6AqCHiAJ8YP+nF48D1GCHqZKk6CXXoCn0A966Qd7HU+DXtfPLzcP+RfrAUB3EPQASegu6E/1YnoIEvQwVYYe9PJs0LeM+tOgv7egX3OHG4DOIegBknAS9HbwzLWw7VIvrPuSoIep8ifoVxb0Ns/L0X5NL75T6Ad90I/1qoaY1wdiHxZfs83OtnH7PwDoDoIeIAkE/akEPUwVgj4E/YfsafndxmJF0AN0DEEPkIQzQX9OC96u9GK7Cwl6mCpjCPqgH/RmKdDrGoK++EDsr2y31x1u2MYBuoSgB0hCzaD3tAhOrRfeqSToYaq8CXqb7+42fUEvvlPoxrwsBXoTFfR3s0/ZYn3HHW4AeoCgB0hChKAPKg460gvxeL7Lnpafs9XmLo+fW3C3X2aHw8aC77Y+IPj8vM/vQ67l3+XjcFlv7MbkZjfP5/Wjze8325W3TTt68Z1KN+hbXnYj7xe2fW8fucMNQA8Q9ABJiBj0ZU9jIaF+lLfzYfE+j57Z6utNqN9IrLZ3edTfEor59fY+W6w1Bl+u6o1d4aW/61edjf/j8ms+r+/n799uS952fEEvwFOYIur1gdj1jjvcAPQBQQ+QBIJ+qj5ccnHqO4va7zYXVsd5cQs8Z/v9Kltuvlvo/p09Lv5yfXilN3by0t+VPI6/t75S6W0zrt52fEEvvlMYP+jtzfriW7bdLfNLkACgWwh6gCQUQT+zoL+LGfTn9EIikV7c4EtUvtKCfk7Qu74O+kvhfunvSpbG3Vs/KfS2D1dvmz2jF98pPBv0QTfaz/t79iF7Wv3Mig/EAkDXEPQASXg5Q99J0J/qRUUCvci5dcthSdCfD3qZKuilt25i620TZ/W20zN6AZ7Ci1HvRPsl72Yfba7rA7G74zwAgC4h6AGSYFFz2GaL9S878PcQ9GW9uEikFz23LEF/OeiDKaLeWx8p9LYDV2/bPKMX3yk9G/ZOuJ9T3xC72j7xgViAniDoAZIQgv63Heg+uAdR70DeuV54RNaLoFuRoK8b9Nei/tLfmxrzkt46SaE37129bfCM3j4jlX7QSz/ePR8XuiXtiuvnAXqCoAdIwvWgP9U7qHeiFx6J9aJoihL01YJeRgn6oMb+ZF2k1Jvjrt72d0ZvH5HSNkH/++l9Nlv94HIbgB4h6AGSoKDfWdTc2QG/WtCX9Q7wnemFSAK9MJqaBH2ToL8U7uf+3FHjf7I+UunNb1dve7uit39I4dmgrxD1d/OPtr7vudwGoEcIeoBE6OCmg9z94qMdMHUXCP9AWkfvgN+JXpwk0gumsUrQVw96eT3qz/25o8b/qLduUujNZ1dvG7ugty+IrR/0R52IL/u4+Jptd/oGaO4/D9AXBD1AIvRtmavNg8XFJztgxgn6oHfQ71QvUhLoRdOYJOjrBb28HPTy0t+dqHVw1Fs/KfTmsau3XZ3R2wek0I35XAv3M2fqdbvK4nKbbb7uAaAfCHqAROhs1XrzaGERP+hP9SKgE71QSawXUUOVoG8b9OfC/dyfO2o9HPXWUWy9OXtWb5s6o7fdx9aPeWnxfibo7+efssXmnrPzAD1D0AMkQnd7WG+eLCoI+ph6ETVUCXqC/qLeNnVGb7uPrR/z0uLdDfr3NsZfs812lu/vAKA/CHqARORBbwe6h8VnO1imDfpzemHQiV68JNQLqyFI0NcP+uDloJfX/v6o1sNRbx2l0Jujrt62c0FvG4+tH/RBi/hXYf/e1rHNb1vfWu8A0B8EPUBCNrtF9rj8agfjfoK+rBcInenFTAK9uOpTgr550MvL0V4x6INaHyfrJ6Xe/Hyjt61U0Nu+Y+sHvXwJel0/P1/94naVAAOAoAdIyHa/zJ5W/9jB+/2bA2ZfeoHQuV7cJNALrS4l6GMF/aVwrxf13npKqTcvX+ltH1f0tuvY+jEvLeaPUa/r53UnL66fB+gfgh4gIfpV9Gz13Q7c9e9F34dePHSmFzuJ9MIrhQR9u6CX16P+0t+dqHVyso5S680/V2+bOKO37abQD3pZBL1uV7nZzfN1DgD9QtADJGS3X+e3dCPoK+hFTiK98EohQU/Qe/PP1dsmzuhtuyn0Y14WQT9b/cz2h81xvQNAnxD0AAnRvZnnq992wB5H0Jf1QqIzveBJoBdgMSXouwh6ee3vSzrrKaXevHP1toMLettsCs8F/W/bpy23D1xuAzAQCHqAhBwO+2y5vrMDtr4t1j9gjkUvKjrRi59EekHWxoe5Bf2KoG9r9aj3/vzEfL0Ueusshd5cc/Xm/wW97TS2ftBrLHW5zeK4zgGgbwh6gKQ8Z6vNox3Uxx/0nl5kdKYXRAn1Qu2aBH2coA9eD/pr/8bM18uL3npLoTenXL25fkFvu4ytF/Q6UVF8OywADAGCHiAx662+XKq/e9F3qRccneiFUSK9WDsnQZ8i6C8Fe4Wgl/m6KfTWWyq9+eTqzfEzetthbE9j/n7xKdts51xuAzAgCHqAxPy5F73FnXewnKJeeHSmF0gJ9cLtRYLeC/OmvgT7uWi/9vdHbd30EfXe/HH15vUFvW0whYr537P32Wz909bxOl/XADAMCHqAxGwtbvRtikO6F31qvejoTC+QEuqF24sEvRfmbYwS9NLWz1SCXnrbYWwV9Hezj9lq+5Qd8i+TIugBhgJBD5CY3WGTzde/7EA9vjvdxNYLkU714imRRcQR9F6Ut7F6tFeIemnribP0VX1nY/Yl2+7X2fMzMQ8wJAh6gMQcnncWN/d2kP7w5xpU/2B523qR0oleTEWSoI8f9LJ61FfQ1lNZL8JT6M0XV2/OntHbrmKqfZhOTujuXQAwLAh6gMTog2P6FbU+SBaC/pzeQfTW9cKlE724qilBnybo5euoPxf2l/6upK0rgv6ydzo7v/xyvFUlZ+cBhgZBD5AY/Wp6vZtbOHyxaH/3JuLLegfSW9cLl0704qqmBL0F/dIP8ra+jvlz0X7t74/auuo66KU3Z97ozc0zettPLO91dn71k7PzAAOFoAdIjIJ+s1ta2PxjB8bLQV/WO6iiHzKd6MXWFQn6voNeXvt709ZVH1HvzRlXbz6e0dtm2vsue1r8Y/ux+XEdA8DQIOgBEqOg3+5W2Wz53Q6M7914v6R/gEUvZjrTiy5Hgv5v0w/yWF6P+kt/V9LWF0Hvqzt0zVe6dl53tgGAIULQAyTHAuewyRbrX3Zw1AdjdZa+rB/yVfQOvresFzid6YQYQX8M+qCFdQqrRXuFqJe23roMe2/enNWbdxf0tpH6vst/u7je6uw8184DDBWCHqADDs9bCxzd6eZj9uvJIl6+CXvph3sV/YPxbetFTiceA+zWg352GvSdRH3FcL+krbsuz9TLV+F+Tm+uXdDbJuqqs/OL9W+bw5vj+gWAIULQA3TA4XmfrTaPduD+RNB3qBc5nXgMMIJ+aEFfMfZt3RH0hfeLz8UXSdk+DACGC0EP0AG6deVmO7NI+GIx//4l6ssS98n1oielf4LeAvd2eBv0wZRRnyLouwx7N+DP6cy1c3rbQR312R992zWX2wAMG4IeoAOKD8YuLTz+yX6fC/pzEvnJ9UIohgT966D/E/UW1imsFvUVtXDuMuilG++ezlw7pzffq3o/e5//ZnH/zIdhAYYOQQ/QEboG9cniLr/TjRfuVXwV99KP96p6B/Fb1YuhthL0b4M+j/pjgMf2ddCfi/pzf36ihXPXUe/G+zmd+XZOb75f8z6/s83P/AP9ADB8CHqAjtg/b/OvTb+blT4Y29ZIce8d0NGPozoS9GeCXlpYp/Jy0NfQwrnLoJduvHs68+2S3vw+Z/6tsIvP+W8Vn7PDcb0CwJAh6AE64vl5l//6+q78wdiYvop76cd7Fb2D/K3rRdI1bznoVxeCXvZ76c21vz/RAnrsUe/Nac8i5j9k8/XP7MClNgCjgaAH6Ij8g7G7pR2w9cFYC+7YEvRJ9SLpmgQ9Qd9EN949nTl3Tm9Oe97N3tu6Kb4VlrPzAOOBoAfoiGeFzvPODpbf7MDZ4jr6OkaMfOkFwK3rxVOQoL8S9EEL69i+jvZz4X7uz0+0eO4y6t14P6cz7y7pzeGy9/OP+X3n9/vtcX0CwBgg6AE6RGfpdbC8myW67OaaEQPfi4Fb901AWdDPCPqzpgx6mSLqvQBPoRvvnt68u6A3b198Z+vln2yznef7KgAYDwQ9QKc8l+5Hb0HdtxHCPugHwm1L0F8O+mB3UX8S6XW1gO4y6qUb8Z6abxX15qp8WHzMlpu7bH/Q2XnuOw8wJgh6gE6x2NHtKxffLKg7uuymrn8iX/rxXkcvHG5Fgr5G0MtjhMf0ddCfi/pzf+5o8TzFM/XFbSp/5HOVs/MA44OgB+gYHSznq192EP2Y/bSAlm5Y9+2rsJd+sFf1NCBuQYK+WtDLfs/S1w/6sUf9q3lq2/fj8ku23j5lh+f9cT0CwJgg6AE6Rh+OXW4e7AD8+U/QDzrsgxEDvxwTU5agrxn00uI6hZeDXl77+6MWzlMIehnm6f38Q/FB2MMm3z8BwPgg6AF6YLNbWGT8k4fyadSf+iashyahf1aCvnrQB70Yj2W1aK8e9V58p9INeE/NuxrqTkz5B2F1m0outQEYLQQ9QA/s9huLnZ8Wse/diL+kG9VD8U3cSz/eq+hF8pgk6BsEffAY4TGNFvTS4nmQUa95V8PwQdjDMx+EBRgzBD1AD+wPu2y+vrNofbmOvqpuSA9Fgv6VBD1BH1M34E/VvKuoPgir+bndLbPnZ2IeYMwQ9AA9oIPncvNoB+C319HX1Q3rIRgx7KUXzEOXoB9q0AdPIv2Pl/7uqMVz11HvBryn5t5VdanN12y9nfFBWIAJQNAD9MRmq+vov7mRHkM3sofijUT+zQb9oXnQB1OFfbWov/R3JS2exxr294uPto7uj/ecB4CxQ9AD9ITuKDFf/7YwrX8dfRPdsO7bP2Ef9MO9il5Q963OghL0frBXsZsz9cc4f2OFf2PRPNagn63+sXnJpTYAU4GgB+iJw/PBoucp+z2vfx19U92oHpITC/w86JcEfVP7PUsvr/29aeE8tqjXbSpX28fscNjl6wsAxg9BD9Aj290qe1imu+ymqm5cD8mRhj5B3y7oZaqz9LJSsFfRwnksUV98EPanzUnuOQ8wJQh6gB7R9avL9YMb2V3qRvSQJOhHBEHvxXcK3Xj3zOeifqb4IGxxVxvuOQ8wJQh6gB7RQVVn6X/NPrih3ZduVA/FSGEvvQiP6U0H/daCfhUp6GUpxGNaLeorRr/Fc5dBL92AP/U4Fx+Xn7Pl9oHr5gEmCEEP0DM6S6+73XT14dgmumE9BCPFvRfjMSTo2we9TBn1L0EfPIn0P176u5IW0EM8S/+w+JQt1/fZ4cAtKgGmCEEP0DO6B/Rq82AB2N2HY2PoBvYQfBX50o/4KnqRXkeCPk7QB/uN+kt/V1LxXNKL8Nh6AV9WH4Jdrn9l+/06Xz8AMD0IeoCeyS+7seB7WHyxIB3uWfpzulE9JHsMfIJ+SkEvL/1dSYvooQS9rpufrb5l292C6+YBJgxBD9Azup51f9hlT6sfFqDjOksfdEN6KBL0HUPQdx308lzMPy2/ZJvdzPYzutSGs/MAU4WgB+id5zzqF5sHOwh/doN5bLphPQQ7j3uC3gvzNqaKepki6r34TuXrmH9v4/U1W++ebP+i+80DwJQh6AEGwna/tqjQPenHd9lNXd3YHoIRg18S9ImCXlpYp/Jy1F/6O0eL667Cvhz0T8tP2XqjO9rw5VEAtwBBDzAQ9M2x8/WdReA4L7tpqhvWQzFC4D8sPmeL9S8L3MdsPRI3uyd7g/loPhTu7L9X9iFb737ZMn+2NzPxg17+iXppcR3bl2g/F+/n/tzR4rrrs/UPi4/HD8FubM9CzAPcAgQ9wIBY7xbHb46d/ln6oBvSQ/FN0Hv6IR/UWfq7+XsLrQ+jUDE4W32yN5cfzPfmu+I/V/VUzM+OAZ7C1GfqLwd9TRXZJ9GdysfFJ3szpW+C1ZdHEfMAtwJBDzAgDoddfpb+5+xD9sOCUnoRfCu6kT0Ea0b9Jf1r7/vzLv8gpe6M8pf5rxctnpvqBXlbCfq3Piz0JizEPHe0AbglCHqAQfGcrbYzi6ovf4L+VC98b0E3rIfihOJeQf84pqCXxwiP6eugPxf15/7cUcF9EuAxfVjoNynfsu1+bjHPl0cB3BoEPcDA2O032dPqZx6xXtCfehq+t6Ib1n36KuqDfrhX0YvtLgxB/3Qa9C3C3gvyWPZ7lr5+0KcJe62vz/lnH4h5gNuEoAcYGPrm2MXm0aLuowX7+zcBX1UvgqeuG9p9O7LQvxr00iK6iV6QxzBl1MvrUX/p70tafMcO+oe5Pq+g21M+2r6D21MC3CoEPcDgeM42u0V2v/ia/Zw1D/qgF763oBvXQzBi3EsvytuYMuilF+Qx7P9M/bm/K5kHeKEX53V9sHU1W37J1tsHi/ltvu8AgNuEoAcYILv9Or/s5lfpw7FN9WL3FnRjeggS9EkcW9C3j3pbR8tP2Wp7l+0P3J4S4NYh6AEGiC670X3Lf88+ZT8f25+lD3rhewu6YT0UIwa+F+h1Pfuh2FMtoJvqBXlbb+2ym8fFh2yx+ZntDmtLeWIe4NYh6AEGij4c+7j8YdH3IfvxaEHuaUEYQy+Cb0E3sIdgj5FfOeilRXQTvSCPYR710sI6hdejvULQB51Ir6qum1+sv2f7/YrbUwJADkEPMFB0oNYtLH/PPvsxX9YiMKZe/E5dN6yHYodxXw76uRfxZS2em+jFeAz/BL08RnhMX4K+v6jXveYXq+/Z7hBinrPzAEDQAwwaXUuvs/Q/L52l97QIjKkXwFPXDeu+fRX2QT/eq9g66IMW0HX1gjyW/Z+lrxj1CvSjXryf+rB4n83X37Mt3wILACcQ9AADJtzC8vfsix/u57T4i6kXvFPXDeq+Jegr2U3Qn4v2S393oiL9qBfwZYtvgbWY3y24zAYA3kDQAwwYnYXb7FbZw/K7RWbNs/SeFoWx9CJ46rqRPQRjRv5J0FeKegvoJnoxHsPRXHajUD/qRXzwcfEp/wBscWaeL44CgLcQ9AADZ3/YZvP1faY73riR3lQLwRh64XsLumE9JBtGvhf0Ywz7fs/SyxhRb+ti8TFbbe6y3WHDZTYAcBaCHmDg6Nfr693CDv7filtYenHeVgvAWHrxewu6UT0Ua4T9b/s3jws/6K9GvUV0E70gj2H/UV9Ri/fTqNf18k+rz9ly8zs7KOb58CsAXICgBxgBOku/SHGWvqoWhTH1gvgWdGO7b1/F/rurQS/dmA9aRDfVi/K2dnf5zbmwv/R3JUtR/7jU9fL/5N8Au99zZh4ArkPQA4wAHdC3+5Ud6CNdS99Ei7+YesE7dd2g7tvToL9wyU3QDfmgxXNTvSBv65+glxbWMX0d8+ei/dLflQwxvyjuMb/dz7KDvZHntpQAUAWCHmAkHJ4P2XLzlN3NdcebRJfeVNVCMJZe+E5dN6wHYpWgl27MBy2em+gFeQzHcpZe3/5a3JZyzodfAaAWBD3AaHjO9oeNhc8vC6+Pfmj3oUVgTL0AvgW9uO7DKpfcSDfkgxbPTfWCPIapgl5eD3p56e9svJef7A37r2x70J1suC0lANSDoAcYEc/ZIVvlZ+m/Wky/z757gT0ULQ5j6kXwlPViuwv/BL0FsBfyZd2YL2uPUVcvxmPZzVl6L9jlub/Xm6cv2Wp7b2/Y13z4FQAaQdADjIz822NXPy2+PuZBf6ob10PQYjGWXgBPXS++U9h30Ae9IG9ryrP0slrUl/+3Yv5r8eHXw8a2bmIeAJpB0AOMDH177HJbnKX/fjxLT9BPXy++UzjloJdeiMeyetDrcwrhTjaPfPgVAFpD0AOMkN1+Y8F0ZwH24U3Ql3XDeghaOMbQC99b0YvxGNYJ+qAb80F7nCZ6MR7D/q+l/9tex8dssf6RbXeL44dfiXkAaAdBDzBCdBvLzXZpcfCPhfvbs/TndON6KFpMxtSL4FvQi/Q6Ngl66ca8tMdpoxflbez7spun5Yf8y6J0iQ3XywNALAh6gJGyP+yz1WaW/Zr519JX0Q3rIWmB2VYveqeuF+pVbRr00g16aY/VVC/K25pHvSyFeCwvnaWfrT5nm91jdnjeEvMAEBWCHmCkKAh06Y2+bOrHU/Wz9Jd0o3oIWmjG1IvgqevFu+fQgl56Ud7GlEEvXwd9cb38Yv2PxfxTHvNcYgMAsSHoAUaMPiC72s6zu9kXN9Dr6sb0ELTQjKkXvFPXi3dPgr69Iej1ra+6JWV+f/n9It9eiXkASAFBDzBidJb+8LzLnlY/s5+zyx+QbaMb2UPQAjSWXgTfgjGDXrpBH7THbKIX5TFMGvbLdzYeuotNcUtKLrEBgJQQ9AATYLPTB2S/WeTGufTmkm5Y962FaEy98L0Fkwe9tMdtqhflbY0f9O/scT9ky/X3411s+NZXAEgPQQ8wAXT279K96VPohvWQtDiNoRe+U/aXBX3+hUcWul6wV9EN+aA9blO9IG9rrLP0T4viDjb5veV3fFEUAHQLQQ8wEXaHncXEvQXZZzfAu9KN6yFp0RpDL4anYIygD7pBL+2xm+pFeVvbBb3uXvM+/9Drentv2+Eqv1aeS2wAoEsIeoCJkN+bfre0OPlp4Zruevq6ulE9BC1eY+rF8Ri92aAPutHuqTPyNk6rT9lq8zPb7uf5HWyeMy6xAYDuIegBJsRed73ZzLLf+V1vurn05ppuTA9Bi9eYenE8RjsJemmP30QvyGNYPepDyH/Mlutv2Wb7mO0P6+O18pyVB4B+IOgBJsb+sMsWmweLs+ZfONWlbmwPRQvcmHoBPTRjBr10Yz5oz9FEL8hjeTno/7bnt5DffM/Wu3vb1lYW8roVJQBAvxD0ABNkd9hanPy0QBtH1Jd1w3ooWvDG0AvpoRg76KUb82XtuerqxXgs/ZjXbSi/ZOvtnW1fSwv5nW1pnJEHgGFA0ANMEH0gb7NdZPfzfyxEh3HpTRPdqB6KFr+x9MK6L8cS9EEvyNsaztI/LXV5zXtbhs/Zcv0j2+5mpevkiXkAGA4EPcBEyS+9Wd9nv2f93vWmjW5ID0WL31h6Yd2XBL083oJS3/JqIc918gAwdAh6gImis/Tb/drCZJyX3lzSDeyhaFEcQy+2u7CXoJf2fE30g7ypxS0o5+vP2WrzKz8jv7dtSLehJOQBYMgQ9AATRmcU17tFdr/4ZiE83ktvrumGdd9aHMfUi+8Upgj6oBvyQXu+pvpxXte/7TXqA68/s91+aRG/y98UE/IAMAYIeoCJozBZbB6Pt7L0g3gqumE9FC2WY+rFeAxTBr10Yz5oz9lEP9CrqDPy7/6ckd/pGvnDVltNsfEAAIwEgh5g8jxnu/3GwuUu+5lfTz/dM/XndAN7KFpEx9IL9Lr2GvTSnreJfrB7hoj/VNx+cnuffynU/nnDLSgBYLQQ9AA3QPEtsqvscfHTwm9a19NX0Q3poWgRHUsv0Os65aCfLd9li5XOxv+w7eHB3ugu+HZXAJgEBD3AjaCzj6vtLLubf7X4u72z9GXdsB6KFtVt9UK9qqmDXrohH8zDu5lvI15fBKUz8rpjzac85Le7p+zAHWsAYGIQ9AA3xOGwy+Yjv5VlCt2w7luL61h64X7OEPRzC2IvxmPohvyp9vx1fR3yuqzmQ7ZYf7E3svqg6+IY8QAA04OgB7gpXq6n//X0Kftm4egF7q3rBvZQtOiOZV9BH3RDvuwx1KurO9W8z5YW8WuL+O1+lu0P4dp4zsYDwHQh6AFuDJ2lzK+nX+p6+g951Jf1AveWdaN6KFqAx/Q06BcW1V6Ix9KN+LJvgv3UcEnNO4v4rxbxd9luN8/2+W0ni5DX50eIeQCYOgQ9wA1y+HN/+n8s5N5GPWF/Xjesh+JJoDfx50nQp4566cZ8sBzvujuNuVi9zxZrXRP/Ldtsf1vEPx0jfpe/YSXgAeDWIOgBbhB9Yc5eH5LdPGV3cz/qvZhFi94h6wR6XYcR9BbwR/OAX3/ML6NRwOtSms3uLtvuLeIPFvH5feMJeAC4bQh6gBtGX2m/3Myyu8U/2fczZ+pP9SIXLYaHrBPu5/SCPnXUl2N+vvrbAv6DzUt9mFVn4H9l2/wWk7oevgh4rokHAHgNQQ9w4+jym8XmKfs9/2pR/96N+Gt6gXvrumE9BJ2IL3su6ONEvc64v7hY6+y7PsSqL3nSB1kt4Hc6A39f3CP++IFWXUaj3yoBAIAPQQ8A2e6wtbB6yH4p6h+bRf2pXuTeum5gD8UKQX8p6hd5oEudYZdFrBfBrktmLNrXny3cv2arzT+leNcHWXX5zMrC3QI+s4A3izPwRDwAQBUIegCwbDrkt7OcW9T/nn2x+Gwf9V7Q3rpuSA/JP0GvL2JSiJ/64bUbC/WNzq7rA6qfs9XWYn37T36pzHr7I9vk17v/zra7e/Mxv2xmd5gfL53Rlzvp8pnyB1kJeACAJhD0AJCj2/vt9ttssXrIfuVfPBXnTP2pXujeum5c9+TPp/fZ40Jn0r8XQR7Mz6ZbnO8tzv94jPT9vNBCXeps+96CXZfMHPJo12Uz4dIZ4h0AIDYEPQD8QVG/z79N9sHC7rMb5DH14vbW9SK7S4ug/5Ztd/pmVcV4UGfSZYjzY6DnPhce/+8l2Il2AIAuIOgB4A37/TZ7Wv62uPtkkZnmTL2nF7i3rBfcqc2Dfvk92+3Xx9kAAABDh6AHAIfnbLtbZ0+r3/nlN158d6kXu7euF+MxJOgBAMYHQQ8ALrqUYrtb5VGvM/VeaHelF7S3rhfjMSToAQDGB0EPAGc52P9t9isLvF/Zj6ePbmx3rRe3t64X5k0l6AEAxgdBDwAX0RdPrXer7H7xYzBRH/Ti9tb1Ir2OBD0AwPgg6AHgKrqDycai/mH5c3BRf6oXubeuF+7nJOgBAMYHQQ8AldCZ+s1umT0uflj0DTvqy3qBe+t6IR8k6AEAxgdBDwCVUdSvtvPsfvF98Gfqz+kF7i1L0AMAjB+CHgBqsX/eWdTPsgeL+jGdqQ96UXvLEvQAAOOHoAeA2hSX3+juNz+yH7Nxnqkv64Xurfrj0YLe3qwR9AAA44GgB4CGPGdbi77H5c+suE99d98om0ovcG9Ngh4AYHwQ9ADQmOLuN8tspm+UnX+xIBx/1Jf1gnfqEvQAAOODoAeAVjw/7/P4W6wfsrv5P9n3pw9uHE9JL4SnIkEPADA+CHoAaM2z/d/+sM2Wm1n226L+h0X91M7Wl/VCeCoS9AAA44OgB4Bo7J/3edTfL77ld8CZctSf6sXxGCXoAQDGB0EPAFE5WNTruvqniV5XX1cvmoesgl63JCXoAQDGA0EPANF5fj7kd8CZb+6z30T9K72IHpIEPQDA+CDoASAJL9fVP2Z386/5N8t+fyLsT/Wiuk8JegCA8UHQA0BCnrPDQdfVP2UPyx/Zz9k07lcfUy+q+5SgBwAYHwQ9ACRHl+Dom2WfVr+yX7PPFo5E/SW90O5Kgh4AYHwQ9ADQCfoSKl1XP1vdZT8t6r2QRV8vvFNJ0AMAjA+CHgA6Q9fV7/bhuvrifvVewKKvF+CxJegBAMYHQQ8AnVJ8WHaXrbaz7HGh6+p1tp5LcNrohXlTCXoAgPFB0ANALxTX1S+z2ep39nv+1WKSqG+qF+ZNJegBAMYHQQ8AvaHr6sOtLYuo5xKcGHqhXlWCHgBgfBD0ANA7h+dDtt4ts/v5NwvKj26kYjO9aL8kQQ8AMD4IegAYAM9/vl12sXnK7hffsp9PhH1KvZiXBD0AwPgg6AFgMOgSnMNhl5+t17X1+T3r+XbZTiToAQDGC0EPAAOjOFu/t6Bcbp/yuPw1+0TYJ5agBwAYLwQ9AAyUIuz1DbPz9X1+Gc4Phb0FpxekGEeNL0EPADAuCHoAGDz5ZTjbRf4ts/ndcDhbn0yCHgBgfBD0ADAKdLZ+p1tcbmd5cP584mx9Cgl6AIDxQdADwGjQt8wenvfHL6S6y29zWVxfz/3rY0nQAwCMD4IeAEaH7oajs/XhMpz8+nrO2EeRoAcAGB8EPQCMFn0h1W5fhP1jfpvLL5ytbylBDwAwPgh6ABg9OmO/f95lq+08e1z+yL+UirP1zSToAQDGB0EPAJPh+XmfbXfrbLl+zB4XP/Mz9j8eOWNfR4IeAGB8EPQAMDGKb5vV/euXm1n2tPyV/VbYcylOJQl6AIDxQdADwEQp7oizPayzxeYpe1j+zH7PCftrEvQAAOODoAeAyWNpn+0Om2yxtbC3WP05+2xh/5EvqHIk6AEAxgdBDwA3xHO2P+zyS3Eelr+4K44jQQ8AMD4IegC4OXQpju5jv9ktsvnqLrubfeXDs0cJegCA8UHQA8DN8vx8yPZ7hf3Swv4+u5//U1yKY1Hrxe4tSNADAIwPgh4Abp78PvaHXfHNs+v77GH5I/8A7c8b/PZZgh4AYHwQ9AAARxT2h8M+21rMLrezbLa6y7+o6m7+z/HLqqZ/WQ5BDwAwPgh6AACH5+NtL3Wt/Xo7z+P+fv69+LKqCX+QlqAHABgfBD0AwEUs7Z+lxf1+l6038+xx+Ss/a//rz+0vP0zm0hyCHgBgfBD0AACVCGF/yK+3L76J9il7Wt1ZABeX5Uwh8Al6AIDxQdADADQgv97+eZ/fJWdrca8P1C7Wj9nT8vefS3N03b0uzxlT3BP0AADjg6AHAGhJOHOvD9Tu8ttgrrLVprju/mHx7fWZ+6f3hXnkDy/0CXoAgPFB0AMARCcE/s7CeJN/gdVyq8tzfmf3FsvhlphDvOc9QQ8AMD4IegCA5OjynEO2OyjudXnOPFtsHrP5+j6PfN33/m5Ruga/x8gn6AEAxgdBDwDQGc/F/+X3u99l+8M2P4Ov+94r8vUh2yLyf+VR/eqDth1FPkEPADA+CHoAgN54zi0+YFtcgy/1Qdv8OvztLFusH7LZ6nf2uPyZ3S++5ZH/e/a1+NDt7FP2Y1Z88DaoL79SlAfrXqdP0AMAjA+CHgBggIQz+boWP78eX5fs5NfjL7P1ZpYtdcnO6i57Wv7KY1+3ztT1+ffzl+j/bdEvdZZf8f/ix/wOPL6fssfFz/y5AABgHBD0AACjIk/9k+Df5/fG15l9nVnPo19uF8WlPPnlPHoT8JQt8st6dNb/Lj/z76l/p8cDAIBxQNADAEyGP6lfiv3gPlf3zs/vn6878Djqz/X3egwAABgHBD0AAAAAwIgh6AEAAAAARgxBDwAAAAAwYgh6AAAAAIARQ9ADAAAAAIwYgh4AAAAAYMQQ9AAAAAAAI4agBwAAAAAYMQQ9AAAAAMCIIegBAAAAAEYMQQ8AAAAAMGIIegAAAACAEUPQAwAAAACMGIIeAAAAAGDEEPQAAAAAACOGoAcAAAAAGDEEPQAAAADAiCHoAQAAAABGDEEPAAAAADBiCHoAAAAAgBFD0AMAAAAAjBiCHgAAAABgxBD0AAAAAAAjhqAHAAAAABgxBD0AAAAAwIgh6AEAAAAARgxBDwAAAAAwYgh6AAAAAIARQ9ADAAAAAIwYgh4AAAAAYMQQ9AAAAAAAI4agBwAAAAAYMQQ9AAAAAMCIIegBAAAAAEYMQQ8AAAAAMGIIegAAAACAEUPQAwAAAACMGIIeAAAAAGDEEPQAAAAAACOGoAcAAAAAGDEEPQAAAADAiCHoAQAAAABGDEEPAAAAADBiCHoAAAAAgBFD0AMAAAAAjBiCHgAAAABgxBD0AAAAAAAjhqAHAAAAABgxBD0AAAAAwIgh6AEAAAAARgxBDwAAAAAwYgh6AAAAAIARQ9ADAAAAAIwYgh4AAAAAYMQQ9AAAAAAAI4agBwAAAAAYMQQ9AAAAAMCIIegBAAAAAEYMQQ8AAAAAMGIIegAAAACAEUPQAwAAAACMGIIeAAAAAGDEEPQAAAAAACOGoAcAAAAAGDEEPQAAAADAiCHoAQAAAABGDEEPAAAAADBiCHoAAAAAgBFD0AMAAAAAjBiCHgAAAABgxBD0AAAAAAAjhqAHAAAAABgxBD0AAAAAwIgh6AEAAAAARgxBDwAAAAAwYgh6AAAAAIARQ9ADAAAAAIwYgh4AAAAAYMQQ9AAAAAAAI4agBwAAAAAYMQQ9AAAAAMCIIegBAAAAAEYMQQ8AAAAAMGIIegAAAACAEUPQAwAAAACMGIIeAAAAAGDEEPQAAAAAACOGoAcAAAAAGDEEPQAAAADAiCHoAQAAAABGDEEPAAAAANCQ/8T4nxn/e+P4R51D0AMAAAAAjBiCHgAAAABgxBD0AAAAAAAjhqAHAAAAABgxgwz6Z/3f8yE7PO+xkgcbr+d85OpS/BwMDa0WrRt5ONi2cDT8GXTD6fiX1wN0R5j3p+uA9QAAUDDIoFekbnbLbLF5wAouN0/Zdr+2g9vhOIL1CAfGqdkW7zGvWQ4Oud/vs91u98ftdpttNps3rtdy/cbVavXG038THkOPr+eTIXjGzOnYVvF07E/HX5bHXZ6O56lV14HWbVgHUxj/riivt7C+wnZyOs5jXgd6JYeTORrbrvG2uxj2sd5SLEewzvKEMU1pE7zHmbpjY5BBvztss9nqLvs1+5z9xKvezb9my/WT7Th2xxGsjiatDoCnB8exqwO7dqRNaTIuiozlcvnKxWKRzefzP85ms+zx8TGJenw9nywHzxgjU69Tr7k8vtfse/yfnp7yx9fz6Ln1ehSZYfy1TGMZ/1SEMdjbXNzZmGg7La+7sL40lt4YX1M/p58vbwN6jiHMf8W85oM3T2OoZdVydknYR3qvp6lhu2mz/26C5kmKdaPHrHo80vwM20Qqtc6abAd6/d6b7KnaxxxsyzCDfr/Nnpa/su+P77Nvj+/wij+fPmXz1YNNvvpBfzg82+TdND6ADlWFlTbIpgdwbcjaqOsG4MPDwxvv7+9feXd3l8Tyc+h5w2vSMoTA0cFCO3Qtn+wzcC6h16X1V2deno67LI9J0Bu7WIbn0HPrNYXI14Fd82mMb67aouXU8mrZNf+0Xpc2F+c2J8P69daXN77XLP98WAen87+v8debGL2Oc3O1rRrLNvu8JiyXq/x5vdfTVI2P3pRpvnSJttEU60aPqXHSnLuG/o3maZ39Xh31uHr8JnNE202Yv7eglrXrN8htIegn4M+njxb097YzaBL0xQ5EOx7vADlWdUBvs0EW47LON2zv8cdkOXC0Q9fBUutcB/8Q99rBDykv9XoUwFOYl2H8NZdCXGrZQlzmY99hhHWBlkfzSssXzjhquZ9s+R9sHO5tvYZx8cYspuF5wvwP4x/eXHU19qn3tXpcPb6epws0btqXpFiH2la6fHMSlsV7LW0N66XKsmjdLRbLZHNE60pvXJqMq7aXVGM0RLWvrtoPNp5z8785/s/eIOgnIEHvq4N304NCMS7TCPpTQ9yUAzOPS1tmXRYwBLTOpjgvvbjUHO0qwrpA604HQkWzAkDLqeXNw08649KdL+MffnOiud/F+GtcNCap9ilaLo131Qhpi55HY5ivV+f1tFHrp8s3J4pVLYv3Wtqq9a31XuU4pOUl6IdhzaD/783/1fF/9gZBPwEJel8tk4KpyQGuGJdpBn3ZEDdaTu2sNx3FzTV0wJnqvCyr5VPw6iCredrkQDsU9Nq1DIoXHQy1bCliL6bl8VewpB5/vXnQ86UYFz2mtmMtRxfojWjKZenyzYmW5dGWxXstbdRyaFuouk4I+uFYM+j/g/m/OP7P3iDoJyBB71t3Z1rmVoK+rMZLB+jyGfvUgXMOPe8tBH1Qy6mDpT7PMoQ3VHXQutKBT1GkZdA2o7nkLecQ1Wt9Gf/ig6Wp5r0eW/ukFOMTlkPbburNNmyfqfaPWhaNU7Es6fdBilxdCua9ljZqObQ/rbpNE/TDkaCPBEFfT4LeV8vEGfr6hoOp5sV2m/6spYee85aCXmrci7PFq/zgOfSw16wIIa9ISHW2titfxn+Z7DIozes8HhMGW/Ha026zRXguki6H9r1altT7Hz2+QjXF3NX4aD9WdRmKcSXoh2DNoH9v/q+P/7M3CPoJSND76oCgZWtyYC7G5TaDPqg5MZ/rGu9uzpKV0fPdWtBLHXC1zIolHUC7Hveq6HXptpNaR2MP+bJh/HUwz8M48vhr3FJfR990n1cHPf5sNk+2fYb1oLHqZlnS/Nbk8bH4HFdV9FoI+mFYJ+htvR1srq6P/7M3CPoJSNC/VTsu7Xya/sq2GJfbDnqpcVSwpTpjeQ6ts1sM+qDGXQf2Jr9dSk0e83ZwT3mmuW81/tr215HnvcYu9XX0Wi+p540e/+kpTQQH9dip35xofRTLEn996PEUhdpWqqJlJeiHYZ2gHwoE/QQk6N+aH4ztDXOTHZcoxoWgD2ocujjzF9B6u+Wglzr4Nr1kLBVa/3pzp4Od95qn5v39gwXQKuo60GOlOiOsx1R0pZwz4U1JF/vG1PNfy6IbAaRYlhDPdfaZ+rcE/TAk6CNB0NeToH9tCCHtgAj6OGpMNRbh7F/Tca2KHv/Wg15q+TUOQziwhH2F7iWv+eC93imqeR/z0o8i2tJcf671ohBJ+WHS4ssI153MgdTLosfVsqRYF3rMuieVirlB0A9Bgj4SBH09CfoXYx3QinEh6E8tR31KtO4I+kKNucYiVlDWResi7Cfy7eGGYj6oSzJiRb3GU4+Vat8SLpFrs/+7xH5fRGcXQa8xSnm5X5jXKfYzWg91j0N6PQT9MCToI0HQ15OgfzFW/BTjQtB7hjFOubPTAWdK87KNOginjrRz6Pm0nlMG6BgM6yBG1GtMFUcKBu+52hoiONVc0XzQWHQR9CFA2475OfS4qX5bovit+1vi4vUQ9EOQoI8EQV9Pgr5QO6uwE21LMS4E/TlD1Kc60OqAQ9C/qLndxwEm7B/YDl6iPsYZY61HhaQe03uuNmqbifXbBA/tX7vcLrVPTzXv9bjarmKvBz2etpu6r1vrjKAfhgR9JAj6ehL0LwdbHchinJkqxoWgP2cY71RnAgn6t2osNCdThdopeh6tX61n7/Xcopr3ipo4l/Slmd8h2lLMEz2m9rHe86Yy1X5Gj6dI1T5eY+Y9d1O1XpscizS+BP0wJOgjQdDXk6AvdqDhg7AxKMaFoL+kDg7a6aX40Joer495qWWqovezqdXz6laBKT8kGNDj6+4fMzuA97W8Q1X7BEVRm4N9Pr4Wqan2L3PtCxPEiJZZ+1nvOVOpfYD2BSn2MTpepJjfWq/aTutC0A9Hgj4SqYNej/vj6YP5cRL+mn3JFuvHPOif8+9vrE4I+nCWIoXexhLb2Gdxugj6MD7aeet5tAxSO5JLhn+nn9HPdjnOp+p5dYDXji/mAVePlTro9dq9cddB65phPej1hXXgPUds9VwaF83PVITQURSmHP9rhnkdxvjUrsb8VD2v1n+by1o0xgo+zSHvOdqqOxHpDVlsUr7mc2q8tY+JPee1DnTM8J6zrXrj3eTkkpaRoB+G2sYJ+gikDvpfs8/Z4/Jn9rT6bdrGN3Ln6/tss9OvWC2qagZ92KmFSIltqh1TWT2H7hUdc4evx0od9Hpsjbt2sIoDrQepg+Yl9W/07xV2+lk9RohL7bC7Dh0th15LzJ2f5mXKoA9Rpucoj7kOWlqOc+rvZVgP+nnFhsa/q3HX+m4SC1XR3F9q7BPO/XNqDLXOHx6KN1paVo2vp9Zf+U2t93ip1HPqtWkeNIkj/Yzmk5YjxWvXumtyyccl9Fh6zFTb5Dk1PinmfLGPX7nP2VZ9w7bWb11uOehftv1hqGVtsg775OaC/vvTh+x+8T1bb/UryW22P9gBfAIeng+2kZo1g15oJxJCJaYKHn09eOqDrQ7sTQ+s5+gi6EMQ6Ln02uuqnwvrTo+jg214E5V6zE+NvQ70OCmDXo+rA5t22GE86xB+RuOvx9Bc1/pM9XrLak7GjrWAHlPrUeuzyzmk55LaX+jN+XpdvMnS3Nb4eurvNA5aj+HSoC5fs9aDnltzoAn6Of18ijmjcdD2E3OOhNfb5RgH9eZO21jM5dEcShGoYeybzAv9zC0G/b0tr/Y5em0aOx17+1bzrem23Rc3GfQ6O7/b286hQfxCNbTD0AaR+syldnxNd56X0ONpo04Z9Do7p4NKDDTeUjtdjUfXQRYCOdZ60LJoOVId2Ip5Ey+Kw9hrDFLOGRkOyrHmThk9ph67q7mj59G60H5Ccb7b7fM5FObzJcK/0b/vY97redp8pkE/o2VONV+0f4m5X0wZdNfUGGn9Nhnnc2h5Uoy95oUeuwlaX7cY9GH9av+jLy4bgjHnWlfcbtAfNsdng9hoQwhnP1LtmKR2TjqAa0cTe+PTjnVMQV9Gr12REeKyq8BRlOl5Y6D1OaagF2He63WnfiOrbatpNJxDrz+8CfeeM7ZaB1oOjZfmTdv4DPNe21XK7baslkG/UWiyHWu89XpTzRX9tiPmHAmv1Xuu1GqcY79B0fLE3r9oPWruNX2d+rlbDXq9uY25fm8Rgh6iEqJGO41UO6Vg2Ak02TldQzuWsQa9COuhi7gM6jliHXT1+scW9IEw7innf5j7MdHr1vpLPVf0+GGuhJCPtR70OAqP8GbWe/6Yajl0UkHL0WQZUo65tnu9QYuBli3lbxOuGcY51p17wvLEHndt87p+vul8JugJ+jYQ9BAVbZBd7Pi1Y9LOJdUOQI875qAPhLjs6kAcKyJ0wBlr0OsxdfBTgHjPHcM2B2YPPY6uW+/iDGyIsxDzsQnjrzPnKd9USS1LPpdsn7dvsCx6ranmubZ5PXaMOVLsD9OP5zk1zpqbWq8x2O+LzwN4z9VGjbn2f03HXONM0ENTCHqIhnYSOkjrYK0dh7fhRvEYBLHOPnlMJehFOAvYxcFYz6HnahsR+vmxBr3Q/NGBM+WBWeMc6wCoa9d1ZjHpdmuGMGt6RrsOmvdaB6mXSSp0thY8TZZI+7EU+5lY26II87mLsTynxkjzJgZhn+g9Txs1t9uMt8aZoIemEPQQjdQRE9Qt2XRbvZRBoGWZStBrnMLOuKtg03K1WT/62TEHvR5X8ZHqjLfGWesz1vxRVKZ6rUG95nBmPtW4n6Lx6WLeaz41DRJtmylOgoQ5EiOSwjh6z9OVIfpizJ1w4sl7nqaG8W7z+rSuCHpoCkEPUQgBk/rsfNhpaueSkikFvdD66SLapA5GbaNNPzvmoBeaQ7GjIRi2gxjzJ/VYS71ezT3NwS4P2mHeaztOuV+STbdnjUeKs996PM2/GPtK/fahi33HJWPOecVj7H27Hq9pLAc0Fwh6aApBD1EI1ySmjAJZhFjab8oUevwpBb3Qc+k5U68jHTTarqPUkVnMo7RBr8dOOd6xYk3rSQfq2EFZNsSYXm/KMffQvNdzp573Wh96I1sXjUeKwNSYK8KbvKZT9BmB1ON3zbA8bfeZqfYt4Q1rG7QtEvTQFIIeWqOdQzj7mzIKpL5AposomGLQd7We9Nja8bdZtlQH3aAet4ug1zKkmkOxYk2PkXpO9HnA1nrQc2sZvdcWy2JO1X8jG7bL2L/d1GNpmdtGptC+KuX8qKKeX2Pc9k2s9kspfiMS3rC2gaAn6NtA0EMrtGMoNvT0Z371+BuLj0ODnVFdphj0Qs/XxcFZ49bm4BZiONWc0uOmDnoR3kB5r6GteaxFCHrdDSblPNdcC7GTerzPEWIk5bzXYzfdplO9Pq1XbUdt0Dpre+mYlivGsukxFH5t5lGKfaAeK8b+nKAn6NtA0EMrivBNf1vEsMPsaoOfatBrR97V+lLMNj3whtc59qDfbtNdexwj6LX8qUM3HKxTj/Ul9Nx6DSnnvcaw6WVQ2g+kOGusea79TNOx189pedrMYS2Txj3GttxmjAMp3jxp2ZqGchmCnqBvA0EPjdFOQb+uTx0EYSfeZfxONeiF1lnsX+97tlk+za1JBH3LGLpk26DXsusgnTpytX9oE2AxCMuaet6HMKk7r7S/SfGGQ/Nc4980lIr9YLvt8P7+webqLNp2oNfS5lKzcHmT99hNDeu9LRpvgh6aQtBDY8JZpVQ7n6AeXweV1PFVpjiQTTPo9Zx67tRB3+ZMmtY1QX/ZtkGvOa6DaMp50Me2e44u5r32F02CSf8+xeVZWlZth01DST/X9oSN5oDGPVYM6rU0/e2ffqZ44xR3nDXGMT7PovEm6KEpBD00QjuELs706rG1E+n6DJ92LFMNeq27lLEc1NjpwNuE1K+xCM30QZ8i0oJtgz514Opx89fYML5iU2zTaee9Hltj2iRMUvwGQY+l7bDpfkY/p3XY5jXp+TXuCslYy6bHarJML/uVuPv1hb2eGDGqxyDooSkEPTQixEDKg6PUwUQbetdBUBz8pxn0IoRmzHg4VY+tg2cTXg684w16PXZxNjDNHNL6a3NWUAdovbZUc0CP28eb8XNofWjexz47WzYsc5PtOuxTY68PzXXNk7pzPYxX220wvKnT9hxr2TTGTeZ+qmCOdYwi6An6NhD0UBvtDLRzThm7Uju1pmdi2jL1oNfOOUU8nKrnaLKTDnMs1YFNj9tF0Gv+plgGrTedzW06f/TaYsTaJcP2O5SDtJZZ8342S/uZHwVskzcxxT4n/pzXsupx666H8HrajlW4HCXmm9vwJqHu9qtlUpTGXP9apjZvrMvo9RH00JTbDfp9sTMYsnYIKgZkQOh1FQfFbi61aXJmKQbFwWy6Qa/l04495TqUWodNllHrfOxBry9bS7WdhO2j6QFQP6cDaMr1Hw7SfWy/59Byp34jGwKv7nLr3xe/QYi7zwlzpe52qH8fY6z03DpmaEwU4t6/qWux/dZ/k6Jl0jbpPWZT5zZGTd7AeRTzk6CHZtxk0D8svmfrrW2EFvV6riG6P+ws54c3ubXBKYRS7XCC2vH3GQNhOaca9BpXjW/q9RjOztVFr2/MQa/HTXnLSh2YNX+avv5ifse7BOJUPW7Ts6gp0WtJ/UZWc6vpvivVyRKtizr7Gr308Fq8x6tqmKd67phBWDxuvd/ean1oPsbcJvU69C26sUJUj0PQQ1NuL+jtMX/PvmSP9vizlQXHQF1tZ9nheRjXnga0E0h1wCkbDgJ6rr64haBPfcmFDFFXF72+MQd96vnT5sAsNO80/1Jtx3rc8GYu1Rg3JX8jm3C7LuZWszv7aN5ovcae91ofdfaneu0xzqiHear5JvXfvX9XVz1u3f2nlknrJeY2qfXU5ITFOW416LW8el16Do1BFw5tvxSDmwt6qcfVmfofA/Zx+cPGof19bWOinWfKs3pB7XAVgdro+kLPPfWg14Eo5fLJWw16rdeU356s9abxaUrKg7PUPkJB3+eb8nOEUE21HyvipFnQh+0yxbzRdlj1Nenf6d+33T9ojDVP84iyx9SbKe/fNVHzt05Ma5m0z401tinmuMbpFoM+jKVeWxdqTtbZHsbCTQb9GNRlQUMK+lg7+GtqR1aEbr+/eruVoI/562dPrU8dxOui1zfWoO9iWwlnv5vQxbrXAVrze7frZ35fIoSJXqP32tuqx9V11U3nlvY9KdaNQqbq/qbY/7Xf/jQW4VKKsF3EGneNkR676jjr32m78R6ricU+pP51/JfQY91i0Eu9tq7U+GpZYq67IUDQD9ShBb02Zh2gvQ0xltrQ2oRKTIoD2rSDPnXYSO04dQCpi17fWINec0fLnHJc28wdLXPqNxxa9iIgh3fALH57kjjo7fGbzi39XIrXp8fUNl8FjZHCsu1r0HYWzoRK7dtjbdNh31JlnPVvtEwx3yiVly0Wtxz0XapxUGsQ9B1A0A8r6Iu4TX+bSj1+7DMeTSmWmaBvazjo1kWvb4xBr/Wp161wSDWuetw224mWWWc2U87tYnyHsS2fotekbS/lvNfYtlk/Kea+XlOVkyV6fv07BU+bMdLPahnKz6l9Tqx5p8fX/qvKOIdlivncYTxj7kOKuUnQp1bjQNB3BEE/rKDXWYi2O/drhp1z7B1kU7ShE/Tt1YHpVoK+i5iXevw2Zwb1cwR92qDXYzfdtrV+tH5jnk2WWid63GvEen6NQYjegPY5OpZ4/76Jeo16zGtomWLuT7Rsem6t46bboQdB340aB4K+Iwj64QS9Jrx2AKl2MEHt+MO1lkNAr2PKQS/03KmDXo+t5ax7AIl9AD5Vjxsz6LuKedl23miZta2l3KaL8R1w0Ns+LfW8b7OOUsST1kmVa85jbXsaA0VTObhj73O0f9ZrvUa+zm27iTXn9Th6vNjzu3idBH1qw9wc4v6pDQT9QA1B//zc34TTNq97aaf8qnTp7fj7Rhv6LQS9XkOsg6unHrvJgS9WVJxTjxsj6PXzmrdaxpRzJajxbBvKes2pgz68QW87vinQ3VYUNannfZttO8W2qcfScl+bO7GeWz+vACyPg5475nKFsL6GXkPMNxJ63qZhfIlifAj61GocCPqOIOiLoN/stOH1F3yxd77nLOJqWGfz9FoI+vbqsfUcddetDjipg16vSwcpjUNZvdZLhn+nyxL0GF2clZfhINT2jW9nQd/isqCUhLmVet5rjjQlxfwP2+K1+aNLZDTP2o6Pfv70DYT+e+w3U5pr5ec4RWOpZW57CVFZPWeby97OoeUg6NOrcSDoO4Kg7z/oNdFTBlVQj6+dhG5vF3nf2Ipi+Qn6tuqx9Rx1d5wpguZUPbYO8tqxS83DoF7zOfX3+jnNDT1GyvErq+fSmLQ9CGlsCfphB73QOoq9/9HcLV/T7hHreTUGeqzyfE1xXNFjKTbPzTX9ueI71lhqubS/SLHv1vgQ9OkN67DtvnRoEPQDtc+g18aunX7MMxrn1HOkONPRluLAQ9C3VY+t56i749R86OINZVm91rcq2MsWf+79fEr1nLEiQmNL0A8/6GOdKS8b9rfn0NjEOoOu+XX65kGPrz/T/Ii1XHqc0zcOZfTnMd8cabm0T0sxt/VaCfr0ahwI+o4g6N9ls9Vdb0GvA1HsX4t6aqel5xniRqXXRNC3V489lqAfsnkgW5TEiAg9BkE//KBPsX0+WtCv11ovxyc5IdZz6uf15uF0DDT2+rOYb1T0OJf2McW+PN6+RHNbj5dibuu1EvTp1TgQ9B1B0L/LFpvHXoJeG7oO9qnPzmuD0s5BO4khUhwECPq2Nj2A6N8T9IUhIGLNlbCNE/TDDnq9Tm07MdfTw3EuHQ5v14ueL9ZvBfTz5yJbf6Z9f6zx1+N4bx4C+vOYJ6g0PhqnFHOboO9GjQNB3xEEfX9Br402deRJ7bCKA8swNyi9LoK+vVrPOoDUJURXyugcg2H8Ys4Tje2tB33MwPPUY8dYZ7o8JubJFa3zc6Ed5kWMfZ6eR4/lrX/9Wex968OD/yVP+t8hRGOsbz1Gyv02Qd+NGgeCviMI+n6CXht5rB36JcPGdHp95ZAg6OOoA5PCvC7FQf+2g17X7M/ni+hnA8N2nnJshxz02rbTB/1DlG07nGDxnqOpijIvZMK4xJgXehNyad5ubblivlHReOtSotPl0vPrdcT4rYMM+7NU81qvn6BPb2gQbzsYMwT9QO066GPv+C6ZH+wtKIa8Mem13ULQxzpzdc6wruui+XirQa/1oeXWtpjiA+N6vE6C3p4j9muPQRFNKd/I6htSn6Ls34r9UNzLgxTSirNTwhv8tvNCr3U2u3w5pZYr7r7H/34Gzb+Yv+XQ46S8iUMxNwn61BZzlKDvBIK++6CPtTO/Ztih9BmyVSgOpNMO+rCDjndQfWs4ANZFB5xbDHqtC805zQ29wU5xwNHYpg56PfZQ37QX0ZQu6GPHgrafmOtK88vbJmO9wdfPV9m3xfptQNB7zjDXY+3HNT6X3qi0pZibBH1qY2+jQ+Emg/7H04fs1+xz9nv2ZbAuNxZCHQV9eafXdmd+ST3201PxxThNdihdog19ykGv8devvVMHvXaa+rbhuuj13WLQa76FN7yptpGwvaccWz32cjnMz8hobFMHvbarWOtP+0ttR95zNVHrRttWGb3WWM8THv/autebipj7V73202NLzLmu9Zr6ZBRB340aB4K+I1IG/fen99nd/Gs239hOZzPL1pv5MN0tsu2+m19Zpz7ABbWT0s61i2Vqy00E/fESK++1xbLpGS29vlsKei2n1oW2j5QxL/TY4Q2891piGMJhiAfMECap9ndal02jyaOIvHjX0YfXVybsD2JcmqLHKG6NeXn5Ywei5rOWofy8xX48zn5Ej1HljUobinV9e0Gv16X1p31gF2o5tC5jbaND4QaD/kP2sPxxjOVDvkKH6P55lx06ODuvHYgO7rGuMTynPrSk6yr7Cti6FAeC6Qd96vXedBn1+m4h6MOBTAfZVJfYnKKxjX129FQtV5/z+xIKEx3UUwZ9zFgI6yvW69XjKGjKr0//PcabPD22xvY0rD3095r3sZZL465xKj+v5p/mYYznCJcPXluuNtxq0Gt5tZ40vnqeLhxLi9ThJoP+cfkz2x3qX9c7NbRRa8erjTTWTvWc+pBYlbM2Q+EWgj511EkdPJpEql7frQR91QCKRRfrXsul/YoOnEMjvJFNtc9LEfR6zTHXl+Zc+fUV+7v221tY71X2a3r+mNu4Huf0N8CafzGOb11tp7ca9Jrbmgta/pTjO3UI+htGO11t3Kl2HsE/O/kGYdcXUw/6cABve6C7ZtOw0c/EPNgPWc0xhYjWSRdobBX0KX87Uw6goaETCym3az12zDO5ehztJzSesbZXPVZ5vum/xziTrZ/X41SZy7HnoZ47RGEg1pu3sFyp36AW6+E2g77LfeBUIegHim1w4kn/7/hHUdHDpj5TFdRzaMc9JrRjmXLQhzdzKde9Hls76SZoft5K0MsQgV0d0LTtK+q81xJLbfepz2jWRa8l9bzXuowdfpoXMc40B7VutA8I66YIyfZBr+31NKovEXsenu5T9fgx9uEaF82b1NtnsR4IemgGQT9QbIO7N/8L/efxj6KinV7KHUdQG2oXO8LY6PVOOei1c45xAL+kDtRNw0YHnFsKeq0HjVfMM7uXKLb/uF9YdKqicWiX2RXBlG7eh/UYe7vWGGo/Gmt70H6tPNf0emOEdTh5U3Wdx94PaRkU8QG9lhj78NPxSgVBT9C3gaAfILaxHcy/zP/c/HL842hoYy52dGnPzuuxtQNoGnV9MvWg1/rXwdd7XbHUum+6fJqjfQS95qy+Rl5jU1avI+W2IvUcmhNdbC+a3zqwp1wmbTvahoZykFbC7Gw+zhMGvdZhMe/jLnOxz453Hb1epwJKjys052IEfd03M/q3Md+oaFvVvi2gZWz72Jorut1yk9vv1oWgJ+jbQNAPENvYFub/21TQfz/+cTS0E015lkrqscNG2mTn0TdTDnqtD8VyyvUv6/zq/ZTwGlMd2PS4ig8d3PQ8Qc1XBYHiqaz+XOtLwZBy3IoIXiWfF1ovWqaUy6Ix1vj2Mcc9NKc2Wzu2RAjXc2r9aZ6kCBM9puZsjHWmxwhhl49LhDf4esy6y67njvmZBs25EPSx9iFNlqspeg6CHppC0A8Q29j+Mf9Ppi65mR//OAraYLTD0wYU48BwTj32WM/OC43TVIO+qzd0mmdNDhxCPxfjYHxOPa4ev+rr07/TuGmZiqhKd8DVdpP62nM9tpbllvYBWmZFQ8ptWo/dNJiqoPGMsc70GNoH6HXGGpe625TQv411nbvUcmlZ9Lj6LUmMOG6yXE0h6An6NhD0A8M2NJ2d/5f5fzB/mFH3Iik3yrLaQFNHSUqmHPRaL7HO9J1TZ/vahJzmTfqgr//bI/37YhtaJH1tOuimPrhpOdqelb2k5le4BGIIe4Fim057GVdY3lTEev1aNzoOaD7HGhcte5N9vuah9kfeY9a1HKzat8Z4AxSOZV2gdUHQQ1MI+gFhG9nafG++/hq/SOi2kUvbcac6uxjsKkhSUhzkphf02pHr4J1yuXTQaLts4XWmOrDpcZsEfUDLpi9KaxsL5wwR0fT1VUFzXNtpqmWQeUDYehzCviAPvIS/mdLjKnjavJG9Rv4mLFL8KqK1XnQmW29Q24yLflbbfJP1HHMehnWgdR3eKLRdLm3nKddpGY0FQQ9NIegHgm1g4pf535vR9x7afBUIxRm5tAdw7US7OqORCu1Yphb0Nq/ynbKeN9UBQ+qx2+6c9VqHHPT6OS1jqvmh7UjrSeur6Wu8hh5X15SnnAtaDgVo6jcn18iX9Xi5lPc6YxjWWcoo0XLEiiodCzS/ZNtx0RxqGor6GW1LGj/vseuYH39sfLa2TJpzbbfPsFyxP+R8DoKeoG8DQT8QbAPbmZ/M/9aMXsP52XnbkJMfvO0gMYUNU69/ikGvdaN1FOPg6anH1Zi1DTj97JCDXuRnfO0Al2osuzjIab+Qcj7IEEV97hOK7Tntb6Ye7LH1HG3mVBU0ljHWV9hOZfugb7fsikW9ntbLZT+v+aw3b7Lt+g7bYOp1GtA8JeihKQT9QLANTJfb6Nr5b2b0ytPOTTs6b2OKpXZC+VnFDiM1FUUATCvoi4NF+g/DKg50RqvJQSOgnx160Otnwxsk7znaqrHUAVQH0lRoGWIF4iXDb+3ajHcbUr/5kvpNhH7jkZoYoSr1GHqsGHM4RHTT9Rtz/YQ4lG33H1quLuctQU/Qt4GgHwi2gYUPw8Y/O9/BwUxq57e2nfqhp4N2TKYW9NqB68CUKj6DOhAtl+3PUurnhx70Yrcrti2dGfSep635NmUHurav8xKxAvGSxXj3cy29xq6LZexqe1ZYxbh0KKwTBV6bsdFxRa+nzRtPzQu9jhjbux5Dj9X28bRc2rZTvqE+haAn6NtA0A8E28Bm5v/fjPbCtNEWYZoujIJhJ9pVoKamGLdpBL3mQdgZp3xTFw7sMb6ARa95DEF/ONjrtANRqnnSxXalx+7iDb+eo4+z9F3M/Xw+JX7jFSiir/1v2vTzehzZZjsLj9NmjmrcYr3p0rLoOnrZZoy62PZOIegJ+jYQ9APBNrAn878yo92qUhtHF2emtJPo+1fqsZlK0Gt9aEesnXiqg0RQY6XnUeS2Ra97DEEvirP0C3vcNMHY9nKGa+hxu5wfXQZSV8uWv1mx7awLin1T+21D+229bu272zyWfrbY7tvFmPZTMX6DqOXS48g2Qa/5qnHuMjL1XAQ9NIWgHwi2gSno/2vz1RdJ2f8WG/2/4x9Vposzb2HnObWNUcsyhaAvlmPV+uB2TT12HjWRriHWdB9L0BevVdfrpjtLr7mSavvS69f2GyOmLhn2FV1FkpYrXGaWcu4Xc6m78NPzaH213TdpTBTzbcdHy6/X03Zbinm80muS3t9VVeOb8o20h9YtQQ9NIegHgm1gCvr/wXx1msf+t66rXx3/Z2XsZzo5O68dTzjrpuecCtqxjD3owxzQQTtl0EjNg5hRo9c+pqAvwjHdOGsexnqz5KF5qPmYep7o8RWQqUNJj60ASb1MemxtX3qulMtTRs+z2RRvVLzXVFW9ds0rbQttxkivI8bcLL7ZNc4tdbU8bdd71+tVEPQEfRsI+oFgG5iC/i/zT+HZf9dW9389/mctwsHM23hiqZ2DdnramTd4iYNm7EGv199VzEs9j+7wEWse6HHGEvQidRDr9TY9EFch9XiXLe83UhzA9Zja/2m8Ui9PWC9dh4g+pxLjbLZevx6jzeOE8G1Ll3PwmhoPjW/KEy4emkcEPTRB6/Qmg/7Bgn67Lw7mfWnDXyzsEfszBf2/m6dn6NfH/1oZbRRd7BinvBEWYzi+oNfc0o5X6z/1pQZBPYeeL+Y80HKknMN63JhBr8fRtpByvuixU7551lxMdcD2zN8E2pvOmPNGj6UxinWm95Ka94+P3d7WMKDlVJh1sX1fUs+vsY6xDjWGmg9tf/MQQ82dPo5tej6CHuqgdRn8t/BfhkTSoLfH/D37ms1Wv7PF+rEfN3YQyN9QvExeWwe6y81/Y9YO+DJal13sFMOOQREwtPkTA+1Yxhb0es1a9yFmujrYh7OtMeeBHmtMQS/Cdpdq3IvXnO5abY2FDqqpxvxUjZPGS/sRxULbdaFx0evXfOxi7mucUrwpr0LYPrraxs+pMdCYx9iO9Bjaj3S1/i5ZvFHr9nIboTlM0EMdtC6DNxn08sfTx+zn06de/DX7nC03j9mh9P1Rtg7E/dGvpq6nnx3/ujLhLFvqHaI2QO0YtAPWhj40277R0I4lddBrPWn89Fx6rcFrhH+nn5NaXsWk1kdXZ+WlnidVZGr5xhb0Wg9apykPxnp8PU/M112mq/1HWY2XnlNzWMtW3h7OEf7+dP5re+3ites5wm8YUq2La2jf0ffZbIWvxiAWfcw/T61bXdPfNZrPtxj0YR+gfbLm0xBt2xSpCPtC+Sfog0MgZdAPwR9PH7LF+v5V0AdsHTzZf/yX9p//N/P/Yf535hfz6pl7+zd5BKWM0KCeQzs9bYRDtO27ff1s6qAPZyf1WrXD0AG6yhsk/Rv9e/2c1rfOEnYZ8kHthPXcum1jbMJcTnVg0+PGDnrNGa3PlAdjzUet+zZz+xJ6XM2rrsI4qOfSHNZ80noP24Pmuw6kel1S/728DZTnv/e4qdT46LlTrYcqaBy0r+t6uy+r59friIXGM+U2VEWNp+ZUzH1DVbT8txj0el1aZm1XQ7Xv7b2M1p/nv3l/KPvkxoP+H/P/bv4X5n9m/ufmZ/PiStFfD+XsxhAMB5orw3YWbbipg97fiRX3T75k+Lf62b7WtZ5Xb+gUVk3H+BJ6zLEFvR5LMax1lGq96HEXdkDWtp4KPbaC5j7R2F+zvE1ojum1LG0uSC27/qzPbSBfB/aa2uxfYlDso/q97CZ25IRlSrXdV1HzSttxH+tWy3+LQT8GNW4x53oTtN4u+W/eHwb74laD3sZ8Z+rbYhXx/8H8z+yPdbZ+UfyL82iihTNr3mS8NccQ9GNVB4XUBz097tiCXmjO5TGcMLL0hkFvpFKhMdEdix7tebzn70WNZ4/hWjaf+/otSaK5XwfNgz7jV78libkN5XPPHrPP/a62L72pjb1vqAJBP1yrBL2Na87xf0ZFD3vNf/P+8NSuueGg1/Xz/09Tl9p8NP9fps7YX1wJ+mttbDpz5U3EW5SgT6cONtq5pTxLrPU2xqAPb6xTRlY4KF87uLRhr/mf+LcNY7SLuV+HsN/vYx1pLNrsY8/RZzhqHPXcKbetS+h5Cfphem2fa2O6NP+P5v/G/H3841ZoPdWRoO/BC0H/zdTlNv+tuTV155uLK0B/rYOLJlvKiBibBH0aQ9CkOJCX0WOPMeiFzpqmjiyFtsYn1TrQw2q/oudgGyjUnBnCpTZligBM/4Vgnm33sefQMmkf09eblKbBGwOCfrhq3M4FvY3n2vxp6ruE/nfmf3r8q1bY49SyUtDLLrnhoF+Y/z9TZ+orDbom2Ho9jHv3DkmCPr5dBo0ef6xBH95gpwwSvX7N8dS/JdG6ThkYY1HrUuOd6jMjTSn2U/1cc67n1fyLPR56vL4uH9Vz9nX9vCDoh+u5oLex1GA+HP9T/3sl9d/boIera+WgD3bBDQf9s00a/dqm8kAXG5nO0Nz2AfdUgj6uml9tx7QOeo6xBr0eU9cBp4wsHZhDfKREy7I9Hsj7iMYhONSYF3o9mgNdn9DRmOh5vcBpi5ZJ+5k+TlLpOVN+PuUaBP1wPRf0KdD6aWLtoC+bilsN+roU0cmvxD0J+njq4KI3jV0GjZ5nrEEvNPdSX3YTQjP1QUZjpHWv385o3FIu09DUsmo9xv7wZ0y0brq+jl7zIOX+QHNac7vLZQrbk7bdvtByE/TDNHXQa520tVXQB2ND0FcjbGBd7vTGYtgxN52fBH2x89fya0fWZiyboOcac9AXB+YigL3nj6XWTxexqcfXHNAy3cI2obmvdaflVbimPJC3RZe9dDHXghobncneJbzcS+Ot/U5XyyS1XBrHPtd1sd8g6IdoyqDX+ohhlKAPxoKgv44mFmfnz0vQt7OI+eKDl13HvNDzjT3o9dkWjaH3/LHUetLZ2ZTX0gc0VpoLeWjZdqHn9l7T2C3m/mMed1reVAfxWOj1dRm/Yc6lHBfNtWL76W7/W+wTWl/63AqNKUE/TGMHvdZBbP/N+8M2xoCgv4wmlc7K6SzJVA+qbSXomxliRnNLB1SNQ6ztug56zjEHvR5X36CreZg6tLTOtCwxDzbn0HLlH/q1dRP2P1PZB2k5tK60XDp4p/jAZwr0Grv8EKnGqGkUVkWPvd12eytmrXcdV/tE2zBBP0xjBb3GPpXRgz7YBoL+PBrbsGFN5UCaQoK+vppPOoAqpLu8Xt5Dzz3moBd67PDG23sNMdV60zrrCm0fWrZwCc7Y90V6/VoOHbQ1jl38xiMWmmd6zV3MM6lxSn2Zlx676+Ocnqvv9U7QD9cYQa9xT2myoJdNIejPo3Htcuc9Vgn6emo5FWc6UGvZ22y/MdDzjz3ohcayi4NgOFB3GSQaOz2fxnHM+yONnd4Q6Sz3WM7Kn6LX3VX8al23DZtraB3oOTSnU+0DTtX+pu91r2Um6Idpm6DXeHdh0qA/tSoE/Xk0jtqwZrPb3bCqSNBfVjt2qeXTWCnkFQWpD9RV0XqbQtCH5ehiHnV9lj6w3x/y59UBT8vZRVS2Va9RnwN4sjHT5UPaVwxl7jdBr325TLe9BDVummdd0OW2o3HTPrBvtB4J+mFaN+g1xl37b94fprIqBP1lDoe0sTMFCXrfEPE6KOuMvM5KthmnVOj1TCHoRTgQpg5dLZMOOl2epS+j59V80rzSWVy9ntTLXNfy/FfIb+yNyJhDPqB5rDdVqfdXYY51hSK7i9/+hONF3xD0w5Wgd6xCOeinaNugF11Fwlgl6F/UHNEBQgdGjYtCWQf/IZ2RP0XrbSpBX8ylbs6eah33cZY+oGXVdqcQ0wFQ4axtqM/91Ln5r9faxfrvCm3PqeO36/ml59IcSj1/NCeGsC/UayDohylBf8Zr7A+2Ea/us59Pnybpr6fPFvRPFvTNdyAax+LOBtzpxvOWg17zIURMCBntjMqX1TQdl67Q65tK0Os5FCZaD6m31XDA7jtOtMx6DdoGNc6KsjAfuxiD8vzXc4eI1/wf+txvipZNvx1JOb7ar3Y5t7RMes6Uy6TH1r5xCPNCY0vQD9Mq+9XyuOq/d20vQX/qKc8Wurv9JtvsFhN1me3sTYst/XGJm6Hb4oW7TISDFxZqXIqD93GwaqINV2+YyiHSxPv7ImBO9XYYTQ2PGZ5T80E73jxibCfc5QE4FtovaPwVweXxjKUeV4/v7X9SEOZTF9uq5myXZ1GroHFWEGgMyvustttE+efD8mvd6jn0XG3e1I8NLWfKN8Ea46Yx2JQulklzcSjzRPsJLW+q/Z6WVY/fZFk1RtquvMe9BeueKNEYd+2/eX/Yh2/Rn+tM4jTV8rVF46Zo1cEbX9v2TFwYW+3EvMev4mYT3ORxUVY7xrIK8Lrq57ST0ePpOcLz6jVrxyPbjEHf6PW3Gf9LhjHqkpTLU7aPZauC5qJeV3kcwrahD3TW2Q7CdqOfK89/Pa62Wz2Hnm/M878uYZ+lYAvjE1PtazTGXY+p1mmqZZJ67CFtL+XtI4WaI03Qek/5uoZu3TkS9j9dOuCghyp4Y4nx5pP32HXUB5i1IzhVO9VTtbOsavnnwmOG55wS5bGMbR94ryOVQ0ev8WV7eNkmvPl+6sv8f9mmxrDMqdEYvIxNfDXOXdPFMg1t7uj1pLQN3uPdgtc4/TenP9+Fgwn6IMCt4m0P5wSYKt58PxUAYKh4+6wuHFzQlwUAAAAAGCqHk19deT3bhYMOegkAAAAAMDSsUxfW869i9bRju3LwQR8EAAAAABgKFvP/X2tUgr6uAAAAAABDwNr03fG//uG0XbtyVEEfBAAAAADoE2vS3fG/5pz2apeOMuhPBQAAAADoCuvPrTmIy23kJII+CAAAAACQmr2+yOAEr027clJBXxYAAAAAoAu8Fu1Sgh4AAAAAoAVei3bpZINeAgAAAACkwuvPPpx00AcBAAAAAGLiNWdf3kTQlwUAAAAAaIvXmX15c0EfhGFg6+Jg6tZPS/PJfLQ/fiirPzMP9t8BAAAAese6ZFAS9NApNvZiZ65MBfx3829TX5/8/zH/O/O3uTEV+7nHHwcAAADoFeuS/2C+6sq+vdmgD0I/2Nif5fhPYEDYapmb/xfzPzL/5+b/2dwc/xoAAOAmOBwO/27Hv//YfNWTfXvzQX8qAPjY9iH0G5O9/svxjwEAAG4CHfuM/6lFva40eNWPfUvQOwIAAAAAlLFG1Gf+/icW9Dqx9aYf+5SgryC8xsYk5/g/AQAAACaPpY+unf9fWtDrt9WvWrFvCfoaQoGNhT7MOj/+TwAAAIBJY92jiP/fWsv/V2rCoUnQ1xAKbCwEH4gEAACAm0DXzZv/Kk7O+53YpwR9QwGGjM1R7XH0oR3d/nNdUv9bf86tQC+g8TmOF+MEAACvGnCIEvQtBRgaNi+FPrCjeNf9/vWlXWX15/vjPwcHGx/F/F/mZ3Om8TLZ4DtE423ojVXZnOM/AQDoBO12hi5Bn0AAGDe2HSvo9YVnivofpu7Drz/TbzfYyDtA42wQ9ADQK9rljEGCPoEAMG4OxsZYH9F/3xp74/hPAADgBvA6b4gS9IkEAAAAgPHi9d1QJeg7FAAAAACGj9dxQ5ag70EAAAAAGCZeuw1dgr4nAQAAAGAYeK02Jgn6ngQAAAAYO17jYPcS9AMSAAAAYAx4HYP9SdAPUAAAAICh4TULDkOCfuACAAAA9InXJzgsCfoRCQAAAJAar0Fw2BL0IxYAAACgLV5j4Lgk6EcsAAAAQFu8xsBxSdCPXAAAAIAmeF2B45Sgn5AAAAAAl/D6AccvQT9hAQAA4Lbx+gCnJ0F/IwIAAMDt4LUATleC/oYEAACAaeMd/3H6EvR43AUAAADA2PCO63h7EvT4RwAAABg+3jEcb1uCHl0BAABgGHjHacSyBD26AgAAwDDwjtOIZQl6rCwAAAB0g3ccRjwnQY+1BQAAgLh4x1vEqhL02FoAAABohndcRawrQY9JBAAAgBe8YyViLAl6TCoAAMAt4x0bEWNL0GNSAQAAbhnv2IgYW4IeOxUAAGDKeMc+xNQS9DgIAQAAxoR3LEPsS4IeByUAAMCQ8Y5diH1L0OMgBQAAGALeMQpxaBL0OFgBAAD6xjs+IQ5Ngh5HJQAAQCq84w7iGCTocdQCAAA0xTuuII5Rgh5vQgAAuC28YwHiVCXo8SYEAIDbwjsWIE5Vgh5vUgAAmB7e/h7xFiTo8eYFAIBx4u3TEW9Rgh7xggAA0C/evhkRX0vQI9YQAADS4+1/EfG8BD1iCwEAoB3evhUR60nQI7YQAADa4e1bEbGeBD1iQgEAgGhHTC1Bj9ihAABTx9v3IWJaCXrEngUAGDPefg0Ru5WgRxy4AAB94e2TEHFoPmf/I2YUBBwVmU6yAAAAAElFTkSuQmCC); */
		background-repeat: no-repeat;
		background-position: top center;
		background-size: contain;
	}
}

.doc_header {
	width: 100%;
	border-top: 2px black solid;
}

.doc_header2 {
	width: 100%;
	border-top: 2px black solid;
    page-break-inside: avoid;
}

.doc_body {
	border-top: 2px black solid;
}

.doc_title {
	font-size: 1.2em;
	text-align: center;
	font-weight: bold;
	border-bottom: 3px black solid;
	margin-top: 3px;
}

.section_title {
	font-weight: bold;
	padding: 3px 0 0 7px;
}

.klucz {
	text-align: center;
	padding: 3px 7px 0 7px;
	clear: both;
}
.klucz_value {
	font-size: 0.9em;
}

.doc_id {
	font-size: 10px;
}
.doc_id_label {
	float: left;
	vertical-align:top;
}
.doc_id_value {
	float: right;
	vertical-align: bottom;
	font-size: 10px;
}

.disclaimer {
	font-size: 0.9em;
	text-align: center;
	padding: 0 7px;
}

.header_label {
	font-weight: bold;
	padding: 3px 0 0 7px;
}

.header_value {
	padding: 3px 7px 0 7px;
}

.header_inline_value {
	display: inline;
}

.body_element {
	width: 100%;
}

.table {
	display: table;
}
.table > div:nth-last-child(1) > div {
	padding-bottom: 1px;
}
.rowedm {
	display: table-row;
}
.cell {
	display: table-cell;
}

.row_value {
	padding-top: 3px;
	padding-right: 7px;
	overflow-wrap: break-word;
	word-wrap: break-word;
	-ms-word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}

.row_value > div:first-child {
	padding-top: 0;
}

.row_value_element {
	padding-top: 3px;
}

.row_label {
	width: 95px;
	font-weight: bold;
	padding-top: 3px;
	padding-left: 7px;
}

.not_known_id_prefix {
	font-weight: bold;
}

.address_value {
	padding-left: 7px;
}

.caption {
	font-weight: bold;
}
.caption + .paragraph_text {
	padding: 0 7px;
}

.footnote_div {
	border-top: 1px solid #dcdcdc;
	padding: 7px;
}
.footnote_label {
	font-size: 1.0em;
	font-weight: bold;
}
.footnote_values {
	font-size: 0.9em;
}
.footnote_value {
	margin: 2px 0 0 15px;
}

.table_caption {
	font-weight: bold;
}
table {
	border-collapse: collapse;
}
th, td {
	padding: 5px;
}

#footer {
    position: running(footer);
    text-align: right;
    padding-right: 2px;
}

@page {
    size: 105mm 297mm;
    margin: 5mm;

    @bottom-right {
        content: element(footer);
    }
}

#pagenumber:before {
    content: counter(page);
}

#pagecount:before {
    content: counter(pages);
}
			</xsl:text>

			<!-- jeśli nie użyto żadnych stylów typu Rrule Lrule Toprule Botrule,
				 dodawany jest styl delikatnej szarej linii -->
			<xsl:if test="//hl7:table and not(//*[contains(@styleCode, 'Botrule')] or //*[contains(@styleCode, 'Lrule')] or //*[contains(@styleCode, 'Rrule')] or //*[contains(@styleCode, 'Toprule')])">
				<xsl:text>
table, th, td {
	border: 1px solid #dcdcdc;
}
				</xsl:text>
			</xsl:if>

		</style>
	</xsl:template>

	<!-- ++++++++++++++++++++++++++++++++++++++ OBSŁUGA NARRATIVE BLOCK (kod polskiej transformaty HL7 CDA, brak obsługi multimediów) +++++++++++++++++++++++++++++++++++++++++++-->

	<xsl:template name="sectionText">
		<xsl:param name="text"/>
		<xsl:apply-templates select="$text"/>
	</xsl:template>

	<!-- Transformata przekodowuje tagi MIME type "text/x-hl7-text+xml" na html:
        <content> - odpowiednik <span> w HTML, posiada opcjonalny identyfikator wykorzystywany do wskazywania tego tekstu w bloku entry.
        <sub> oraz <sup> - identyczne jak w HTML
        <br> - identyczne jak w HTML
        <footnote> oraz <footnoteRef> - bez odpowiedników w HTML, należy oprogramować
        <caption> - nagłówek elementów paragraph, list, list item, table, table cell, renderMultimedia. Może zawierać linki, przypisy, sub, sup.
        <paragraph> - odpowiednik <p> w HTML
        <linkHtml> - odpowiednik <a> w HTML, nie identyczny
        <list> z atrybutem @listType oraz elementem <item> - odpowiednik list <ol> i <ul> z elementem <li> w HTML
        <table> z elementami <thead>, <tbody>, <tfoot>, <th>, <td>, <tr>, <colgroup>, <col>, <caption> - identyczne jak w HTML
        atrybut styleCode z listą prymitywnych wartości.
        -->

	<!-- content -->
	<xsl:template match="hl7:content">
		<xsl:choose>
			<xsl:when test="@revised='delete'">
				<!-- content wyróżniający fragmenty tekstu jako usunięte z poprzedniej wersji dokumentu, nie dotyczy recept -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="span">
					<xsl:apply-templates select="@styleCode"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- sup -->
	<xsl:template match="hl7:sup">
		<xsl:element name="sup">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- sub -->
	<xsl:template match="hl7:sub">
		<xsl:element name="sub">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- br -->
	<xsl:template match="hl7:br">
		<xsl:element name='br'>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- przypis w sekcji -->
	<xsl:template match="hl7:footnote">
		<xsl:variable name="referenceId" select="@ID"/>
		<sup>
			<xsl:text>[</xsl:text>
			<xsl:choose>
				<xsl:when test="$referenceId">
					<a href="#przypis-{$referenceId}">
						<xsl:value-of select="$referenceId"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<!-- pojedyncze wystąpienie bez ID, brak odnośnika -->
					<span class="footnote_text_inline">
						<xsl:apply-templates/>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>]</xsl:text>
		</sup>
	</xsl:template>

	<!-- odnośnik do przypisu -->
	<xsl:template match="hl7:footnoteRef">
		<xsl:variable name="referencedId" select="@IDREF"/>
		<sup>
			<xsl:text>[</xsl:text>
			<a href="#przypis-{$referencedId}">
				<xsl:value-of select="$referencedId"/>
			</a>
			<xsl:text>]</xsl:text>
		</sup>
	</xsl:template>

	<!-- przypis - wyświetlenie -->
	<xsl:template name="footnotesOnTheBottom">
		<xsl:variable name="footnotes" select=".//hl7:footnote[@ID]"/>
		<xsl:if test="$footnotes">
			<div class="footnote_div">
				<span class="footnote_label">
					<xsl:text>Przypisy</xsl:text>
				</span>
				<div class="footnote_values">
					<xsl:for-each select="$footnotes">
						<xsl:if test="./@ID">
							<div class="footnote_value">
								<a name="przypis-{./@ID}">
									<xsl:value-of select="./@ID"/>
									<xsl:text>. </xsl:text>
									<xsl:apply-templates/>
								</a>

							</div>
						</xsl:if>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<!-- nagłówek w przypadkach innych niż dla elementu paragraph i table -->
	<xsl:template match="hl7:caption">
		<xsl:element name="span">
			<xsl:attribute name="class">list_caption caption</xsl:attribute>
			<xsl:apply-templates select="@styleCode"/>
			<xsl:apply-templates/>
		</xsl:element>
		<xsl:text> </xsl:text>
	</xsl:template>

	<!-- nagłówek akapitu
		parametr intentionally wyokrzystywany jest do wyjęcia <caption> z elementu <paragraph>
		w sytuacji gdy caption nie jest pierwszym węzłem paragraph,
		umieszczenie tekstu w <paragraph> przed elementem <caption> jest zgodne z XSD
		o ile tekst nie zawiera innych elementów, nie jest jednak zalecane -->
	<xsl:template match="hl7:paragraph/hl7:caption">
		<xsl:param name="intentionally" select="false()"/>
		<xsl:if test="$intentionally">
			<xsl:element name="span">
				<xsl:attribute name="class">list_caption caption</xsl:attribute>
				<xsl:apply-templates select="@styleCode"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<!-- paragraph -->
	<xsl:template match="hl7:paragraph">
		<div class="paragraph">
			<xsl:apply-templates select="hl7:caption">
				<xsl:with-param name="intentionally" select="true()"/>
			</xsl:apply-templates>
			<xsl:element name="div">
				<xsl:attribute name="class">paragraph_text</xsl:attribute>
				<xsl:apply-templates select="@styleCode"/>
				<!-- spowoduje wywołanie template'ów dla wszystkich węzłów i dla caption,
					jednak template caption nie wykona się bez parametru "intentionally" -->
				<xsl:apply-templates/>
			</xsl:element>
		</div>
	</xsl:template>

	<!-- list -->
	<xsl:template match="hl7:list">
		<xsl:choose>
			<xsl:when test="@listType='ordered'">
				<xsl:element name="ol">
					<xsl:choose>
						<!-- HTML5 nie wspiera tych typów -->
						<xsl:when test="contains(@styleCode, 'Arabic')">
							<xsl:attribute name="type">1</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'BigAlpha')">
							<xsl:attribute name="type">A</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'BigRoman')">
							<xsl:attribute name="type">I</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'LittleAlpha')">
							<xsl:attribute name="type">a</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'LittleRoman')">
							<xsl:attribute name="type">i</xsl:attribute>
						</xsl:when>
					</xsl:choose>
					<xsl:apply-templates select="@styleCode"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="ul">
					<xsl:choose>
						<!-- HTML5 nie wspiera tych typów -->
						<xsl:when test="contains(@styleCode, 'Circle')">
							<xsl:attribute name="type">circle</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'Disc')">
							<xsl:attribute name="type">disc</xsl:attribute>
						</xsl:when>
						<xsl:when test="contains(@styleCode, 'Square')">
							<xsl:attribute name="type">square</xsl:attribute>
						</xsl:when>
					</xsl:choose>
					<xsl:apply-templates select="@styleCode"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="hl7:list/hl7:item">
		<xsl:element name="li">
			<xsl:apply-templates select="@styleCode"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- zmiana nazwy bez przepisywania ze względów bezpieczeństwa, wyłącznie dla znanych wartości
	 style jest silniejsze niż przypisany class -->
	<xsl:template match="@styleCode">
		<xsl:if test="string-length(.) &gt;= 1">
			<xsl:attribute name="style">
				<xsl:if test="contains(., 'Italics')"> font-style: italic;</xsl:if>
				<xsl:if test="contains(., 'Bold')"> font-weight: bold;</xsl:if>
				<xsl:if test="contains(., 'Underline')"> text-decoration: underline;</xsl:if>
				<xsl:if test="contains(., 'Emphasis')"> font-style: bold;</xsl:if>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<!--  Tabele  -->
	<!-- Znaki dopuszczalne w atrybutach, obsługa w XPATH 1.0, tj. bez wyrażeń regularnych -->
	<xsl:variable name="CHARACTERS_ALLOWED_IN_TABLE_ATTRIBUTES">abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ,.-_!</xsl:variable>

	<!-- tabele są identyczne jak w HTML (poza atrybutem styleCode), są więc kopiowane bez zmian
		 XSD dopuszcza wiele atrybutów nieobsługiwanych w HTML5, ale dopuszczalnych tutaj. -->
	<xsl:template match="hl7:table | hl7:col | hl7:colgroup | hl7:tbody | hl7:td | hl7:tfoot | hl7:th | hl7:thead | hl7:tr">
		<xsl:element name="{local-name()}">

			<xsl:call-template name="copyTableAttributes">
				<xsl:with-param name="tagName" select="local-name()"/>
				<xsl:with-param name="attributes" select="./@*"/>
			</xsl:call-template>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- atrybuty pochodzące z HTML dopuszczalne w XSD Narrative Block dla elementów tabeli
		 wszystkie wartości tych atrybutów będą przepisywane bez zmian niniejszą transformatą z postaci "text/x-hl7-text+xml" do "text/html" -->
	<xsl:variable name="TABLE_ALLOWED_ATTRIBUTES">
		<item name="COL">;ID;language;span;width;align;char;charoff;valign;</item>
		<item name="COLGROUP">;ID;language;span;width;align;char;charoff;valign;</item>
		<item name="TABLE">;ID;language;summary;width;border;frame;rules;cellspacing;cellpadding;</item>
		<item name="TBODY">;ID;language;align;char;charoff;valign;</item>
		<item name="TD">;ID;language;abbr;axis;headers;scope;rowspan;colspan;align;char;charoff;valign;</item>
		<item name="TFOOT">;ID;language;align;char;charoff;valign;</item>
		<item name="TH">;ID;language;abbr;axis;headers;scope;rowspan;colspan;align;char;charoff;valign;</item>
		<item name="THEAD">;ID;language;align;char;charoff;valign;</item>
		<item name="TR">;ID;language;align;char;charoff;valign;</item>
	</xsl:variable>

	<!-- pełna kontrola nad tym co jest kopiowane -->
	<xsl:template name="copyTableAttributes">
		<xsl:param name="tagName"/>
		<xsl:param name="attributes"/>

		<xsl:param name="allowedAttributes" select="document('')/*/xsl:variable[@name='TABLE_ALLOWED_ATTRIBUTES']/*"/>
		<!--<xsl:param name="allowedAttributes" select="$TABLE_ALLOWED_ATTRIBUTES/*"/>-->

		<xsl:for-each select="$attributes">
			<xsl:if test="string-length(translate(., $CHARACTERS_ALLOWED_IN_TABLE_ATTRIBUTES, '')) = 0">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'styleCode'">
						<xsl:if test="string-length(.) &gt;= 1">
							<xsl:attribute name="style">
								<xsl:if test="contains(., 'Italics')"> font-style: italic;</xsl:if>
								<xsl:if test="contains(., 'Bold')"> font-weight: bold;</xsl:if>
								<xsl:if test="contains(., 'Underline')"> text-decoration: underline;</xsl:if>
								<xsl:if test="contains(., 'Emphasis')"> font-style: bold;</xsl:if>
								<xsl:if test="contains(., 'Botrule')"> border-bottom: 1pt solid #dcdcdc;</xsl:if>
								<xsl:if test="contains(., 'Lrule')"> border-left: 1pt solid #dcdcdc;</xsl:if>
								<xsl:if test="contains(., 'Rrule')"> border-right: 1pt solid #dcdcdc;</xsl:if>
								<xsl:if test="contains(., 'Toprule')"> border-top: 1pt solid #dcdcdc;</xsl:if>
							</xsl:attribute>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="contains($allowedAttributes[@name=translate($tagName, $LOWERCASE_LETTERS, $UPPERCASE_LETTERS)], concat(';',local-name(.),';'))">
							<xsl:copy-of select="."/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- nagłówek tabeli to element caption, wyjątkowo -->
	<xsl:template match="hl7:table/hl7:caption">
		<xsl:element name="caption">
			<xsl:attribute name="class">table_caption caption</xsl:attribute>
			<xsl:apply-templates select="@styleCode"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- odnośnik -->
	<xsl:template match="hl7:linkHtml">
		<a href="./@href">
			<xsl:apply-templates/>
		</a>
	</xsl:template>
</xsl:stylesheet>