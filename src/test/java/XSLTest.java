import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class XSLTest {


    @Test
    public void xslTest() throws IOException, TransformerException, ParserConfigurationException, SAXException, JAXBException {
//        String xmlFileName = "src/test/resources/recepta-poprawna-1.3.1.xml";
//        File prescription = new File("src/test/resources/recepta-poprawna-1.3.1.2.xml");
        String fileText = new String(Files.readAllBytes(Paths.get("src/test/resources/recepta-poprawna-1.3.1.2.xml")));
        TransformerFactory factory = TransformerFactory.newInstance();

        // Use the factory to create a template containing the xsl file
        Templates template = factory.newTemplates(new StreamSource(
                new FileInputStream("src/test/resources/pl_informacja_o_receptach_1.3.1.xsl")));

        // Use the template to create a transformer


        final DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//        Document document = db.parse(prescription);
        Document document = db.parse(new ByteArrayInputStream(fileText.getBytes()));
        Map<String, Document> docs = new HashMap<>();
        docs.put("doc2", document);


        Transformer transformer = template.newTransformer();
        transformer.setURIResolver(new DocumentURIResolver(docs));
        transformer.setParameter("kluczPakietu", "10502604538400068944928237096675979833067110");
        transformer.setParameter("kluczDoc1", "10502604538400068944928237096675979833067110");
        transformer.setParameter("receptWPakiecie", "3");
//        transformer.setParameter("doc2", URLEncoder.encode(fileText));
//        transformer.setParameter("doc2", document.getNodeValue());
//        transformer.setParameter("doc2", clinicalDocument);
//        transformer.setParameter("doc2", Base64.getEncoder().encode(fileText.getBytes()));
//        transformer.setParameter("doc2", new File(xmlFile));
//        transformer.setParameter("doc2", new StreamSource(new ByteArrayInputStream(fileText.getBytes())));
//        transformer.setParameter("doc2", new StreamSource(new StringReader(fileText)));
//        transformer.setParameter("doc2", new ByteArrayInputStream(fileText.getBytes()));
        // Prepare the input and output files
        Source source = new StreamSource(new ByteArrayInputStream(fileText.getBytes()));
        Result result = new StreamResult(new FileOutputStream("src/test/resources/recepta-wynik-1.3.1.html"));

        // Apply the xsl file to the source file and write the result
        // to the output file
        transformer.transform(source, result);
    }

    public class DocumentURIResolver implements URIResolver {

        final Map<String, Document> _documents;

        public DocumentURIResolver(final Map<String, Document> documents) {
            _documents = documents;
        }

        public Source resolve(final String href, final String base) {
            final Document doc = _documents.get(href);
            return (doc != null) ? new DOMSource(doc) : null;
        }
    }

}
